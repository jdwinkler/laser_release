from collections import defaultdict
from RegulatoryBuilder import RegulatoryBuilder
from ReactionBuilder import ReactionBuilder
import pickle
import DatabaseUtils
import os
import psycopg2
import psycopg2.extras
import ProductAnnotator
import networkx


'''

Types of toplogical changes this script examines.

Definition: touched node-a node [metabolite or reaction in a metabolic network, gene in a regulatory] that is affected by some sort of mutation

All mutations are treated identically, and properties for all of nodes found in the original network properties
are computed based off of the original network. Properties of heterologous nodes are computed from the altered network.

Current properties assessed:

d(N), degree of touched nodes
eigenvector centrality (N), where EC_{v} = 1/lambda * sum_{t in Neighbors(V)) x_{t}, Ax = lambda x-measures importance of nodes altered.
diversity: number of subsystems hit by changes. 

previously used: betweenness centrality, BC_{v} = sum_{s neq v neq t in G} = sigma_{st}(v)/sigma_{st}, or the number of shortest paths connecting
all nodes in the network that contain V divided by the number of shortest paths between all other nodes. Computationally expensive for large
networks.



'''

class TopologyAnalyzer:

    def __init__(self, standard_obj, calculate_tchars = True, clustering_method = 'multilevel', netgen_method = 'met-rxn',use_currency = True):

        self.standard_obj = standard_obj

        self.INPUT_DIR = DatabaseUtils.INPUT_DIR
        self.OUTPUT_DIR = DatabaseUtils.OUTPUT_DIR

        self.METHOD = clustering_method

        self.network_method = netgen_method

        #papers = DatabaseUtils.getDatabase()

        self.rxn_build = ReactionBuilder(standard_obj)
        self.reg_build = RegulatoryBuilder(standard_obj)
        
        ecoli_regnet, yeast_regnet = self.reg_build.getBaseNetworks()
        ecoli_model, yeast_model = self.rxn_build.getBaseModels()
        
        ecoli_currency, yeast_currency = TopologyAnalyzer.processCurrencyMetabolites(self.INPUT_DIR)
        self.biomass_set = {'Escherichia coli'.upper():self.rxn_build.ecoli_biomass_rxn, 'Saccharomyces cerevisiae'.upper():self.rxn_build.yeast_biomass_rxn}
        self.currency_set = {'Escherichia coli'.upper():ecoli_currency, 'Saccharomyces cerevisiae'.upper():yeast_currency}

        #Is it proper to remove the currency metabolites?
        ecoli_metnet = ReactionBuilder.extractTopology(ecoli_model, method=netgen_method, ignore_reactions = self.rxn_build.ecoli_biomass_rxn, ignore_metabolites = ecoli_currency)
        yeast_metnet = ReactionBuilder.extractTopology(yeast_model, method=netgen_method, ignore_reactions = self.rxn_build.yeast_biomass_rxn, ignore_metabolites = yeast_currency)

        self.base_reg = {'Escherichia coli'.upper():ecoli_regnet, 'Saccharomyces cerevisiae'.upper():yeast_regnet}
        self.base_met = {'Escherichia coli'.upper():ecoli_metnet, 'Saccharomyces cerevisiae'.upper():yeast_metnet}

        self.base_model = {'Escherichia coli'.upper():ecoli_model,'Saccharomyces cerevisiae'.upper():yeast_model}

        #cluster metabolic and regulatory networks

        if(calculate_tchars):
            #cluster map is node-> cluster, memberships is cluster-> list of nodes
            ecoli_cluster, ecoli_memberships = DatabaseUtils.compute_igraph_clustering(ecoli_metnet, method = self.METHOD)
            yeast_cluster, yeast_memberships = DatabaseUtils.compute_igraph_clustering(yeast_metnet, method = self.METHOD)

            self.metabolic_clustering = {'Escherichia coli'.upper():ecoli_cluster, 'Saccharomyces cerevisiae'.upper(): yeast_cluster}

            r_ecoli_cluster, r_ecoli_memberships = DatabaseUtils.compute_igraph_clustering(ecoli_regnet , method = self.METHOD)
            r_yeast_cluster, r_yeast_memberships = DatabaseUtils.compute_igraph_clustering(yeast_regnet , method = self.METHOD)
            
            self.regulatory_clustering = {'Escherichia coli'.upper():r_ecoli_cluster, 'Saccharomyces cerevisiae'.upper(): r_yeast_cluster}


            self.met_centrality = defaultdict(dict)
            self.reg_centrality = defaultdict(dict)

            #this is really slow to continually recompute, but required since I'm not maintaining multiple network centrality files.
            self.met_centrality['Escherichia coli'.upper()]['betweenness'] = DatabaseUtils.compute_igraph_betweenness(ecoli_metnet)
            self.met_centrality['Saccharomyces cerevisiae'.upper()]['betweenness'] = DatabaseUtils.compute_igraph_betweenness(yeast_metnet)
            self.met_centrality['Escherichia coli'.upper()]['eigenvector'] = DatabaseUtils.compute_igraph_evcentrality(ecoli_metnet)
            self.met_centrality['Saccharomyces cerevisiae'.upper()]['eigenvector'] = DatabaseUtils.compute_igraph_evcentrality(yeast_metnet)

            self.reg_centrality['Escherichia coli'.upper()]['betweenness'] = DatabaseUtils.compute_igraph_betweenness(ecoli_regnet)
            self.reg_centrality['Saccharomyces cerevisiae'.upper()]['betweenness'] = DatabaseUtils.compute_igraph_betweenness(yeast_regnet)
            self.reg_centrality['Escherichia coli'.upper()]['eigenvector'] = DatabaseUtils.compute_igraph_evcentrality(ecoli_regnet)
            self.reg_centrality['Saccharomyces cerevisiae'.upper()]['eigenvector'] = DatabaseUtils.compute_igraph_evcentrality(yeast_regnet)

    @staticmethod
    def processCurrencyMetabolites(INPUT_DIR):

        ecoli_file = INPUT_DIR + 'Ecoli Currency Metabolites.txt'
        yeast_file = INPUT_DIR + 'Yeast Currency Metabolites.txt'

        echandle = open(ecoli_file,'r')
        yhandle = open(yeast_file,'r')

        ecoli_set = set()
        yeast_set = set()

        for line in echandle:
            tokens = line.split("\t")
            ecoli_set.add(tokens[0])

        for line in yhandle:
            tokens = line.split("\t")
            yeast_set.add(tokens[0])

        yhandle.close()
        echandle.close()

        return ecoli_set, yeast_set

    #eta1 number of unique types of gene manipulations
    #eta2 number of genes manipulated (unique)
    #N1: number of gene manipulations
    #N2: number of network nodes altered

    #halstead's metric does not include node interactions, which is odd for biology
    #include these interactions as another term (eta3, N3)
    #based off of "Explaining software complexity measures" by EJ Weyuker

    #need to rescale to avoid unit problems
    def compute_halstead_paper(self, paper, mtuples,rtuples, base_met, base_reg, met_clustering, reg_clustering):

        mutation_types = set()
        mutated_genes  = set()
        cluster_crossings = set()
        intents = set()

        eta3 = 0

        nodes = set()

        for i in range(0,len(mtuples)):
            log = (mtuples[i])[1]
            for g in log.get_data('matchedGenes'):
                nodes.add((g[0], log.get_data('species').upper()))

        for i in range(0,len(rtuples)):
            log = (rtuples[i])[1]
            for g in log.get_data('matchedGenes'):
                nodes.add((g[0], log.get_data('species').upper()))

        for (node,host) in nodes:

            if(node in base_met[host].nodes()):
                network = base_met[host]
                clustering = met_clustering[host]
            elif(node in base_reg[host].nodes()):
                network = base_reg[host]
                clustering = reg_clustering[host]
            else:
                continue

            neighbors = network.neighbors(node)

            if(node not in clustering):
                continue

            for n in neighbors:
                if(n in clustering and clustering[n] != clustering[node]):
                    eta3+=1
                    break

        for mutant in paper.mutants:
            for mutation in mutant.mutations:
                mutation_types.update(mutation.changes)
                mutated_genes.add(mutation.name)
                intents.update(mutation.effects)

        eta1 = float(len(mutation_types))
        eta2 = float(len(mutated_genes))
        eta3 = float(eta3)
        eta4 = float(len(intents))

        return eta1,eta2,eta3,eta4, paper.total

    def compute_halstead_INT(self, altered_nodes, mutant, metabolic_network, regulatory_network, base_met, base_reg, met_clustering, reg_clustering, get_etas = False):

        intent = []
        originality = 0.0

        for mutation in mutant.mutations:
            intent.extend(mutation.effects)
            if(mutation.is_original):
                originality += 1.0
                
        manipulations = []
        nodes = []
        genes = set()
        seen = set()
    
        for node in altered_nodes:

            node_name = node[0]
            gene_name = node[1]
            mutation_list = node[2]

            #don't keep adding the same mutation list over and over.
            if(gene_name not in genes):
                manipulations.extend(mutation_list)
                
            nodes.append(node_name)
            genes.add(gene_name)

        total_connections = 0

        eta3 = 0

        for node in nodes:

            if(node in seen):
                continue
            else:
                seen.add(node)

            if(node in metabolic_network.nodes()):
                network = metabolic_network
                clustering = met_clustering
                name = 'M'
            elif(node in base_met.nodes()):
                network = base_met
                clustering = met_clustering
                name = 'M'
            elif(node in regulatory_network.nodes()):
                network = regulatory_network
                clustering = reg_clustering
                name = 'R'
            elif(node in base_reg.nodes()):
                network = base_reg
                clustering = reg_clustering
                name = 'R'
            else:
                continue

            neighbors = network.neighbors(node)

            if(node not in clustering):
                continue

            for n in neighbors:
                if(n in clustering and clustering[n] != clustering[node]):
                    eta3+=1

            '''
            if(node in clustering):
                clusters.append(name + str(clustering[node]))
            else:
                clusters.append(name + 'new node')
            '''

            '''
            if(network.is_directed()):
                print network.out_degree(node), 'directed', node, network.degree(node)
            else:
                print network.degree(node), 'undirected', node
            '''
                
            total_connections = total_connections + float(network.out_degree(node))

        eta1 = float(len(set(manipulations)))
        eta2 = float(len(set([x.name for x in mutant.mutations])))
        eta3 = float(eta3)
        eta4 = float(len(set(intent)))

        N1 = float(len(manipulations))
        N2 = float(len(nodes))
        N3 = float(total_connections)
        N4 = float(len(intent))

        #print eta1,eta2,eta3,eta4

        if(eta2 == 0.0):

            if(get_etas):
                return 0,eta1,eta2,eta3,eta4,originality
            else:
                return 0

        #n1+n2+n3+n4 = total number of "things" (modificiations, types of mutations, interconnectivity, and actual researcher goals) performed by this design
        #eta1+eta2+eta3+eta4: total unique operators used to implement/think about this paper (strictly less than or equal to N1+N2+N3+N4)
        #D ~ originality penalty (raise complexity for more original modifications)
        #V = (N1 + N2 + N3 + N4) * numpy.log2(eta1+eta2+eta3+eta4) * (1 + float(originality)/eta2)

        #y = -4.711206626*10-1 x1 - 2.290765293*10-2 x2 + 4.261356141*10-1 x3 - 6.108432152*10-1 x4 + 1.479908459*10-1 x5 + 3.42453437

        #wgc = lambda e1,e2,e3,e4,orig :  -0.4711*eta1 + -0.022907*e2 + 0.4261*e3 - 0.6108*e4 + 0.14799*orig + 3.4245

        #V = wgc(eta1,eta2,eta3,eta4,originality)

        V = (eta1+eta3+eta4)/eta2 * (1 + float(originality)/eta2)

        #print eta1,eta2,eta3,eta4,originality


        #V = eta1*eta3*eta4/eta2 * (1 + float(originality)/eta2)
        #print eta1,eta2,eta3,eta4,originality
        if(get_etas):
            return V,eta1,eta2,eta3,eta4,originality
        else:
            return V

    def compute_rho_affected(self, base_network, metabolic_network, original_node_clustering, targeted_nodes):

        name = 'M'

        clusters = set()
        
        for node in targeted_nodes:

            if(node in original_node_clustering):
                clusters.add(original_node_clustering[node])
            else:
                clusters.add(name + 'new cluster')

        return len(clusters)
    
    @staticmethod
    def node_property_changes(base_network, altered_network, altered_nodes = None, original_between = None, original_eigen = None):

        node_properties = defaultdict(dict)

        ran = False

        #protects against accessing nodes that do not exist in the original network
        shared = set(base_network.nodes()) & set(altered_nodes)
        #deleted nodes
        difference = set(base_network.nodes()) - set(altered_nodes)

        if(original_between == None):
            original_between = DatabaseUtils.compute_igraph_betweenness(base_network)

        if(original_eigen == None):
            original_eigen = DatabaseUtils.compute_igraph_evcentrality(base_network)

        #calculate network betweenness/eigenvector centrality of nodes added to the altered network
        
        #bc = DatabaseUtils.compute_igraph_betweenness(altered_network)
        #ec = DatabaseUtils.compute_igraph_evcentrality(altered_network)

        missing = []
        
        for touched in altered_nodes:

            ran = True

            '''
            if(touched in altered_network.nodes()):
                node_properties[touched]['betweenness'] = bc[touched]
                node_properties[touched]['eigenvector'] = ec[touched]
                node_properties[touched]['degree'] = altered_network.degree(touched)
            '''
            if(touched in base_network.nodes()):
                #these are deleted nodes
                node_properties[touched]['betweenness'] = original_between[touched]
                node_properties[touched]['eigenvector'] = original_eigen[touched]
                node_properties[touched]['degree'] = base_network.degree(touched)
            else:
                missing.append(touched)
            
        return node_properties, ran, missing

    '''
    Want to summarize topological changes for both metabolic engineering and regulatory networks.

    Prefer to do this by comparison with unaltered models (easier conceptually, don't need to pass around large amounts of data).

    
    Number of modifications
    Product, last non-native metabolite added
    Nodes altered/mutated/removed and their degree, centrality
    New linkages added
    Key metabolic chokepoints
    Key regulatory chokepoints
    Complexity of modifications in totality
    Any changes not implemented

    #papers: database of papers from MetEngDatabase
    filename: name of output pickle file
    '''

    def process_paper(self, cur, paper):

        model_log_tuples = self.rxn_build.generateModel(cur, paper)
        network_log_tuples = self.reg_build.generateNetwork(cur, paper)
        
        if(len(model_log_tuples) != len(network_log_tuples)):
            raise ValueError('Either regulatorbuilder or reactionbuilder is failing and not returning the expected number of results, look into it.')
            
        #for each model, extract topology (for met model, intersect node sets, then compare edge sets for each node.

        met_results = []
        reg_results = []

        met_missing_nodes = []
        reg_missing_nodes = []

        targeted_density = []

        halstead = []
        orig = 0

        for i in range(0,len(network_log_tuples)):

            model = (model_log_tuples[i])[0]
            regulatory_network = (network_log_tuples[i])[0]

            model_log = (model_log_tuples[i])[1]
            regulatory_log = (network_log_tuples[i])[1]

            host = regulatory_log.get_data('species').upper()

            #find list of "touched" genes

            #matchedGenes
            met_unhandled = model_log.get_data('unhandledNodes')
            reg_unhandled = regulatory_log.get_data('unhandledNodes')

            metabolic_network = ReactionBuilder.extractTopology(model, method = self.network_method, ignore_reactions = self.biomass_set[host], ignore_metabolites = self.currency_set[host])

            met_altered = set()
            reg_altered = set()

            altered_full = []

            for node in model_log.get_data('matchedGenes'):
                met_altered.add(node[0])
                altered_full.append(node)

            for item in met_unhandled:
                met_altered.add(item[0])
                altered_full.append(item)

            for node in regulatory_log.get_data('matchedGenes'):
                reg_altered.add(node[0])
                altered_full.append(node)
                
            for item in reg_unhandled:
                reg_altered.add(item[0])
                altered_full.append(item)

            if(host not in self.base_reg or host not in self.base_met):
                print 'Unknown host %s' % host
                print 'You probably should not be seeing this message, to be honest.'
                continue
            
            met_properties, msuccess, met_missing = TopologyAnalyzer.node_property_changes(self.base_met[host],  metabolic_network, altered_nodes = met_altered, original_between = self.met_centrality[host]['betweenness'], original_eigen = self.met_centrality[host]['eigenvector'])
            reg_properties, rsuccess, reg_missing = TopologyAnalyzer.node_property_changes(self.base_reg[host],  regulatory_network, altered_nodes = reg_altered, original_between = self.reg_centrality[host]['betweenness'], original_eigen = self.reg_centrality[host]['eigenvector'])

            met_missing_nodes.extend(met_missing)
            reg_missing_nodes.extend(reg_missing)

            ####add edge and cluster membership calculator
            #cluster original networks
            #get list of altered nodes that are still in the original network
            #get a list of their new edges/deleted edges
            #classify manipulated as in/out of cluster (if possible)
            #compute out/in (without weight) and out/in (with weight)
            #how do deal with added edges out of the network? add to out, cluster, etc... add a keyword option

            met_cluster_count = self.compute_rho_affected(self.base_met[host], metabolic_network, self.metabolic_clustering[host],met_altered)
            reg_cluster_count = self.compute_rho_affected(self.base_reg[host], regulatory_network, self.regulatory_clustering[host],reg_altered)
            targeted_density.append((met_cluster_count, reg_cluster_count))

            En = self.compute_halstead_INT(altered_full, paper.get_mutant(i+1),  metabolic_network, regulatory_network, self.base_met[host], self.base_reg[host], self.metabolic_clustering[host], self.regulatory_clustering[host])
            halstead.append(En)

            #collate affected subsystems
            unique_subs = set()
            for node in met_altered:

                if(node in model.reactions and node in self.base_met[host].nodes()):
                    subsystem = model.reactions.get_by_id(node).subsystem
                    met_properties[node]['subsystem'] = subsystem
                    unique_subs.add(subsystem)

            met_results.append(met_properties)
            reg_results.append(reg_properties)

        time_eqn = DatabaseUtils.strain_properties_effort_correlation()

        e1,e2,e3,e4,e5 = self.compute_halstead_paper(paper,model_log_tuples,network_log_tuples,self.base_met, self.base_reg, self.metabolic_clustering, self.regulatory_clustering)

        T = time_eqn(e1,e2,e3,e4,e5)

        '''
        fhandle = open('etas.txt','a')
        fhandle.write('\t'.join([paper.title,str(e1),str(e2),str(e3),str(e4),str(e5)]) + '\n')
        fhandle.close()
        '''

        return T, model_log_tuples, network_log_tuples, met_results, reg_results, targeted_density, halstead, met_missing_nodes, reg_missing_nodes

    def process_dataset(self, cur, papers, filename):

        met_cache = DatabaseUtils.load_metabolite_cache()

        counter = 0
        outputList = []
        #method = 'additive'

        seenPapers = set()

        met_missing_nodes = set()
        reg_missing_nodes = set()

        met_missing_count = defaultdict(int)
        reg_missing_count = defaultdict(int)
        
        for paper in papers:

            year = paper.year

            #print 'Year: %i' % year
            #for each model, extract topology (for met model, intersect node sets, then compare edge sets for each node.

            #print paper.title

            time_estimate, model_log_tuples, network_log_tuples, met_results, reg_results, targeted_densities, halstead, met_missing, reg_missing = self.process_paper(cur, paper)

            for met in met_missing:
                met_missing_nodes.add(met)
                met_missing_count[met] = met_missing_count[met] + 1

            for reg in reg_missing:
                reg_missing_nodes.add(reg)
                reg_missing_count[met] = reg_missing_count[met] + 1


            titer_conversion, yield_conversion = ProductAnnotator.unit_analysis_paper(paper, met_cache)

            #print paper.title, time_estimate

            for i in range(0,len(met_results)):

                pickled_dict = {}

                #store enough information to uniquely associate these results with a mutant/paper
                pickled_dict['year'] = year
                pickled_dict['DOI'] = paper.doi
                pickled_dict['host'] = model_log_tuples[i][1].get_data('species').upper()
                pickled_dict['time_required'] = time_estimate
                pickled_dict['title'] = paper.title
                pickled_dict['mutant'] = i+1
                pickled_dict['metabolic'] = met_results[i]
                pickled_dict['regulatory'] = reg_results[i]
                pickled_dict['halstead'] = halstead[i]
                pickled_dict['met_clusters'] = targeted_densities[i][0]
                pickled_dict['reg_clusters'] = targeted_densities[i][1]
                pickled_dict['titers'] = titer_conversion[i]
                pickled_dict['yields'] = yield_conversion[i]

                #add results to output array, save for subsequent processing
                outputList.append(pickled_dict)
                
            counter = counter + 1
            
            if(counter % 10 == 0):
                print 'On paper: %i' % counter

        met_error = []
        reg_error = []

        for met in met_missing_nodes:
            met_error.append((met, met_missing_count[met]))

        for reg in reg_missing_nodes:
            reg_error.append((reg, reg_missing_count[met]))

        DatabaseUtils.writefile(('Missing Node','Count'), met_error, '\t', DatabaseUtils.ERROR_DIR + 'Missing Metabolic Nodes.txt')
        DatabaseUtils.writefile(('Missing Node','Count'), reg_error, '\t', DatabaseUtils.ERROR_DIR + 'Missing Regulatory Nodes.txt')
                
        fhandle = open(filename,'wb')
        pickle.dump(outputList, fhandle)
        fhandle.close()

    @staticmethod
    def generate_data(pickle_name, papers, standard_obj):

        try:
            connect = psycopg2.connect("dbname='biocyc' user='james' host='localhost' password='winkler'")
        except:
            print "I am unable to connect to the database"
            raise

        cur = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        
        ta = TopologyAnalyzer(standard_obj,netgen_method = 'met-rxn')

        ta.process_dataset(cur, papers, pickle_name)

if(__name__ == '__main__'):

    from Standardization import Standard

    DatabaseUtils.OUTPUT_DIR = os.path.join(os.getcwd(),'output_2015')

    cur_name = os.path.join(DatabaseUtils.OUTPUT_DIR,'Model and Node Topology-+C R-R Network.pkl')

    standard_obj = Standard(os.path.join(DatabaseUtils.INPUT_DIR,'Gene Name Mapping.txt'))

    TopologyAnalyzer.generate_data(cur_name, standard_obj)





        



















    
