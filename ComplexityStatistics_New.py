import DatabaseUtils
from collections import defaultdict
from TopologyAnalyzer import TopologyAnalyzer
import networkx
import numpy
import pickle
import scipy.stats
from scipy.cluster.vq import kmeans,vq
import sys
import copy
import GraphUtils as glib
from operator import itemgetter
import MetEngDatabase
import os

#plots correlations between various metrics. also computes and outputs spearman (to account for non-normal data) correlation values.
def model_correlation_analysis(dataset, metric1, metric2, metric1_label, metric2_label, m1='linear',m2='linear', output_figure= False):

    m1_values = []
    m2_values = []

    for point in dataset:

        if(metric1 not in point or metric2 not in point):
            continue

        if(metric1 == 'titers' or metric1 == 'yields'):
            m1_value = point[metric1][0]
        else:
            m1_value = point[metric1]

        if(metric2 == 'titers' or metric2 == 'yields'):
            m2_value = point[metric2][0]
        else:
            m2_value = point[metric2]

        if(numpy.isnan(m1_value) or numpy.isnan(m2_value)):
            continue

        if m2_value > 200 and metric2 == 'yields':
            print metric2, point[metric2], point
            raise AssertionError('temp breakpoint')

        if(m1 == 'log'):
            m1_values.append(numpy.log(m1_value))
        else:
            m1_values.append(m1_value)

        if(m2 == 'log'):
            m2_values.append(numpy.log(m2_value))
        else:
            m2_values.append(m2_value)

    (rho, p) = scipy.stats.spearmanr(m1_values, m2_values)

    print 'Spearman p-value between %s and %s is: %g, with r = %g ' % (metric1, metric2, p, rho)

    if(output_figure):
        glib.scatter(m1_values, m2_values, metric1_label, metric2_label, os.path.join(DatabaseUtils.OUTPUT_DIR,'complexity',metric1 + '_' + metric2 + '_scatter plot.pdf'))


#just outputs design statistics, not comparison to others
#design interconnectivity
#number of nodes altered
#product
#yield/titer
#median node degree, betweenness, eigenvector centrality
def output_design_properties(dataset, papers, outputFile, selection = 'metabolic', numGK='NumberofMutations'):

    #re-org papers into DOI-paper object map

    numeric_fields = ['eigenvector','degree','betweenness']
    count_uniques = ['subsystem']

    if(selection not in {'metabolic','regulatory'}):
        print 'Invalid data output selection.'
        raise AssertionError('%s is not a valid network type' % selection)

    paper_dict = {}

    for paper in papers:
        doi = paper.paperBacking['DOI']
        paper_dict[doi] = paper

    if(selection == 'metabolic'):
        density = 'met_clusters'
    elif(selection == 'regulatory'):
        density = 'reg_clusters'
    else:
        raise AssertionError('Wacky!')

    header = ('Title',
              'DOI',
              'Journal'
              'Imeplemented Designs',
              'Model Number',
              'Product',
              'Final Titer',
              'Titer Units',
              'Final Yield',
              'Yield Units',
              'Genes Altered',
              'Nodes Altered',
              'Perceived Complexity',
              'Clusters Affected',
              'Modified Halstead',
              'Inverse Abundance Complexity')

    keys = ['title',
            'DOI',
            'journal',
            'models',
            'mutantNumber',
            'product',
            'titer',
            'titer_units',
            'yields',
            'yield_units',
            'genes_altered',
            'nodes_altered',
            'perception',
            'density',
            'halstead',
            'inverse',]

    outputList = []

    dictOutput = []

    paper_mapper = {}
    counter = 0

    for paper in papers:

        paper_mapper[paper.doi] = paper

    for point in dataset:

        combined_data = {}

        mutantNumber = point['mutant']
        paper = paper_dict[point['DOI']]
        perceived_complexity = paper.paperBacking['project_score']
        product = paper.mutantBacking[(mutantNumber, 'TargetMolecule')]
        mutations = int(paper.mutantBacking[(mutantNumber, numGK)])

        genes = set()

        for i in range(1,mutations+1):
            genes.add(paper.geneBacking[(mutantNumber,i,'GeneName')])


        journal = paper.paperBacking['Journal']
        models = paper.paperBacking['total']

        combined_data['title'] = paper.paperBacking['Title']
        combined_data['DOI'] = point['DOI']
        combined_data['models'] = models
        combined_data['journal'] = journal
        combined_data['mutantNumber'] = mutantNumber
        combined_data['product'] = product
        combined_data['titer'] = point['titers'][0]
        combined_data['titer_units'] = point['titers'][1]
        combined_data['yields'] = point['yields'][0]
        combined_data['yield_units'] = point['yields'][1]
        combined_data['density'] = point[density]
        combined_data['inverse'] = point['inverse']
        combined_data['halstead'] = point['halstead']
        combined_data['perception'] = perceived_complexity

        data = point[selection]

        for field in numeric_fields:
            values = []
            for node in data:
                if(field in data[node]):
                    values.append(data[node][field])
            combined_data[field] = numpy.median(values)

        for field in count_uniques:
            unique_values = set()
            for node in data:
                if(field in data[node]):
                    unique_values.add(data[node][field])
            combined_data[field] = len(unique_values)

        combined_data['nodes_altered'] = len(data.keys())
        combined_data['genes_altered'] = len(genes)

        dictOutput.append(combined_data)

        tempList = []
        for field in keys:
            tempList.append(combined_data[field])
        outputList.append(tuple(tempList))

    DatabaseUtils.writefile(header, outputList, '\t', outputFile)

    return dictOutput, keys

#break down by species/network type
#number of nodes
#number of edges (average degree)
#diameter of the network of giant component
#number of components
#average betweenness (very small)
#number of clusters with applied clustering method

#properties of modified networks
#number of nodes modified (avg,std)
#number of edges modified (avg,std)
#average betweenness of modified nodes vs. unmodified network (P < 0.00000000)
#number of clusters modified (avg,std)

def topology_information(dataset, metabolic_networks, regulatory_networks):

    def network_analysis(networks,ntype):

        for host in networks:

            num_nodes = len(networks[host].nodes())
            num_edges = len(networks[host].edges())
            #components = networkx.number_connected_components(networks[host])
            #diameter = networkx.diameter(networks[host])

            print '\nOverall Network properties for (host): %s and type: %s' % (host, ntype)
            print 'Number of nodes: %i' % num_nodes
            print 'Number of edges: %i' % num_edges
            print 'Average node degree (Ne/Nn): %f' % (float(num_edges)/float(num_nodes))
            #print 'Network diameter (maximum path length between nodes): %i' % diameter
            #print 'Number of connected components: %i' % components

    def compute_dataset_properties(dataset, ntype, species = 'ESCHERICHIA COLI'):

        modified_count  = []
        modified_degree = []
        modified_clusters = []

        species = species.upper()

        for point in dataset:

            if(point['host'].upper() != species):
                continue

            node_data = point[ntype]

            modified_count.append(len(node_data.keys()))

            if(ntype == 'regulatory'):
                modified_clusters.append(point['reg_clusters'])
            if(ntype == 'metabolic'):
                modified_clusters.append(point['met_clusters'])

            for node in node_data:
                modified_degree.append(node_data[node]['degree'])

        print '\nEvaluating modified network component properties for %s networks for %s' % (ntype, species)
        print 'Average number of modified nodes: %f, (std = %f)' % (numpy.mean(modified_count), numpy.std(modified_count))
        print 'Average degree of modified nodes: %f, (std = %f)' % (numpy.mean(modified_degree), numpy.std(modified_degree))
        print 'Average number of modified clusters: %f, (std = %f)' % (numpy.mean(modified_clusters), numpy.std(modified_clusters))

    print 'Starting topological summary.'

    network_analysis(metabolic_networks, 'metabolic')
    network_analysis(regulatory_networks, 'regulatory')

    compute_dataset_properties(dataset, 'metabolic' ,species = 'ESCHERICHIA COLI')
    compute_dataset_properties(dataset, 'regulatory',species = 'ESCHERICHIA COLI')

    compute_dataset_properties(dataset, 'metabolic', species = 'Saccharomyces cerevisiae'.upper())
    compute_dataset_properties(dataset, 'regulatory', species = 'Saccharomyces cerevisiae'.upper())

    print 'Finished topological summary.'

def inverse_abundance_complexity(papers, dataset):

    #calculate complexity of each paper based on the number of mutations + the inverse frequency at which each mutation method is used

    def frequency(datadict):

        data_sum = 0

        for key in datadict:
            data_sum = data_sum + datadict[key]

        output_dict = {}

        for key in datadict:
            output_dict[key] = float(datadict[key])/float(data_sum)

        return output_dict

    def iter_papers(mutation_frequency = None, dmethod_frequency = None):

        mutation_count = defaultdict(int)
        dmethod_count = defaultdict(int)

        cscores = {}

        for paper in papers:

            for mutant in paper.mutants:

                complexity_score = 0

                mutations = mutant.mutations
                methods = mutant.methods

                for method in methods:
                    dmethod_count[method] = dmethod_count[method] + 1
                    if(dmethod_frequency != None and method in dmethod_frequency):
                        complexity_score = complexity_score + 1.0/dmethod_frequency[method]

                for j in mutations:
                    mutation_list = j.changes
                    for mut in mutation_list:
                        mutation_count[mut] = mutation_count[mut] + 1
                        if(mutation_frequency != None and mut in mutation_frequency):
                            complexity_score = complexity_score + 1.0/mutation_frequency[mut]

                cscores[(mutant.id,paper.doi)] = complexity_score

        return cscores, mutation_count, dmethod_count

    _,mutation_count,dmethod_count = iter_papers()

    cscores,_,_ = iter_papers(mutation_frequency = frequency(mutation_count), dmethod_frequency = frequency(dmethod_count))

    for point in dataset:

        point['inverse'] = cscores[(point['mutant'], point['DOI'])]

    return dataset

def yield_trend(dataset):

    xvalues = []
    yvalues = []

    counter = 0

    for point in dataset:

        if(not numpy.isnan(point['yields'][0])):
            xvalues.append(point['year'])
            yvalues.append(point['yields'][0])
            counter+=1

    print 'Number of designs with yield information: %i, fraction of total designs: %f' % (counter, float(counter)/float(len(dataset)))

    glib.scatter(xvalues, yvalues, 'Year', '% Theoretical Yield (cmol/cmol)',os.path.join(DatabaseUtils.OUTPUT_DIR,'Year vs Yield Scatter.pdf'))

#converts 234 emilio models into LASER paper records for topological model generation, analysis
#really stupid, but whatever.
#note: probably underestimates complexity due to the fact that there is not a 1-1 mapping between rxn/genes
def comp_library_screening(ta, mutantFields, geneFields, parserDict, numMK = 'NumberofMutations'):

    from ReactionBuilder import ReactionBuilder
    from Standardization import Standard

    host = 'ESCHERICHIA COLI'

    ecoli_cobra = ta.base_model[host]

    fhandle = open(DatabaseUtils.INPUT_DIR + 'EMiLIO Strain Designs.txt')
    emilio = fhandle.readlines()
    fhandle.close()

    halstead_yield = []
    xlabel = []

    counter = 0

    for line in emilio[1:]:

        tokens = line.split('\t')

        #they give reaction names
        deletions = tokens[0].strip().split(',')
        mod_lower_bound = tokens[1].strip().split(',')
        mod_upper_bound = tokens[2].strip().split(',')
        pred_yield = float(tokens[3].strip())

        model = ecoli_cobra.copy()

        geneBacking = {}
        mutantBacking = {}
        annotationBacking = {}

        altered_nodes = []

        mut_counter = 1

        for del_rxn in deletions:

            if('-' in del_rxn):
                continue

            rxn = model.reactions.get_by_id(del_rxn.strip())
            rxn.remove_from_model()

            altered_nodes.append((del_rxn, str(mut_counter) + '_gene',['del']))
            geneBacking[(1, mut_counter, 'GeneName')] = del_rxn.strip()
            geneBacking[(1, mut_counter, 'GeneSource')] = 'Escherichia coli'
            geneBacking[(1, mut_counter, 'GeneEffect')] = ['decreased_byproduct_form']
            geneBacking[(1, mut_counter, 'GeneOriginal')] = ['no']
            annotationBacking[(1, mut_counter, 'GeneMutation','del')] = ''
            mut_counter = mut_counter+1

        for mod_rxn in mod_lower_bound:

            if('-' in mod_rxn):
                continue

            rxn = model.reactions.get_by_id(mod_rxn.strip())
            gene = rxn.get_gene()

            altered_nodes.append((mod_rxn, str(mut_counter) + '_gene',['mutated']))
            geneBacking[(1, mut_counter, 'GeneName')] = mod_rxn.strip()
            geneBacking[(1, mut_counter, 'GeneSource')] = 'Escherichia coli'
            geneBacking[(1, mut_counter, 'GeneEffect')] = ['increased_flux']
            geneBacking[(1, mut_counter, 'GeneOriginal')] = ['no']
            annotationBacking[(1, mut_counter, 'GeneMutation','mutated')] = 'targeted for modification'
            mut_counter = mut_counter+1

        for mod_rxn in mod_upper_bound:

            if('-' in mod_rxn):
                continue

            rxn = model.reactions.get_by_id(mod_rxn.strip())
            gene = rxn.get_gene()

            altered_nodes.append((mod_rxn, str(mut_counter) + '_gene',['mutated']))
            geneBacking[(1, mut_counter, 'GeneName')] = mod_rxn.strip()
            geneBacking[(1, mut_counter, 'GeneSource')] = 'Escherichia coli'
            geneBacking[(1, mut_counter, 'GeneEffect')] = ['decreased_flux']
            geneBacking[(1, mut_counter, 'GeneOriginal')] = ['no']
            annotationBacking[(1, mut_counter, 'GeneMutation','mutated')] = 'targeted for modification'
            mut_counter = mut_counter+1

        mutantBacking[(1,numMK)] = str(len(altered_nodes))

        mutant = MetEngDatabase.Mutant(1, mutantBacking, geneBacking, annotationBacking, mutantFields, geneFields, Standard('',empty=True),Standard('',empty=True), parserDict, ignore_missing_values = True)

        #get reaction network after processing
        metabolic_network = ReactionBuilder.extractTopology(model, method = ta.network_method, ignore_reactions = ta.biomass_set[host], ignore_metabolites = ta.currency_set[host])

        H = ta.compute_halstead_INT(altered_nodes, mutant, metabolic_network, networkx.DiGraph(), ta.base_met[host], ta.base_reg[host], ta.metabolic_clustering[host], {})

        #print 'Finished model %i' % counter
        counter = counter + 1

        xlabel.append(' ')

        halstead_yield.append((H, pred_yield))

    halstead_yield = sorted(halstead_yield, key=lambda item: item[0])

    #glib.simple_bargraph([], [value[0] for value in halstead_yield], 'EMiLIO Generated Models', 'Winkler-Gill Complexity Score', DatabaseUtils.OUTPUT_DIR + 'EMiLIO Complexity Trend.pdf')
    #def linebar(keys, values, xvalues, yvalues, xlabel, y1label, y2label, filename, mapper=None):
    glib.linebar(xlabel,[value[0] for value in halstead_yield],[],[value[1] for value in halstead_yield],'EMiLIO Generated Models', 'Winkler-Gill Complexity Score', 'Theoretical Yield (%)',os.path.join(DatabaseUtils.OUTPUT_DIR,'complexity','EMiLIO Complexity Trend.pdf'), edgecolor='b', show_ticks=False)

#evaluates the complexity of pairwise (N-wise technically, but won't plot anything but mut per strain = 2) regulator libraries
#note: update this method if you change the definition of WGC in anyway
def random_reg_variant_generation(topology_analyzer, species, mutantFields, geneFields, parserDict, mutations_per_strain=2, top = 20):

    import itertools
    from Standardization import Standard

    regnet = topology_analyzer.base_reg[species.upper()]
    clusters = topology_analyzer.regulatory_clustering[species.upper()]

    sorted_list = []
    for node in regnet.nodes():
        sorted_list.append((node, regnet.degree(node)))

    sorted_list = sorted(sorted_list, key=lambda x: x[1], reverse=True)

    node_array = [x[0] for x in sorted_list[0:top-1]]

    #okay, now have array of genes, generate all possible combinations of mutations per strain
    node_permute = itertools.permutations(node_array, r=mutations_per_strain)
    #in this case, WGC is basically just proportional to the number of nodes/clusters affected by the mutations

    species = species.upper()

    complexity_scores = {}
    matrix_scores = defaultdict(dict)
    added_nodes = set()

    for p in node_permute:

        geneBacking = {}
        mutantBacking = {}
        annotationBacking = {}

        mut_counter = 1

        geneBacking[(1, mut_counter, 'GeneName')] = p[0]
        geneBacking[(1, mut_counter, 'GeneSource')] = species
        geneBacking[(1, mut_counter, 'GeneEffect')] = 'aa_snps'
        geneBacking[(1, mut_counter, 'GeneEffect')] = ['global']
        geneBacking[(1, mut_counter, 'GeneOriginal')] = ['yes']
        annotationBacking[(1, mut_counter, 'GeneMutation','mutated')] = ''

        mut_counter = mut_counter+1

        geneBacking[(1, mut_counter, 'GeneName')] = p[1]
        geneBacking[(1, mut_counter, 'GeneSource')] = species
        geneBacking[(1, mut_counter, 'GeneEffect')] = 'aa_snps'
        geneBacking[(1, mut_counter, 'GeneEffect')] = ['global']
        geneBacking[(1, mut_counter, 'GeneOriginal')] = ['yes']
        annotationBacking[(1, mut_counter, 'GeneMutation','mutated')] = ''

        altered_genes = [(p[0],p[0],['aa_snps']), (p[1],p[1],['aa_snps'])]

        mutantBacking[(1,'NumberofMutations')] = 2

        mutant = MetEngDatabase.Mutant(1, mutantBacking, geneBacking, annotationBacking, mutantFields, geneFields, Standard('',empty=True),Standard('',empty=True), parserDict, ignore_missing_values = True)

        #get reaction network after processing
        H = topology_analyzer.compute_halstead_INT(altered_genes, mutant, networkx.DiGraph(), networkx.DiGraph(), topology_analyzer.base_met[species], topology_analyzer.base_reg[species], topology_analyzer.metabolic_clustering[species], topology_analyzer.regulatory_clustering[species])

        complexity_scores[p] = H
        if(mutations_per_strain == 2):
            matrix_scores[p[0]][p[1]] = H
            added_nodes.add(p[0])
            added_nodes.add(p[1])

    #print 'Number of permutations: %i' % len(complexity_scores.keys())

    data = []

    maximum = 0
    minimum = 10000000

    for gene_x in added_nodes:

        columns = []
        #symmetrical matrix
        for gene_y in added_nodes:

            if(gene_y in matrix_scores[gene_x]):
                columns.append(numpy.log2(matrix_scores[gene_x][gene_y]))
                if(numpy.log2(matrix_scores[gene_x][gene_y]) > maximum):
                    maximum = numpy.log2(matrix_scores[gene_x][gene_y])
                if(numpy.log2(matrix_scores[gene_x][gene_y]) < minimum):
                    minimum = numpy.log2(matrix_scores[gene_x][gene_y])
            else:
                columns.append(0)

        data.append(columns)

    matrix = numpy.matrix(data)

    glib.generate_heatmap(numpy.array(matrix), list(added_nodes), 'Log2(Joint WGC Complexity)', os.path.join(DatabaseUtils.OUTPUT_DIR,'complexity',species+' Random Library HeatMap.pdf'))

def time_effort_correlation(dataset):

    correlation, y_points, y_est, eqn_string, rsq = DatabaseUtils.strain_properties_effort_correlation(return_y=True)

    errors = []

    for y_real, y_f in zip(y_points,y_est):

        errors.append(abs(y_real-y_f))

    error_mean = numpy.mean(errors)
    std_dev    = numpy.std(errors)

    percent90 = error_mean + 1.65 * std_dev/numpy.sqrt(len(errors)-1)

    glib.scatter(y_est,y_points,'Estimated Completion Times (FTE-Years)','Reported Completion Times (FTE-Years)',os.path.join(DatabaseUtils.OUTPUT_DIR,'complexity','Completion Time Scatter.pdf'), regression='linear', equation_string = eqn_string, errors = [percent90, percent90])

    paper_effort_dict = {}

    longest_paper = ''
    longest_completion_time = 0

    for point in dataset:

        paper_effort_dict[point['DOI']] = point['time_required']

        if(point['time_required'] > longest_completion_time):
            longest_completion_time = point['time_required']
            longest_paper = point['DOI']

    glib.histogram(paper_effort_dict.values(),'Estimated Completion Times (FTE-Years)','Counts',os.path.join(DatabaseUtils.OUTPUT_DIR,'complexity','Completion Time Historgram.pdf'),bins=20)

    print 'This is the doi of the paper that will take the longest time to complete: %s' % longest_paper

def generate_figures(ta, papers, parserDict, standard_obj, cur_name):

    paper_terms, mutant_terms, mutation_terms = DatabaseUtils.get_object_fields()



    comp_library_screening(ta,mutant_terms,mutation_terms,parserDict)

    random_reg_variant_generation(ta,'Escherichia coli',mutant_terms,mutation_terms,parserDict)

    #random_reg_variant_generation(ta,'Saccharomyces cerevisiae',mutant_terms,mutation_terms,parserDict)

    fhandle = open(cur_name,'rb')
    dataset = pickle.load(fhandle)
    fhandle.close()

    dataset = inverse_abundance_complexity(papers,dataset)

    time_effort_correlation(dataset)

    doi_mapper = {}

    for paper in papers:
        doi_mapper[paper.doi] = paper

    for point in dataset:
        point['perceived_complexity'] = doi_mapper[point['DOI']].difficulty
        point['scale'] = doi_mapper[point['DOI']].total_designs

    halstead = [x['halstead'] for x in dataset]
    inverse  = [x['inverse'] for x in dataset]
    time     = [x['time_required'] for x in dataset]

    print 'WGC median and average are %g, %g respectively' % (numpy.median(halstead),numpy.mean(halstead))

    print 'Frequency median and average are %g, %g respectively' % (numpy.median(inverse),numpy.mean(inverse))

    print 'Time required (median): %g' % numpy.median(time)

    #correlation analysis
    model_correlation_analysis(dataset,'halstead','inverse','WGC Complexity','Frequency Complexity')

    model_correlation_analysis(dataset,'halstead','yields','WGC Complexity','% Theoretical Yield (Cmol/Cmol)')

    model_correlation_analysis(dataset,'inverse','yields','Frequency','% Theoretical Yield (Cmol/Cmol)')

    model_correlation_analysis(dataset, 'halstead','perceived_complexity','','')

    model_correlation_analysis(dataset, 'inverse','perceived_complexity','Frequency Complexity','Peceived Complexity')

    model_correlation_analysis(dataset, 'inverse','time_required','Frequency Complexity','Estimated Time Required (years)', output_figure=True)

    model_correlation_analysis(dataset,'time_required','yields','Estimated Time Required (years)','% Theoretical Yield (Cmol/Cmol)', output_figure=True)

    ##plot distributions

    glib.histogram(halstead,'WGC Complexity','Counts',os.path.join(DatabaseUtils.OUTPUT_DIR,'complexity','WGC Complexity Distribution.pdf'),bins=20, useMedianFilter=True)

    glib.histogram(inverse,'Frequency Complexity','Counts',os.path.join(DatabaseUtils.OUTPUT_DIR,'complexity','Frequency Complexity Distribution.pdf'),bins=20, useMedianFilter=True)

if(__name__ == '__main__'):

    generate_figures('','','','')







