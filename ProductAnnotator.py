import DatabaseUtils
import psycopg2
import psycopg2.extras
import warnings

def compound_lookup(cur, compound):

    query = 'select * from compounds where upper(common_name) = upper(%s) or upper(unique_id) = upper(%s)'
    cur.execute(query, (compound,compound))

    return cur.fetchone()

def substrate_annotator():

    fhandle = open('Substrate Name Converter.txt','rU')
    lines = fhandle.readlines()
    fhandle.close()

    substrate_map = {}

    for line in lines[1:]:
        tokens = line.strip().upper().split('\t')

        if(tokens[1] != 'NONE'):
            substrate_map[tokens[0]] = tokens[1]
        else:
            substrate_map[tokens[0]] = None

    outputList = []

    try:
        connect = psycopg2.connect("dbname='biocyc' user='james' host='localhost' password='winkler'")
    except:
        print "I am unable to connect to the database"
        raise

    cur = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    for sub in substrate_map:

        biocyc_key = substrate_map[sub]

        if(biocyc_key != None):
        
            record = compound_lookup(cur, biocyc_key)
            formula = record['chemical_formula']
            mw = record['molecular_weight']
        else:
            formula = '?'
            mw = '?'

        outputList.append((sub,biocyc_key,mw,formula))

    DatabaseUtils.writefile(('Product','Biocyc ID','Molecular Weight (Daltons)','Formula'), outputList, '\t', 'Substrate Information.txt')

def laser_substrate_to_metacyc(papers):

    try:
        connect = psycopg2.connect("dbname='biocyc' user='james' host='localhost' password='winkler'")
    except:
        print "I am unable to connect to the database"
        raise

    cur = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    seen = set()
    correspondence = {}
    counter = 0

    for paper in papers:

        doi = paper.doi
        if(doi in seen):
            continue
        seen.add(doi)

        mutants = paper.mutants
        for mutant in mutants:

            substrates = mutant.substrates

            for sub in substrates:

                sub = sub.strip().upper()
                if(sub in correspondence):
                    continue
                record = compound_lookup(cur, sub)

                if(record == None):
                    correspondence[sub] = 'None'
                else:
                    correspondence[sub] = record['unique_id']

        counter = counter + 1

        if(counter % 10 == 0):
            print 'Paper: %i' % counter

    output = []

    for product in correspondence:
        output.append((product, correspondence[product]))

    DatabaseUtils.writefile(('Product','Biocyc ID'), output, '\t', 'Substrate Name Converter.txt')

def laser_met_to_metacyc(papers, metabolite_cache = None):

    try:
        connect = psycopg2.connect("dbname='biocyc' user='james' host='localhost' password='winkler'")
    except:
        print "I am unable to connect to the database"
        raise

    if(metabolite_cache == None):
        metabolite_cache = {}

    cur = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    seen = set()

    correspondence = {}

    outputList = []

    counter = 0

    for paper in papers:

        doi = paper.doi
        if(doi in seen):
            continue
        seen.add(doi)

        mutants = paper.mutants

        for mutant in mutants:

            product = mutant.metabolite

            if(product.upper() in metabolite_cache):
                continue

            record = compound_lookup(cur, product)

            if(product.upper() in correspondence):
                continue

            if(record != None):
                biocyc_product = record['unique_id']
                formula = record['chemical_formula']
                mw = record['molecular_weight']
            else:
                biocyc_product = '_None'
                formula = '?'
                mw = '?'

            outputList.append((product.upper(), biocyc_product, mw, formula))


            if(record == None):
                correspondence[product.upper()] = 'None'
            else:
                correspondence[product.upper()] = record['unique_id']

        counter = counter + 1

        if(counter % 10 == 0):
            print 'Paper: %i' % counter

    output = []

    for product in correspondence:
        output.append((product.upper(), correspondence[product]))

    DatabaseUtils.writefile(('Product','Biocyc ID','Molecular Weight (Daltons)','Formula'), outputList, '\t', 'Product Name Converter.txt')


def compound_annotator(papers):

    try:
        connect = psycopg2.connect("dbname='biocyc' user='james' host='localhost' password='winkler'")
    except:
        print "I am unable to connect to the database"
        raise

    cur = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    met_cache = DatabaseUtils.load_metabolite_cache()

    seen = set()
    seen_compound = set()
    
    outputList = []

    counter = 0

    for paper in papers:

        doi = paper.doi
        if(doi in seen):
            continue
        seen.add(doi)

        mutants = paper.mutants

        for mutant in mutants:

            product = mutant.metabolite.upper()

            if(product not in seen_compound):

                seen_compound.add(product)

                if(product in met_cache and met_cache[product][0] != None):
                    biocyc_product = met_cache[product][0]

                    if(',' in biocyc_product):
                        continue
                    
                    record = compound_lookup(cur, biocyc_product)

                    if(record == None):
                        print product, biocyc_product
                    formula = record['chemical_formula']
                    mw = record['molecular_weight']
                else:
                    biocyc_product = '_None'
                    formula = '?'
                    mw = '?'

                outputList.append((product, biocyc_product, mw, formula))

        counter = counter + 1

        if(counter % 10 == 0):
            print 'Paper: %i' % counter

    DatabaseUtils.writefile(('Product','Biocyc ID','Molecular Weight (Daltons)','Formula'), outputList, '\t', 'Product Information.txt')
    
def yield_unit_conversion(s_mw, s_formula, p_mw, p_formula, pyield, current_unit):

    unit = current_unit.upper()

    output_unit = '% THEORETICAL'

    if('C' not in p_formula or 'C' not in s_formula):
        return (float('NaN'), None)

    p_carbon = float(p_formula['C'])
    s_carbon = float(s_formula['C'])

    theoretical_yield = s_carbon/p_carbon

    if(unit == 'MOL/MOL SUBSTRATE'):

        return (pyield * (p_carbon/s_carbon)/theoretical_yield * 100.0, output_unit)

    if(unit == 'MMOL/MOL SUBSTRATE'):

        return (pyield * (p_carbon/s_carbon)/theoretical_yield * 100.0/1000.0, output_unit)

    if(unit == 'g/g substrate'.upper()):

        new_yield = float(pyield) * (s_mw/p_mw) * (p_carbon/s_carbon)

        return (new_yield/theoretical_yield * 100.0, output_unit)

    if(unit == 'g/100 g substrate'.upper()):

        new_yield = float(pyield) * (s_mw/p_mw) * (p_carbon/s_carbon)

        return (new_yield/theoretical_yield, output_unit)

    if(unit == 'mg/g substrate'.upper()):

        #print unit

        new_yield = float(pyield) * (s_mw/p_mw) * (p_carbon/s_carbon)

        return (new_yield/theoretical_yield * 100.0/1000.0, output_unit)

    if(unit == 'ug/g substrate'.upper()):

        new_yield = float(pyield) * (s_mw/p_mw) * (p_carbon/s_carbon)

        return (new_yield/theoretical_yield * 100.0/(1000.0*1000.0), output_unit)

    if(unit == '% theoretical'.upper()):

        if(pyield <= 1.0):
            return (pyield * 100.0, output_unit)
        else:
            return (pyield, output_unit)

    if(unit == 'Cmol/Cmol substrate'.upper()):
        
        return ((pyield/theoretical_yield) * 100.0, output_unit)

    #print 'Unknown LASER yield unit: %s' % current_unit

    return (float('NaN'),None)

def titer_unit_conversion(p_mw, ptiter, current_unit):

    output_unit = 'mM'

    unit = current_unit.upper()

    if(unit == 'UM'):

        return (ptiter/1000.0, output_unit)

    if(unit == 'MM'):

        return (ptiter, output_unit)

    if(unit == 'M'):

        return (ptiter*1000.0, output_unit)

    if(unit == 'g/L'.upper()):

        return (ptiter/(p_mw) * 1000.0, output_unit)

    if(unit == 'mg/L'.upper()):

        return (ptiter/(p_mw), output_unit)

    if(unit == 'ug/L'.upper()):

        return (ptiter/(p_mw)/1000.0, output_unit)

    #print 'Unknown LASER titer unit: %s' % current_unit
    return (float('NaN'),None)

def unit_analysis_paper(paper, met_cache):

    mutants = paper.mutants

    y = []
    t = []

    for mutant in mutants:

        products = [x.upper() for x in mutant.metabolite]

        if(len(products) > 1):
            product = products[0]
            warnings.warn('need to allow multiple product-titer-yield tuples in the future, just using first product.')
        else:
            product = products[0]
        substrates = mutant.substrates
        titer_units = mutant.titer_units
        yield_units = mutant.yield_units

        if(not (product in met_cache and met_cache[product][1] != None and met_cache[product][1] != '?' and substrates[0].upper() in met_cache) or len(substrates) > 1 or met_cache[substrates[0].upper()][1] == None):
            y.append((float('Nan'), None, None))
            t.append((float('Nan'), None, None))
            continue

        (p_biocyc_key, p_mw, p_elem_dict) = met_cache[product]
        (s_biocyc_key, s_mw, s_elem_dict) = met_cache[substrates[0].upper()]

        if(product in met_cache and met_cache[product][0] != None):
            product = met_cache[product][0]

        try:
            (new_yield, unit) = yield_unit_conversion(s_mw, s_elem_dict, p_mw, p_elem_dict, mutant.final_yield, yield_units)
            y.append((new_yield, unit) + (product,))
        except ValueError:
            y.append((float('Nan'), None, None))

        try:
            (new_titer, unit) = titer_unit_conversion(p_mw, mutant.final_titer, titer_units)
            t.append((new_titer, unit) + (product,))
        except:
            t.append((float('Nan'), None, None))

    return t, y

'''

def unit_analysis(papers, numMK='NumberofMutants'):

    seen = set()

    met_cache = DatabaseUtils.load_metabolite_cache()

    pc_dict = DatabaseUtils.load_product_ontology(met_cache)

    titer_dict = defaultdict(set)
    yield_dict = defaultdict(set)

    test_product = 'LACTATE'

    product_frequency = defaultdict(int)

    t_outputList = []
    y_outputList = []

    for paper in papers:

        doi = paper.paperBacking['DOI']
        if(doi in seen):
            continue
        seen.add(doi)

        year = paper.paperBacking['Year']

        mutants = int(paper.paperBacking[numMK])

        for i in range(1,mutants+1):

            product = paper.mutantBacking[(i,'TargetMolecule')].upper()

            substrates = paper.mutantBacking[(i,'CarbonSource')]

            titer_units = paper.mutantBacking[(i,'TiterUnits')]
            yield_units = paper.mutantBacking[(i,'YieldUnits')]

            if(len(substrates) > 1):
                continue

            if(product in met_cache and met_cache[product][1] != '?' and substrates[0].upper() in met_cache):
                biocyc_product = met_cache[product][0]
            else:
                continue

            if(product in met_cache and met_cache[product][0] != None):
                (use, pclass) = pc_dict[biocyc_product]
                
            else:
                (use, pclass) = pc_dict[product]
                biocyc_product = product

            if(yield_units != ''):
                (p_biocyc_key, p_mw, p_elem_dict) = met_cache[product]
                (s_biocyc_key, s_mw, s_elem_dict) = met_cache[substrates[0].upper()]

                (new_yield, unit) = yield_unit_conversion(s_mw, s_elem_dict, p_mw, p_elem_dict, float(paper.mutantBacking[(i,'FinalYield')]), yield_units)

                #print 'Converted yield: %f, unit: %s' % (new_yield, unit)

                #for item in use:
                y_outputList.append((year, biocyc_product, new_yield))

            if(titer_units != ''):

                (p_biocyc_key, p_mw, p_elem_dict) = met_cache[product]
                (s_biocyc_key, s_mw, s_elem_dict) = met_cache[substrates[0].upper()]

                (new_titer, unit) = titer_unit_conversion(p_mw, float(paper.mutantBacking[(i,'FinalTiter')]), titer_units)

                #print 'Converted titer: %f, unit: %s' % (new_titer, unit)

                #try converting

                #for item in use:
                t_outputList.append((year,biocyc_product,new_titer))

    DatabaseUtils.writefile(('Year','Product','Titer (mM)'),t_outputList,'\t','PTiter vs Time.txt')
    DatabaseUtils.writefile(('Year','Product','Yield (%T)'),y_outputList,'\t','PYield vs Time.txt')

'''

def update_metabolite_cache():

    metabolite_cache = DatabaseUtils.load_metabolite_cache()

    papers = DatabaseUtils.getDatabase(years=(2014,))

    product_set = set()

    for paper in papers:

        for mutant in paper.mutants:

            if(paper.doi == '10.1016/j.enzmictec.2012.06.005'):
                print mutant.metabolite.upper()

            product_set.add(mutant.metabolite.upper())

    papers = DatabaseUtils.getDatabase(years=(2015,))

    if('RESVERATROL' not in product_set):
        raise AssertionError('zuh?')

    filtered_papers = []

    for paper in papers:
        for mutant in paper.mutants:

            if(mutant.metabolite.upper() == 'RESERVATROL'):
                print mutant.metabolite.upper() in product_set

            if(mutant.metabolite.upper() not in product_set):
                filtered_papers.append(paper)
                break

    laser_met_to_metacyc(filtered_papers,metabolite_cache = metabolite_cache)

    compound_annotator(filtered_papers)

#unit_analysis(papers)
            
