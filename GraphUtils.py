import os
import numpy
import scipy
import scipy.stats
from scipy.optimize import curve_fit

def get_cmap(N):

    import matplotlib.cm as cmx
    import matplotlib.colors as colors
    '''Returns a function that maps each index in 0, 1, ... N-1 to a distinct 
    RGB color.'''
    color_norm  = colors.Normalize(vmin=0, vmax=N-1)
    scalar_map = cmx.ScalarMappable(norm=color_norm, cmap='jet') 
    def map_index_to_rgb_color(index):
        return scalar_map.to_rgba(index)
    return map_index_to_rgb_color

def plot_network(G, title, filename, use_weights = False, use_labels = False, use_title=False, scaling_factor = 1.0):

    import networkx
    import matplotlib.pyplot as plt

    k = 1.5 / numpy.sqrt(len(G.nodes()))

    scale = 10

    pos = networkx.spring_layout(G, k=k, scale = scale)

    #node_colors = {}

    node_color_test = []

    for node in G.nodes():

        if('color' in G.node[node]):
            #node_colors[node] = G.node[node]['color']
            node_color_test.append(G.node[node]['color'])
        else:
            #default color is red (in pajek, matplotlib, etc)
            #node_colors[node] = 'r'
            node_color_test.append('r')

    if(use_weights == True):

        edge_weights = []

        for edge in G.edges():
            edge_weights.append(G[edge[0]][edge[1]]['weight'] * scaling_factor)

        networkx.draw_networkx_edges(G, pos = pos, edges = G.edges(), width=edge_weights)

    else:
        networkx.draw_networkx_edges(G, pos = pos, width=2.5)
        
    networkx.draw_networkx_nodes(G, pos = pos, node_color=node_color_test)

    if(use_labels == True):

        labels = {}

        for node in G.nodes():
            labels[node] = node

        networkx.draw_networkx_labels(G, pos = pos, labels = labels)

    if(use_title):
        plt.title(title)

    plt.xticks([])
    plt.yticks([])

    plt.ylim([-0.5,scale+0.5])
    plt.xlim([-0.5,scale+0.5])

    plt.tight_layout()

    plt.savefig(filename)
    plt.close()

def association_network(G, clusters, node_frequency, filename, node_scale = 0, edge_scale = 0, node_labels = True):
    
    import networkx
    import matplotlib.pyplot as plt

    max_edge_weight = 1.0
    max_node_weight = 1.0

    cluster_membership = set()

    if(node_frequency == {} or node_frequency == None):

        for node in G.nodes():
            node_frequency[node] = 1.0

    for edge in G.edges():

        cluster_membership.add(clusters[edge[0]])
        cluster_membership.add(clusters[edge[1]])

        if(G[edge[0]][edge[1]]['weight'] > max_edge_weight):
            max_edge_weight = G[edge[0]][edge[1]]['weight']

        if(node_frequency[edge[0]] > max_node_weight):
            max_node_weight = node_frequency[edge[0]]
        if(node_frequency[edge[1]] > max_node_weight):
            max_node_weight = node_frequency[edge[1]]

    color_map = get_cmap(len(cluster_membership))

    k = 1.5 / numpy.sqrt(len(G.nodes()))

    edge_weight = []

    if(node_scale > 0):
        max_node_weight = node_scale

    if(edge_scale > 0):
        max_edge_weight = edge_scale

    for edge in G.edges():

        weight = float(G[edge[0]][edge[1]]['weight'])/float(max_edge_weight)

        edge_weight.append((0,0,0,0.5))
        
    node_colors = []
    node_weights = []

    for node in G.nodes():

        cluster_id = clusters[node]
        color = color_map(cluster_id)
        node_colors.append(color)
        node_weights.append(float(node_frequency[node])/float(max_node_weight) * 200 + 50.0)

    scale = 10

    pos = networkx.spring_layout(G, k=k, scale = scale)

    labels = {}

    for node in G.nodes():
        labels[node] = node

    networkx.draw_networkx_edges(G, pos = pos, width=1.0, edge_color = edge_weight)
    networkx.draw_networkx_nodes(G, pos = pos, node_size = node_weights, node_color = node_colors)

    if(node_labels):

        networkx.draw_networkx_labels(G, pos = pos, labels = labels)

    #plt.title(title)

    plt.xticks([])
    plt.yticks([])

    plt.ylim([-0.5,scale+0.5])
    plt.xlim([-0.5,scale+0.5])

    plt.tight_layout()

    plt.savefig(filename)
    plt.close()

def stackedbar(value_keys, dict_array, xtick_labels, xlabel, ylabel, filename, rotation='horizontal', mapper = None):

    import matplotlib.pyplot as plt
    import matplotlib.cm as cmx
    import matplotlib.colors as colors
    import matplotlib.patches as mpatches

    color_mapper = get_cmap(len(value_keys))

    try:

        fig, ax = plt.subplots()

        index = numpy.arange(len(xtick_labels))
        bar_width = 0.55

        opacity = 0.8
        error_config = {'ecolor': '0.3'}

        previous_top = []
        counter = 0

        patch_array = []

        for key in value_keys:

            data_array = []

            for data_dict in dict_array:

                #extract the data in the specified order
                data_array.append(data_dict[key])

            if(previous_top == []):
                previous_top = [0] * len(data_array)

            plt.bar(index, data_array, bar_width, alpha=opacity, color=color_mapper(counter), align='center', bottom=previous_top)

            #need to keep track of where we are on the plot.

            for i in range(0, len(data_array)):
                previous_top[i] = previous_top[i] + data_array[i]

            artist = mpatches.Patch(color = color_mapper(counter), label=key)
            patch_array.append(artist)
            
                    
            counter = counter + 1

        plt.axis('tight')

        plt.legend(handles = patch_array, loc='lower center', bbox_to_anchor=(0.5, -0.5), ncol = 3)

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        if(mapper == None):
            plt.xticks(index, xtick_labels, rotation = rotation)
        else:
            plt.xticks(index, [mapper[k] for k in xtick_labels] ,rotation=rotation)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')
    except:
        plt.close('all')
        raise


def generate_heatmap(matrix, names, ylabel, filename, vmin = None, vmax = None, mapper = None, cmap = 'viridis'):

    import matplotlib.pyplot as plt

    try:

        fig, ax = plt.subplots()

        if(vmin != None and vmax != None):
            plt.pcolormesh(numpy.array(matrix), vmin = vmin, vmax = vmax, cmap = cmap)
        else:
            plt.pcolormesh(numpy.array(matrix), cmap = cmap)

        cb = plt.colorbar()

        plt.axis('tight')

        #ax.set_ylabel(names)
        #ax.set_xlabel(names)

        #plt.xlabel(names)

        if(mapper != None):
            names = [mapper[name] for name in names]
        else:
            names = list(names)

        plt.xticks(numpy.arange(len(names))+0.5, names, rotation='vertical')
        plt.yticks(numpy.arange(len(names))+0.5, names, rotation='horizontal')

        #ax.twinx().set_ylabel(ylabel)
        cb.set_label(ylabel)

        #plt.xlabel(index, names, rotation='vertical')
        ax.set_yticklabels(names, rotation='horizontal', va='center')

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:

        plt.close('all')
        raise


def generate_heatmap_asymmetric(matrix, x_names, y_names, ylabel, filename, vmin = None, vmax = None, mapper = None):

    import matplotlib.pyplot as plt

    try:

        fig, ax = plt.subplots()
        index = numpy.arange(len(x_names))

        if(vmin != None and vmax != None):
            plt.pcolormesh(numpy.array(matrix), vmin = vmin, vmax = vmax)
        else:
            plt.pcolormesh(numpy.array(matrix.transpose()), cmap=plt.get_cmap('Greens'))

        cb = plt.colorbar()

        plt.axis('tight')

        #ax.set_ylabel(names)
        #ax.set_xlabel(names)

        #plt.xlabel(names)

        if(mapper != None):
            x_names = [mapper.get(name,name) for name in x_names]
            y_names = [mapper.get(name,name) for name in y_names]
        else:
            x_names = list(x_names)
            y_names = list(y_names)

        plt.xticks(numpy.arange(len(x_names))+0.5, x_names, rotation='vertical')
        plt.yticks(numpy.arange(len(y_names))+0.5, y_names, rotation='horizontal')

        #ax.twinx().set_ylabel(ylabel)
        cb.set_label(ylabel)

        #plt.xlabel(index, names, rotation='vertical')
        ax.set_yticklabels(y_names, rotation='horizontal', va='center')

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:

        plt.close('all')
        raise

def lineplot(xvalues, yvalues, legends,  xlabel, ylabel, filename, mapper=None, yerrors = None, ymin = 0, xmin = 0):

    #yvalues is a list of lists, legends = len(yvalues), xvalues = len(yvalues[i])
    #if provided, yerrors also == len(yvalues)

    import matplotlib.pyplot as plt

    colors = get_cmap(len(yvalues))

    try:

        fig, ax = plt.subplots()

        y_max = 0

        if(yerrors == None):
            for y,i in zip(yvalues, range(0,len(yvalues))):
                ax.plot(xvalues, y, color = colors(i), label = legends[i])
        else:
            for y,i in zip(yvalues, range(0,len(yvalues))):
                plt.errorbar(xvalues,y, yerr = yerrors[i], color = colors(i), label = legends[i])

                if(max(y) > y_max):
                    y_max = max(y)

        ax.set_ylabel(ylabel)
        ax.set_xlabel(xlabel)

        handles, labels = ax.get_legend_handles_labels()
        ax.legend(handles, labels, loc='best')

        ax.set_ylim([ymin, y_max*1.1])

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:

        plt.close('all')
        raise

def linebar(keys, values, xvalues, yvalues, xlabel, y1label, y2label, filename, mapper=None, edgecolor='black', show_ticks = True):

    import matplotlib.pyplot as plt
    
    try:

        fig, ax = plt.subplots()

        index = numpy.arange(len(keys))
        bar_width = 0.7

        opacity = 0.8
        error_config = {'ecolor': '0.3'}

        ax.bar(index, values, bar_width,
                         alpha=opacity,
                         color='b',
                         align='center',
                        edgecolor = edgecolor)

        ax.set_ylabel(y1label)
        ax.set_xlabel(xlabel)

        ax.set_xticks(index)

        if(show_ticks == False):

            plt.tick_params(
            axis='x',          # changes apply to the x-axis
            which='both',      # both major and minor ticks are affected
            bottom='off',      # ticks along the bottom edge are off
            top='off',         # ticks along the top edge are off
            labelbottom='off') # labels along the bottom edge are off
            
        ax2 = ax.twinx()
        ax2.plot(index, yvalues, 'r-', linewidth=2.0)
        ax2.set_ylabel(y2label, color='red')

        #turns off offset on graph if change between points is small
        ax2.get_yaxis().get_major_formatter().set_useOffset(False)

        ax2.yaxis.label.set_color('red')
        ax2.tick_params(axis='y', colors='red')


        if(mapper == None):
            ax.set_xticklabels([str(key) for key in keys], rotation = 'vertical')
        else:
            ax.set_xticklabels([str(mapper[k]) for k in keys], rotation = 'vertical')

        ax.set_ylim(0, max(values)*1.1)
        ax2.set_ylim(min(yvalues)*0.9, max(yvalues)*1.1)

        #plt.axis('tight')
        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:
        plt.close('all')
        raise

def multi_bargraph(key_list, value_list, legend, xlabel, ylabel, filename, rotation='horizontal', mapper=None):

    import matplotlib.pyplot as plt

    color_order = ['c','r','y','g','b']

    try:

        fig, ax = plt.subplots()

        counter = 0

        width_bonus = 0.5

        lhandles = []

        for keys, values, color, lentry in zip(key_list, value_list, color_order, legend):

            index = numpy.arange(len(keys))
            bar_width = 0.35

            opacity = 0.8
            error_config = {'ecolor': '0.3'}

            #print keys, values, 'printing out desired values'

            rects1 = plt.bar(index-float(bar_width)/2.0+bar_width*counter, values, bar_width,
                             alpha=opacity,
                             color=color,
                             align='center',
                             label = lentry)

            lhandles.append(rects1)

            counter +=1

        plt.legend(handles = lhandles, loc='best', ncol = 1)

        plt.axis('tight')

        temp_ys = []

        for vlist in value_list:
            temp_ys.extend(vlist)

        #plt.ylim([0, max(temp_ys)*1.1])

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        if(mapper == None):
            plt.xticks(index, keys, rotation = rotation)
        else:
            plt.xticks(index, [mapper.get(k,k) for k in keys] ,rotation=rotation)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

        fhandle = open(filename+"_key labels.txt",'w')
        for key in keys:
            fhandle.write(key + "\n")
        fhandle.close()

    except:
        plt.close('all')
        raise
    
#here values is a iterable of lists
def multi_histogram(values, xlabel, ylabel, filename, rotation='horizontal',mapper=None,useMedianFilter=False, legends=None):

    import matplotlib.pyplot as plt
    import matplotlib.patches as mpatches

    colors = ['b','r','g','y']

    max_value = 0

    try:

        fig, ax = plt.subplots()

        first_bins = []

        patches = []

        for i in range(0, len(values)):

            if(not useMedianFilter):
                vrange = [min(values[i]), max(values[i])]
            else:
                vrange = [min(values[i]), 5.0*numpy.median(values[i])]

            if(i == 0):
                hist, first_bins = numpy.histogram(values[i], range=vrange)

            if(legends != None):
                patches.append(mpatches.Patch(color = colors[i], label=legends[i]))

            plt.hist(values[i], color=colors[i], bins = first_bins)

        plt.axis('tight')

        plt.legend(handles = patches, loc='upper right')

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:
        plt.close('all')
        raise

def cluster_plotting(data, idx, xlabel,ylabel, filename, rotation = 'horizontal',mapper=None):

    import matplotlib.pyplot as plt

    try:

        fig, ax = plt.subplots()
        plt.plot(data[idx==0,0],data[idx==0,1],'ob', data[idx==1,0],data[idx==1,1],'or')

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')
    except:
        plt.close('all')
        raise

def boxplot(xvalues, yvectors, xlabel, ylabel, filename, rotation='horizontal',mapper=None, showfliers=False):

    import matplotlib.pyplot as plt

    try:

        fig, ax = plt.subplots()

        rects1 = plt.boxplot(yvectors, labels = xvalues, showfliers=showfliers)         

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:
        plt.close('all')
        raise

def scatter(xvalues, yvalues, xlabel, ylabel, filename, rotation='horizontal',mapper=None, regression=None, equation_string = None, errors = None):

    import matplotlib.pyplot as plt

    try:

        fig, ax = plt.subplots()

        rects1 = plt.scatter(xvalues, yvalues, color='b')

        if(regression != None):

            lsp = numpy.linspace(min(xvalues), max(xvalues))

            if(regression == 'linear'):
            
                slope, intercept, Rsq, pvalue, stderr = scipy.stats.linregress(xvalues, yvalues)

                reg_values = [slope * pos + intercept for pos in lsp]

                equation_str = '%0.3f*t + %0.2f' % (slope, intercept)

                equation_label = 'Linear'

            if(equation_string != None):
                plt.annotate(equation_string, (0.05, 0.9), xycoords='axes fraction')
            else:
                plt.annotate(equation_str, (0.05, 0.9), xycoords='axes fraction')
            plt.annotate('R = {0:.3f}'.format(Rsq), (0.05, 0.85), xycoords='axes fraction')
            plt.plot(lsp, reg_values, color='r', label = equation_label, linewidth=3)

            if(errors != None):
                plt.plot(lsp, [x+errors[0] for x in reg_values], color='g', linewidth=3, linestyle='-.')
                plt.plot(lsp, [x-errors[1] for x in reg_values], color='g', linewidth=3, linestyle='-.')

        plt.axis('tight')

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:
        plt.close('all')
        raise

    

def histogram(values, xlabel, ylabel, filename, rotation='horizontal',mapper=None,useMedianFilter=False, bins = 10):

    import matplotlib.pyplot as plt

    try:

        if(useMedianFilter):
            plt.hist(values, color='b', bins=bins, range=[min(values), 5.0*numpy.median(values)])
        else:
            plt.hist(values, color='b', bins=bins)

        plt.axis('tight')

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

    except:
        plt.close('all')
        raise

def simple_bargraph(keys, values, xlabel, ylabel, filename, rotation='horizontal', mapper=None, yerr = None):

    import matplotlib.pyplot as plt

    try:

        fig, ax = plt.subplots()

        index = numpy.arange(len(keys))
        bar_width = 0.55

        opacity = 0.8
        error_config = {'ecolor': '0.3'}

        rects1 = plt.bar(index, values, bar_width,
                         alpha=opacity,
                         color='b',
                         align='center')

        if(yerr != None):
            plt.errorbar(index, values, yerr=yerr, fmt ='none', color = 'k')

        plt.axis('tight')

        if(yerr != None):
            max_index = numpy.where( values == max(values) )
            plt.ylim([0, max(values)+yerr[max_index[0]]*1.2])

        plt.xlabel(xlabel)
        plt.ylabel(ylabel)

        if(mapper == None):
            plt.xticks(index, keys, rotation = rotation)
        else:
            plt.xticks(index, [mapper[k] for k in keys] ,rotation=rotation)

        plt.savefig(filename, bbox_inches='tight')
        plt.close('all')

        fhandle = open(filename+"_key labels.txt",'w')
        for key in keys:
            fhandle.write(str(key) + "\n")
        fhandle.close()

    except:
        plt.close('all')
        raise
