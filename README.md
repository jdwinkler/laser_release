# README #

->![LASER Logo.png](https://bitbucket.org/repo/gzK9MA/images/3934049195-LASER%20Logo.png)<-

This is the official repository of the LASER database and software! Thank you for your interest in our work. LASER was originally published in [2015](http://www.sciencedirect.com/science/article/pii/S2214030115300031) and updated in 2016 with ~150 additional curated strains (link to be added). Our goal is to provided a consistent standard for representing the content of a metabolic engineering design and the associated growth conditions. This repository contains the basic software and the current 2014/2015 LASER database files for the development of improved tools and metrics in metabolic engineering.

### How do I get set up? ###

**What are LASER records?** Database files are flat, key-value text files design for expressiveness and relatively straightforward parsing. The list of allowed terms and their corresponding types are given in the InputTerms_typed.txt file under ../inputs/. Annotations are loosely typed and categorized as specified in AnnotationGrammar_typed.txt. All files are stored under ../database_store/2014 and ../database_store/2015. In most cases, LASER records were generated using a custom web tool that enforced standardized descriptions of each design aspect (mutation effects, types, growth conditions, and so forth). These terms are specified in the ../inputs/Term Usage.txt file.

**Parsing LASER.** LASER records are parsed using the Paper, Mutant, and Mutation classes within MetEngDatabase. DatabaseUtils.getDatabase method will return an array of Paper objects containing Mutants with Mutations, allowing you to interrogate each mutant/paper as desired without manually calling the parsing functions. Gene names and metabolites are automatically standardized using a many to one mapping; gene names are specified in ../inputs/Gene Name Mapping.txt, and metabolites in ../inputs/Compound Information.txt.

**Examples**. The DescriptiveStatistic.py file contains several examples of how to process and analyze the LASER database, as well as use the visualization functions in GraphUtils.py.

### Contribution Guidelines ###

If you would like to contribute data or code to LASER, please feel free to write us at the email addresses below.

### Who do I talk to? ###

* Creator and Lead: James D. Winkler (james.winkler@gmail.com)
* Developer and Co-author: Andrea L. Halweg Edwards (andrea.edwards@colorado.edu)
* Principal Investigator: Ryan T. Gill (rtg@colorado.edu)

## Acknowledgments ##

We thank the Department of Energy Genome Sciences program for funding, and the shoulders of giants that have helped lay the foundation for this work.

## License ##

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.