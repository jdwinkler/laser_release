__author__ = 'jdwinkler'

#standardizes gene representations as much as possible

class Standard:

    def __init__(self, names_to_standards_file, empty=False):

        if(not empty):

            names_to_standards = self.parseStandard(names_to_standards_file)

            self.standard = names_to_standards

        else:

            self.standard = {}

    def load_tuples(self, tuples):

        name_dict = {}

        for (key, species, value) in tuples:

            name_dict[(key, species)] = value
            name_dict[(key.upper(), species.upper())] = value

        self.standard = name_dict

    def parseStandard(self, filename):

        try:

            fhandle = open (filename, 'rU')
            lines = fhandle.readlines()
            fhandle.close()

            name_dict = {}

            for line in lines[1:]:

                tokens = line.strip().split('\t')
                name_dict[(tokens[0], tokens[1])]         = tokens[2]
                name_dict[(tokens[0].upper(), tokens[1].upper())] = tokens[2]

            return name_dict

        except:
            raise

    def convert(self, name, species, throw_error = False):

        if((name.upper(),species.upper()) in self.standard):
            return self.standard[(name.upper(),species.upper())]
        if((name,species) in self.standard):
            return self.standard[(name,species)]
        if(not throw_error):
            return name

        raise KeyError('Name %s not found in standardization dictionary.' % name)

    def filter_by_model(self, model, genes_to_manipulate):

        accessions = []
        missing = []

        std = self.standard

        for gene in genes_to_manipulate:

            if(gene.upper() in std and std[gene.upper()] in model.genes):
                accessions.append(std[gene.upper()])
            elif(gene in std and std[gene] in model.genes):
                accessions.append(std[gene])
            elif(gene in model.genes):
                accessions.append(gene)
            else:
                missing.append(gene)

        return accessions, missing

    #gene: mutation list for gene_mutation_dict
    def standardize_input_design(self, gene_mutation_dict):

        converted_dict = {}

        for node in gene_mutation_dict:
            if(node.upper() in self.standard):
                converted_dict[self.standard[node.upper()]] = gene_mutation_dict[node]
            elif (node in self.standard):
                converted_dict[self.standard[node]] = gene_mutation_dict[node]
            else:
                #deposit all new nodes anyway
                converted_dict[node.upper()] = gene_mutation_dict[node]

        #print 'converted dict', converted_dict

        return converted_dict

def get_designs_by_product(products, met_cache, papers, standards, species = None, native_only = False):

    original_names = set()
    converted_names= set()

    for product in products:

        original_names.add(product.upper())

        if(product.upper() in met_cache and met_cache[product.upper()][0] != None):
            converted_names.add(met_cache[product.upper()][0])

    compiled_mutations = []

    for paper in papers:

        mutants = paper.mutants
        for mutant in mutants:

            product = [x.upper() for x in mutant.metabolite]
            if(species != None and species.upper() != mutant.species.upper()):
                continue

            passes_test = False

            for p in product:
                if(p in original_names or (p in met_cache and met_cache[p][0] in  converted_names)):
                    passes_test = True

            if(passes_test):

                wrapper = {}

                mutations = mutant.mutations
                for mutation in mutations:

                    if(mutation.source.upper() != mutant.species.upper() and native_only):
                        continue

                    gene_name = mutation.name.upper()
                    source = mutation.source.upper()

                    #todo: convert genes to catalyzed reactions using biocyc
                    standard_name = standards.convert(gene_name, source).upper()
                    mutation_list = mutation.changes

                    #if you want to make species significant, do it here. todo
                    wrapper[standard_name] = (mutation_list, mutation.source)


                if(len(wrapper) != 0):

                    compiled_mutations.append((wrapper, paper.doi, mutant))

    return compiled_mutations

#returns a list of all products in the supplied category (from product ontology input file)
def generate_product_container(cat, met_cache, pc_dict):

    products = set()

    for key in met_cache:

        if(met_cache[key][0] != None):
            name = met_cache[key][0].upper()
        else:
            name = key.upper()

        if(name not in pc_dict):
            continue

        (uses, categories) = pc_dict[name]

        if(cat.upper() != 'ALL'):
            ucat = [c.upper() for c in categories]
            if(cat.upper() in ucat):
                products.add(key)
        else:
            products.add(key)

    return products

def generate_product_dicts(cats_of_interest, met_cache, pc_dict):

    product_list = []

    for cat in cats_of_interest:
        product_list.append(generate_product_container(cat, met_cache, pc_dict))

    return product_list

def output_mutation_list(product_dicts):

    mutation_tuples = []

    for mutation_dict in product_dicts:

        for source in mutation_dict:

            (mutations, organism) =  mutation_dict[source]
            mutation_tuples.append((source, organism, mutations))

    return mutation_tuples