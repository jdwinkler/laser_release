Category	Internal Name	Displayed Name	Type	Required?	Can select multiple?	Key for dropdown entries	List?	Delimiter	Default Value
gene	GeneName	Gene Name	text	1	0	None	0	None	Immortality Gene
gene	GeneNickname	Alternative Names	text	0	0	None	1	,	Soma,Immortality Vaccine
gene	GeneSource	Source Organism	text	1	0	None	0	None	Superman
gene	GeneMutation	Mutation Type	text	1	1	Mutation	1	,	oe,plasmid
gene	GeneEffect	Mutation Effects	text	1	1	Effect	1	,	cellexpander,new_rxn
gene	GeneOriginal	Original to this work	text	1	0	Original	0	None	yes
gene	EcNumber	EC Number	text	0	0	None	0	None	1.1.1.1
gene	gComments	Comments	text	0	0	None	0	None	Possibly fake
hidden	SubmitterName	Submitter Name	text	1	0	None	0	None	
hidden	SubmitterEmail	Submitter Email	text	1	0	None	0	None	
mutant	Species	Species	text	1	0	None	0	None	Homo sapiens
mutant	Subspecies	Strain	text	1	0	None	0	None	Chicken
mutant	CultureSystem	Culture System	text	1	0	Culture	0	None	batch_ft
mutant	Oxygen	Oxygenation	text	0	0	Air	0	None	aerobic
mutant	Medium	Medium	text	0	0	None	0	None	Minimal
mutant	Supplements	Supplements	text	0	0	None	0	None	Protein shakes
mutant	cvolume	Culture volume (ml)	float	1	0	None	0	None	1000
mutant	fvolume	Vessel volume (ml)	float	1	0	None	0	None	2000
mutant	pH	Culture pH	float	0	0	None	0	None	7
mutant	Temperature	Temperature (C)	float	0	0	None	0	None	45
mutant	Rotation	Rotation (rpm)	float	0	0	None	0	None	360
mutant	Name	Mutant Name	text	0	0	None	0	None	Destroyer of worlds
mutant	CarbonSource	Carbon Source	text	1	0	None	1	,	Glucose
mutant	Method	Mutant Method	text	1	1	Methods	1	,	antimetabolite
mutant	TolerancePhenotype	Tolerance Phenotype	text	0	0	None	1	,	Everything
mutant	TargetMolecule	Target Molecule	text	1	0	None	1	;	biomass
mutant	Objective	Objective	text	0	0	None	1	,	max(biomass)
mutant	Pathway	Pathway	text	0	0	None	1	,	Food to biomass (unbalanced)
mutant	FoldImprovement	Fold Improvement	float	0	0	None	0	None	10000
mutant	InitialTiter	Initial Titer	float	0	0	None	0	None	0.01
mutant	FinalTiter	Final Titer	float	0	0	None	0	None	100
mutant	TiterUnits	Titer Units	text	0	0	None	0	None	lives/hr
mutant	InitialYield	Initial Yield	float	0	0	None	0	None	0
mutant	FinalYield	Final Yield	float	0	0	None	0	None	0
mutant	YieldUnits	Yield Units	text	0	0	None	0	None	NA
mutant	mComments	Mutant Comments	text	0	0	None	1	,	Example
mutant	NumberofMutations	Number of Mutations	integer	1	0	None	0	None	1
paper	Title	Title	text	1	0	None	0	None	Hello World
paper	DOI	DOI	text	1	0	None	0	None	Cows
paper	Year	Year Published	integer	1	0	None	0	None	2014
paper	ResearchGroup	Corresponding Author	text	1	0	None	0	None	Superman
paper	Journal	Journal	text	1	0	None	0	None	Metabolic Engineering
paper	project_score	Project Difficulty	integer	1	0	Score	0	None	6
paper	score_reason	Difficulty explanation	text	1	1	Reason	1	,	number_of_mods
paper	project_doe	Project Management	text	1	1	DOE	1	,	none
paper	Tags	Tags	text	1	1	Tags	1	,	alcohols
paper	total	Total Mutants Created	integer	1	0	None	0	None	1000
paper	pComments	Paper Comments	text	0	0	None	0	None	Cows make milk
paper	NumberofMutants	Mutants to Enter	integer	1	0	None	0	None	2
