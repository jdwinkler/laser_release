Product	Biocyc ID
S-3HB	CPD-1843
R-3HB	CPD-335
3-HP	3-HYDROXY-PROPIONATE
4-hydroxyphenylacetic acid	4-HYDROXYPHENYLACETATE
5-aminolevulinic acid	5-AMINO-LEVULINATE
5-aminovalerate	5-AMINOPENTANOATE
aminovalerate	5-AMINOPENTANOATE
Abietadiene	--ABIETADIENE
acetalaldehyde	ACETALD
acetone	ACETONE
acetyl-CoA	ACETYL-COA
Adipic Acid	ADIPATE
2,3-butanediol	BUTANEDIOL
rr-2_3-BDO	BUTANEDIOL
n-butanol	BUTANOL
1-butanol	BUTANOL
Butyrate	BUTYRIC_ACID
butyrate	BUTYRIC_ACID
Cadaverine	CADAVERINE
L-carnitine	CARNITINE
4-courmaric acid	COUMARATE
Sclareol	CPD-10110
R-1-phenylethanol	CPD-10595
styrene	CPD-1092
alpha-santalene	CPD-11377
patchoulol	CPD-11399
Stearidonic Acid	CPD-12653
S-3HV	CPD-12765
R-3HV	CPD-12765
1_4-butanediol	CPD-13560
cubebol	CPD-13860
sabinene	CPD-14012
naringenin	CPD-14042
Naringenin	CPD-14042
Miltiradiene	CPD-14312
deoxyviolacein	CPD-14318
cis-abienol	CPD-14803
protopanaxadiol	CPD-15449
hydroxytyrosol	CPD-15887
mevalonate_phosphate	CPD-16873
perillyl_alcohol	CPD-17084
2-pyrone-4_6-dicarboxylic acid	CPD-184
Lycopene	CPD1F-114
lycopene	CPD1F-114
beta-carotene	CPD1F-129
astragalin	CPD1F-453
4-hydroxyphenyllactic acid	CPD-2222
1_3-propanediol	CPD-347
1-hexadecanol	CPD-348
2-(4-hydroxyphenyl)ethanol	CPD3O-4151
6-Deoxyerythronolide B	CPD-428
phenyllactic acid	CPD-5921
caffeic acid	CPD-676
triacetic acid lactone	CPD-6862
Beta-amyrin	CPD-6948
(2S)-pinocembrin	CPD-6991
Eriodictyol	CPD-6994
3-Methyl-1-butanol	CPD-7032
3-methyl-1-butanol	CPD-7032
isopentanol	CPD-7032
2-methyl-1-butanol	CPD-7033
2-phenylethanol	CPD-7035
amorphadiene	CPD-7554
valencene	CPD-7989
resveratrol	CPD-83
levopimaradiene	CPD-8695
bisabolene	CPD-8738
alpha-pinene	CPD-8754
a-farnesene	CPD-8764
1_2-Propanediol	CPD-8891
1_2-propanediol	CPD-8891
Linalool	CPD-8996
isoprene	CPD-9436
Isoprene	CPD-9436
Glucaric Acid	D-GLUCARATE
dihydroxyacetone	DIHYDROXYACETONE
dihydoxyacetone	DIHYDROXYACETONE
DHAP	DIHYDROXY-ACETONE-PHOSPHATE
D-lactate	D-LACTATE
xylonate	D-XYLONATE
ethylene	ETHYLENE-CMPD
ethanol	ETOH
Ethanol	ETOH
ferulic acid	FERULIC-ACID
L-fumarate	FUM
fumaric acid	FUM
geraniol	GERANIOL
L-glutamine	GLN
glutarate	GLUTARATE
glutaric acid	GLUTARATE
Glucaric acid	GLUTARATE
glycerol	GLYCEROL
Glycerol	GLYCEROL
glycerol 3-phosphate	GLYCEROL-3P
ethylene glycol	GLYCOL
n-hexanol	HEXANOL-CMPD
H2	HYDROGEN-MOLECULE
H2,ethanol	HYDROGEN-MOLECULE,ETOH
Isobutanol	ISOBUTANOL
isobutanol	ISOBUTANOL
isopropanol	ISO-PROPANOL
itaconic acid	ITACONATE
L-alanine	L-ALPHA-ALANINE
L-DOPA	L-DIHYDROXY-PHENYLALANINE
l-lactic acid	L-LACTATE
lactic acid	L-LACTATE
L-lactate	L-LACTATE
L-lysine	LYS
malate	MAL
N-acetylglucosamine	N-ACETYL-D-GLUCOSAMINE
N-acetylneuraminic acid	CPD0-1123
NADPH	NADPH
(2S)-naringenin	NARINGENIN-CMPD
Flavonoids	None
Fatty Acids	None
Fatty acids	None
Biodiesel	None
Carotenoids	None
Anteiso-branched Fatty Acids	None
Ni2+_intracellular	None
fatty acids	None
biodiesel	None
Products	None
FP-151 Protein	None
PHB	None
PHA	None
proteins	None
Biomass	None
Protein	None
muconic acid	None
Hg2+_intracellular	None
biomass	None
glycoglycerolipids	None
long-chain fatty acids	None
FAEE	None
Proteins	None
AvnD	None
Alanine-Glutamine	CPD-13403
isoprenol	None
membrane protein	None
Hydroxyfatty acids	None
4-Hydroxy-2(or 5)-ethyl-5(or 2)-methyl-3(2H)-furanone	None
amylase	None
plasmidDNA	None
DAMP4 peptide	None
Antibodies	None
Protein-Glycosylation	None
salvianic acid	None
Ethyl butyrate	None
Serine-rich proteins	None
D-hydroxyisovaleric acid	None
Cytoplasmic disulfide-bonded proteins	None
pDNA	None
protein	None
Silk	None
methyl ketones	None
C16:0 fatty alcohols	None
GFP	None
L-phenylalanine	PHE
phenylacetic acid	PHENYLACETATE
prenol	PRENOL
propanol	PROPANOL
1-propanol	PROPANOL
Putrescine	PUTRESCINE
pyruvate	PYRUVATE
riboflavin	RIBOFLAVIN
Shikimic acid	SHIKIMATE
Shikimic Acid	SHIKIMATE
Shikimate	SHIKIMATE
succinate	SUC
Taxadiene	TAXA-411-DIENE
taxadiene	TAXA-411-DIENE
L-threonine	THR
L-tyrosine	TYR
L-valine	VAL
vanillin	VANILLIN
violacein	VIOLACEIN
Xylitol	XYLITOL
xylitol	XYLITOL
xylitol, ribitol	XYLITOL,RIBITOL
