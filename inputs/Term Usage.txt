Methods	copy number amplification	Gene Amplification
Methods	antimetabolite	Antimetabolite selection
Methods	combinatorial	Combinatorial-scale
Methods	compartment	Compartmentalizing proteins
Methods	computational	Computational pathway design
Methods	evolution	Adaptive evolution
Methods	feedback control	Feedback control
Methods	modules	Functional modules
Methods	elementary mode	Elementary mode analysis
Methods	fba	Flux balance analysis
Methods	human	Human designed or inferred
Methods	oxygenation	Oxygenation optimization
Methods	in vitro optimization	In vitro optimization of pathway
Methods	library	Library Screening
Methods	regulatory engineering	Transcription factor manipulation
Methods	targeted_dna_editing	DNA editing (CRISPR/TALENs etc)
Methods	modeling	Metabolic modeling
Methods	metagenomics	Metagenomic screening
Methods	ensemble modeling	Ensemble metabolic modeling
Methods	sigma factor manipulation	Sigma factor engineering
Methods	protein engineering	Protein engineering
Methods	proteomics	Proteomic profiling
Methods	random mutagenesis	Random mutagenesis
Methods	facs screening	Rapid FACS screening
Methods	scaffold	Scaffolding proteins together
Methods	selection	Selection
Methods	regulatory simulation	Regulatory Simulation
Methods	statistical design of experiments	Statistical design of experiments
Methods	surfacedisplay	Surface display
Methods	cofactor switching	Cofactor switching
Methods	transcriptomics	Transcriptomic profiling
Methods	mediumoptimization	Medium optimization
Methods	recombination	Genetic recombination
Methods	metabolomics	Metabolic profiling
Methods	regulatory_engineering	Regulatory Engineering
Methods	liquidliquid	Two-phase (LL) process
Methods	enhancer	Non-promoter overexpression
Effect	cellexpander	C1: Increases cell size
Effect	global	C2: Global metabolic/regulatory effects
Effect	cofactor_switch	E1: Changes cofactor
Effect	substrate_switch	E2: Changes substrate
Effect	fbr	E3: Attenuates feedback resistance
Effect	raisedactivity	E4: Raises enzyme activity
Effect	loweredactivity	E5: Lowers enzyme activity
Effect	feedstock_use	E6: Enables use of non-native feedstock
Effect	new_rxn	F8: New reaction in metabolic network
Effect	linkage	F9: New link between met. modules
Effect	relaxed_ccr	E7: Removes catabolite repression
Effect	increased_flux	F1: Increased flux through pathway
Effect	decreased_flux	F2: Decreased flux through pathway
Effect	decreased_byproduct_form	F6: Reduces byproduct formation
Effect	decreased_prod_deg	F7: Reduces product degradation
Effect	local_con_increases	F3: Increases local [metabolite]
Effect	flux_channeling	F4: Channels flux between enzymes
Effect	redox_balance	F5: Improves redox balance
Effect	improved_translation	G1: Increases translation
Effect	improved_transcription	G2: Increases transcription
Effect	added_regulation	G3: Adds new regulatory links
Effect	complement	G4: Complements defective native gene
Effect	modulated	G5: Modulates gene expression
Effect	feedbackcontrol	G6: Feedback control of expression
Effect	importer	R2: Transports substrate into the cell
Effect	transporter	R1: Transports substrate out of cell
Effect	enzyme_tolerance_improvement	T1: Improves enzyme inhibitor resistance
Effect	strain_tolerance_improvement	T2: Improves strain inhibitor resistance
Effect	unknown	U1: Precise effect unknown
Effect	improved_growth	H1: improves cellular growth
Effect	opt_translation	G7: Optimizes translation
Effect	opt_transcription	G8: Optimizes transcription
Effect	insulator	G9: Insulates gene TX/TL
Mutation	compartmentalization	Targeted to organelle
Mutation	aa_snps	Amino acid change(s)
Mutation	amplified	Amplified copy number
Mutation	antisense	Antisense knockdown
Mutation	scaffold_binder	Binds to protein scaffold domains
Mutation	truncated	Truncated coding sequence
Mutation	codonoptimized	Codon optimization
Mutation	con	Constituitive expression
Mutation	del	Deletion
Mutation	frameshift	Frameshift mutation
Mutation	protein_fusion	Protein fusion
Mutation	duplication	Gene duplication
Mutation	indel	Indel
Mutation	intergenic	Intergenic mutation
Mutation	is_insertion	IS insertion
Mutation	mutated	Mutated allele
Mutation	nuc_snps	Nucleotide SNP(s)
Mutation	oe	Overexpression
Mutation	plasmid	Plasmid expression
Mutation	promoter_switch	Promoter swap
Mutation	rbs_tuned	RBS sequence tuning
Mutation	less_nadh_inhibition	Reduced NADH inhibition
Mutation	regulated	Regulated by X genes
Mutation	rep	Repression
Mutation	scaffold_bindee	Scaffold for protein assembly
Mutation	mrna_secondarystructure	Secondary structure altered
Mutation	sensor	Sensor
Mutation	integrated	Chromosomal integration
Mutation	terminated	Terminated
Tags	fragrances	Fragrance
Tags	aldehydes	Aldehydes
Tags	alternative feedstocks	Alternative feedstocks
Tags	redox balance	Redox balancing
Tags	aromatic amino acids	Aromatic amino acids
Tags	biodiesel	Biodiesel
Tags	biofuels	Biofuel
Tags	cosmetics	Cosmetics
Tags	diamine	Diamines
Tags	diols	Diols
Tags	pharmaceuticals	Pharmaceuticals
Tags	esters	Esters
Tags	flavanoids	Flavanoids
Tags	amino acids	Amino acids
Tags	heavy metals	Heavy metal bioremediation
Tags	hydrogen	Hydrogen
Tags	protein expression	Protein expression
Tags	protein solubility	Protein solubility
Tags	ketones	Ketones
Tags	organic acids	Organic acids
Methods	trna manipulation	tRNA Manipulation
Tags	anaerobic rescue	Anaerobic rescue
Methods	synthetic pathway	Synthetic pathway design
Tags	peptides	Peptides
Tags	phenolics	Phenolics
Tags	polymers	Polymer/monomers
Tags	surfactants	Surfactants
Tags	alcohols	Alcohols
Tags	amino sugars	Amino-sugars
Tags	dicarboxylic acids	Dicarboxylic acids
Tags	dna production	DNA (e.g. vaccines)
Tags	bulk chemicals	Bulk (low-value) chemical
Tags	essential production	Essential product
Tags	fine chemicals	Fine (high-value) chemical
Tags	toxic product	Toxic product
Tags	food additives	Food additive
Tags	glycosylation	Protein glycosylation
Tags	bioremediation	Sequesters/metabolizes pollutants
Tags	co-utilization	Substrate co-utilization
Tags	solvents	Solvent production
Tags	biomass	Biomass
Methods	central metabolism	Central metabolism re-engineering
Tags	terpenoids	Terpenoid production
Methods	kinetics	Kinetic strain design
Methods	thermodynamics	Thermodynamic strain design
Tags	vitamins	Vitamins
Tags	fatty acids	Fatty acids
Air	aerobic	Aerobic
Air	microaerobic	Microaerobic
Air	anaerobic	Anaerobic
Air	two-stage	Aerobic-Anaerobic
annotation	AA_SNPS	Enter amino acid residue changes (example: P29S):
annotation	NUC_SNPS	Enter SNPs (example: A348T):
annotation	IS_INSERTION	"List the IS element (IS1, IS5,etc) + location:"
annotation	INDEL	Specify the indel position (relative to start codon) and size:
annotation	intergenic	Specify nearby genomic features (gene1|gene2):
annotation	AMPLIFIED	"Enter the amount of amplification (1, 2, 5...):"
annotation	DUPLICATION	Enter the range of the duplicated nucleotides (X-Y):
annotation	SENSOR	"Enter the detected molecule (acyl-CoA, indole, etc):"
annotation	PROMOTER_SWITCH	Enter the replacement promoter (ex: PpflB):
annotation	PLASMID	"Plasmid copy number (High, medium, low, single) and the origin (high,pUC):"
annotation	OE	Enter the promoter used for expression:
annotation	CON	Enter the promoter used for expression:
annotation	COMPLEMENT	Enter the gene being complemented:
annotation	PROTEIN_FUSION	"Enter the fusion partner (KatF-LacZ, enter LacZ):"
annotation	REP	Enter knockdown technique:
annotation	MUTATED	Enter a general description of the mutation:
annotation	RBS_TUNED	Enter RBS sequence (use standard notation):
annotation	LOWEREDACTIVITY	Describe mutations that cause lower activity:
annotation	COMPARTMENTALIZATION	Enter targeted subcellular location:
annotation	INTEGRATED	"Method of integration (scarring, scarless, etc):"
annotation	TERMINATED	Enter terminator:
annotation	REGULATED	Enter regulator:
Culture		---------
Culture	bflask	Baffled flask
Culture	nbflask	Non-baffled flask
Culture	testtube	Test tube
Culture	bottle	Bottle
Culture	bioreactor	Fed-batch bioreactor
Culture	chemostat	Chemostat/continuous
Culture	agar	Agar Plates
gene	GeneName	Gene Name
gene	GeneNickname	Gene Nickname
gene	GeneMutation	Mutation Type
gene	GeneEffect	Mutation Effects
gene	GeneSource	Source Organism
gene	GeneOriginal	Original to this work
gene	EcNumber	EC Number
gene	gComments	Comments
mutant	Species	Species
mutant	Subspecies	Strain
mutant	CultureSystem	Culture System
mutant	Oxygen	Oxygenation
mutant	Medium	Medium
mutant	CarbonSource	Carbon Source
mutant	Supplements	Supplements
mutant	pH	Culture pH
mutant	Temperature	Temperature (C)
mutant	cvolume	Culture volume (ml)
mutant	fvolume	Vessel volume (ml)
mutant	Rotation	Rotation (rpm)
mutant	Name	Mutant Name
mutant	Method	Mutant Method
mutant	TolerancePhenotype	Tolerance Phenotype
mutant	TargetMolecule	Target Molecule
mutant	Objective	Objective
mutant	Pathway	Pathway
mutant	FoldImprovement	Fold Improvement
mutant	InitialTiter	Initial Titer
mutant	FinalTiter	Final Titer
mutant	TiterUnits	Titer Units
mutant	InitialYield	Initial Yield
mutant	FinalYield	Final Yield
mutant	YieldUnits	Yield Units
mutant	mComments	Mutant Comments
mutant	NumberofMutations	Number of Mutations
paper	Title	Title
paper	DOI	DOI
paper	Year	Year Published
paper	ResearchGroup	Corresponding Author
paper	Journal	Journal
paper	project_score	Project Difficulty
paper	score_reason	Difficulty explanation
paper	project_doe	Project Management
paper	Tags	Tags
paper	pComments	Paper Comments
paper	total	Total Mutants Created
paper	NumberofMutants	Mutants to Enter
DOE	none	Ad hoc (intuitive) design proposal
DOE	modeled	Model/computational guided design proposal 
DOE	statistical	Formal statistical design of experiments sampling
DOE	completionist	Exhaustive (combinatorial) model proposal/testing
DOE	parallel	Parallelized strain design/testing
DOE	serial	Serial (sequential) strain design/testing
DOE	random	Evolution/mutation generated designs
Score	0	1: White Circle (straightforward)
Score	1	2: Green Circle (slightly challenging)
Score	2	3: Blue Square (medium complexity)
Score	3	4: Black Diamond (intricate/highly complex)
Score	4	5: Double Black Diamond (extreme complexity)
Score	5	6: Orange Diamond (uniquely capable)
Reason	number_of_mods	Number of modifications
Reason	type_of_mods	Type of modifications
Reason	system_interactions	System-level interactions
Reason	construction	Capabilities of engineering tools
Reason	resources	"Required strains, reagents, or equipment"
Reason	expertise	Researcher expertise
Reason	scale	Number of designs to test/screen (scale)
Reason	organism	Tractability of host organism
Reason	other_engineering	Z1. Other engineering aspect
Reason	other_design	Z2. Other design aspect
Reason	other_general	Z3. Other issue
Original		---------
Original	yes	Yes
Original	no	No
