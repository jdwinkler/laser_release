Gene	Species	Functional Compression
&alpha;-ketoglutarate semialdehyde dehydrogenase	Azospirillum brasilense	&alpha;-ketoglutarate semialdehyde dehydrogenase
&beta;-alanine transaminase	Pseudomonas putida	&beta;-alanine transaminase
&beta;-amyrin synthase	Pisum sativum	&beta;-amyrin synthase
(+)-&alpha;-pinene synthase	Pinus taeda	(+)-&alpha;-pinene synthase
(R)-specific alcohol dehydrogenase	Lactobacillus kefir	(R)-specific alcohol dehydrogenase
(R)-specific enoyl-CoA hydratase	Pseudomonas aeruginosa	(R)-specific enoyl-CoA hydratase
2-deoxy-glucose 6-phosphate phosphatase	Saccharomyces cerevisiae	DOG1
2-pyrone synthase	Gerbera hybrida	2-pyrone synthase
3-dehydroshikimate dehydratase	Podospora pauciseta	3-dehydroshikimate dehydratase
3-hydroxy-3-methylglutaryl-coenzyme A reductase	Saccharomyces cerevisiae	HMG1
3-hydroxy-3-methylglutaryl-coenzyme A reductase	Delftia acidovorans	3-hydroxy-3-methylglutaryl-coenzyme A reductase
3-hydroxyacyl-CoA dehydrogenase	Ralstonia eutropha	3-hydroxyacyl-CoA dehydrogenase
3-hydroxybutyryl-CoA dehydrogenase	C. acetobutylicum	3-hydroxybutyryl-CoA dehydrogenase
3-hydroxypropanoate dehydrogenase	Escherichia coli	3-hydroxypropanoate dehydrogenase
3-octaprenyl-4-hydroxybenzoate decarboxylase	Enterobacter cloacae	3-octaprenyl-4-hydroxybenzoate decarboxylase
4-carboxy-2-hydroxymuconate-6-semialdehyde	Sphingobium	4-carboxy-2-hydroxymuconate-6-semialdehyde
4-coumarate 3-hydroxylase	Saccharothrix espanaensis	4-coumarate 3-hydroxylase
4-coumarate coenzyme A ligase	A. thaliana	4-coumarate-CoA ligase
4-coumarate coenzyme A ligase	Petroselinum crispus	4-coumarate-CoA ligase
4-coumarate-CoA ligase	Petroselinum crispum	4-coumarate-CoA ligase
4-coumarate-CoA ligase	Pcrispum	4-coumarate-CoA ligase
4-coumarate-coa ligase	Nicotiana	4-coumarate-CoA ligase
4-coumarate-CoA ligase	Hybrid poplar	4-coumarate-CoA ligase
4-coumarate-CoA ligase	A. thaliana	4-coumarate-CoA ligase
4-Coumarate-CoA ligase	Arabdopsis thaliana	4-coumarate-CoA ligase
4-hydroxybenzoate hydroxylase	Pseudomonas putida	4-hydroxybenzoate hydroxylase
4-hydroxybutyrate CoA-transferase	Clostridium kluyveri	4HBCT
4-hydroxybutyrate dehydrogenase	C. necator	4-hydroxybutyrate dehydrogenase
4-hydroxybutyryl-CoA transferase	P. gingivalis	4HBCT
4-hydroxyphenylacetate 3-monooxygenase	Saccharothrix espanaensis	6-carboxyhexanoate-CoA ligase
6-carboxyhexanoate-CoA ligase	Rhizobium trifolii	8-hydroxycopalyl diphosphate synthase
8-hydroxycopalyl diphosphate synthase	Cistus creticus	8HYDPPS
abienol synthase	Nicotiana tabacum	ABOLS
abietadiene synthase	Picea abies	ABDS
abietadiene synthase	Abies grandis	ABDS
ACC1	Saccharomyces cerevisiae	acs
accA	Escherichia coli	ACCS
accA	Photorhabdus luminescens	ACCS
accB	Escherichia coli	ACCS
accB	C. glutamicum	ACCS
accB	Photorhabdus luminescens	ACCS
accC	Escherichia coli	ACCS
accC	C. glutamicum	ACCS
accC	Photorhabdus luminescens	ACCS
accD	Escherichia coli	ACCS
accD	None	ACCS
accD	Photorhabdus luminescens	ACCS
aceA	Escherichia coli	aceA
aceB	Escherichia coli	aceB
aceE	Escherichia coli	pdh
aceF	Escherichia coli	pdh
aceK	Escherichia coli	aceK
acetaldehyde dehydrogenase	Escherichia coli	ACETLD
acetaldehyde dehydrogenase	Salmonella enterica	ACETLD
acetaldehyde dehydrogenase	Entamoeba histolytica	ACETLD
acetoacetate decarboxylase	C. acetobutylicum	ACCADC
alsD	Bacillus subtilis	acetolactate decarboxylase
alsS	Bacillus subtilis	ALSYN
acetolactate synthase	B. subtilis	ALSYN
acetolactate synthase	Bacillus subtilis	ALSYN
acetolactate synthase	Escherichia coli	ALSYN
acetolactate synthase	Bsubtilis	ALSYN
acetolactate synthase	None	ALSYN
acetolactate synthase	Saccharomyces cerevisiae	ILV2
acetolactate synthase	Enterobacter aerogenes	ALSYN
acetyl-CoA acetyltransferase	C. acetobutylicum	ACAT
acetyltransferase	Dianthus caryophyllus	AT
ackA	Escherichia coli	ackA
ackA	Aspergillus nidulans	ackA
ACL1	Yarrowia lipolytica	ATP-Citrate Lyase
ACL2	Yarrowia lipolytica	ATP-Citrate Lyase
acnA	Escherichia coli	acnA
acpP	Escherichia coli	acpP
acs	Escherichia coli	acs
acs	Salmonella enterica	acs
acyl-ACP thioesterase	Umbellularia californica	acyl-ACP thioesterase
acyl-ACP thioesterase	U. californica	acyl-ACP thioesterase
acyl-CoA hydrolase	M. musculus	acyl-CoA hydrolase
Acyl-CoA oxidase	Micrococcus luteus	Acyl-CoA oxidase
acyl-coa thioesterase	R. communis	acyl-coa thioesterase
acyl-coenzyme A-diacylglycerol acyltransferase	Acinetobacter baylyi	acyl-coenzyme A-diacylglycerol acyltransferase
ada	Escherichia coli	ada
adc	C. acetobutylicum	ACCADC
ADE3	Saccharomyces cerevisiae	ADE3
ADE3	Synthetic RNA	ADE3
adh	T. brocki	ADH
adh	C. beijerinckii	ADH
ADH1	Saccharomyces cerevisiae	ADH
ADH2	Saccharomyces cerevisiae	ADH
adh4	Saccharomyces cerevisiae	ADH
ADH5	Saccharomyces cerevisiae	ADH
ADH6	Saccharomyces cerevisiae	ADH
ADH7	Saccharomyces cerevisiae	ADH
adhA	L. lactis	ADH
adhA	Z. mobilis	ADH
AdhA	Lactococcus lactis	ADH
adhB	Z. mobilis	ADH
adhE	Escherichia coli	ADH
adhE2	C. acetobutylicum	ADH
AF157307	C. acetobutylicum	AF157307
agaR	Escherichia coli	agaR
AGT1	Saccharomyces cerevisiae	MAL11
ALD3	Saccharomyces cerevisiae	ALD3
ALD6	Saccharomyces cerevisiae	ALD6
aldA	Escherichia coli	aldA
aldH	Escherichia coli	aldH
alkB	Escherichia coli	alkB
alpha farnesene synthase	Malus x domestica	AFS
alpha-farnesene synthase	Malus X domestica, Escherichia coli	AFS
amorpha-4,11-diene synthase	Artemisia annua	ADS
amorpha-4,11-diene synthase	A. annua	ADS
amyA	Escherichia coli	amyA
apbE	Escherichia coli	apbE
araA	Escherichia coli	araA
araB	Escherichia coli	araB
araD	Escherichia coli	araD
araE	Escherichia coli	araE
arcA	Escherichia coli	arcA
arcB	Escherichia coli	arcB
argA	Escherichia coli	argA
argB	Escherichia coli	argB
argC	Escherichia coli	argC
argD	Escherichia coli	argD
argE	Escherichia coli	argE
argH	Escherichia coli	argH
argI	Escherichia coli	argI
argK	Escherichia coli	argK
Argonaute	Saccharomyces castelli	Argonaute
ARO10	Saccharomyces cerevisiae	ARO10
ARO3	Saccharomyces cerevisiae	ARO3
ARO4	Saccharomyces cerevisiae	ARO4
ARO80	Saccharomyces cerevisiae	ARO80
ARO9	Saccharomyces cerevisiae	ARO9
aroA	Escherichia coli	aroA
aroB	Escherichia coli	aroB
aroC	Escherichia coli	aroC
aroD	Escherichia coli	aroD
aroE	Escherichia coli	aroE
aroF	Escherichia coli	aroF
aroG	Escherichia coli	aroG
arogenate dehydrogenase	Z. mobilis	ArogD
aroH	Escherichia coli	aroH
aroK	Escherichia coli	aroK
aroL	Escherichia coli	aroL
aryl carboxylic acid reductase	Nocardia	ACAR
ascB	Escherichia coli	ascB
aspA	Escherichia coli	aspA
aspartate aminotransferase	Saccharomyces cerevisiae	AAT1
aspartate decarboxylase	Tribolium castaneum	ASPDC
aspC	Escherichia coli	ASPAT
atfA	Escherichia coli	atfA
atfA	A. baylyi	atfA
atoA	Escherichia coli	atoA
atoB	Escherichia coli	atoB
atoC	Escherichia coli	atoC
atoD	Escherichia coli	atoD
ATP-citrate lyase	Mus musculus	ATP-Citrate Lyase
ATP-citrate lyase	Yarrowia lipolytica	ATP-Citrate Lyase
atpF	Escherichia coli	atpF
atpH	Escherichia coli	atpH
avtA	Escherichia coli	avtA
Bay Laurel Thioesterase	U. californica	acyl-ACP thioesterase
bcd	C. acetobutylicum	bcd
Beas-a	Beauveria bassiana	Beas-a
Beas-b	Beauveria bassiana	Beas-b
beta-alanine-pyruvate aminotransferase	Bacillus cereus	BATPAT
Beta-amyrin synthase	Artemisia annua	Beta-amyrin synthase
beta-glucosidase	C. japonicus	beta-gluc
beta-glucosidase	Neurospora crassa	beta-gluc
beta-glucosidase	N. crassa	beta-gluc
beta-glucosidases	A. aculeatus	beta-gluc
BGL1	Saccharomyces cerevisiae	beta-gluc
BkdAB	Bacillus subtilis	2-oxoisovalerate dehydrogenase
bktB	Ralstonia eutropha	bktB
BNA2	Saccharomyces cerevisiae	BNA2
BTS1P	Saccharomyces cerevisiae	BTS1
budA	Enterobacter aerogenes	budA
budA	E. aerogenes	budA
budC	K. pneumoniae	budC
budC	Enterobacter aerogenes	budC
buk1	C. acetobutylicum	buk1
butanol dehydrogenase	Clostridium beijerinckii	BUTDH
butyryl-CoA dehydrogenase	Streptomyces collinus	BCOADH
butyryl kinase	C. acetobutylicum	BUTK
butyryl-CoA dehydrogenase	C. acetobutylicum	BCOADH
butyryl-CoA dehydrogenase-A	Clostridium beijerinckii	BCOADH
butyryl-CoA dehydrogenase-B	Clostridium beijerinckii	BCOADH
butyryl-CoA dehydrogenase-C	Clostridium beijerinckii	BCOADH
cadA	Escherichia coli	cadA
cadR	Escherichia coli	cadR
caffeic acid methyltransferase	A. thaliana	CAMT
caiA	Escherichia coli	caiA
casA	K. oxytoca	casA
casB	K. oxytoca	casB
catechol 1,2-dioxygenase	Pseudomonas putida	catechol 1,2-dioxygenase
catechol 1,2-dioxygenase	Candida albicans	catechol 1,2-dioxygenase
ccmA	Escherichia coli	ccmA
ccmB	Escherichia coli	ccmB
ccmC	Escherichia coli	ccmC
ccmD	Escherichia coli	ccmD
ccmE	Escherichia coli	ccmE
ccmF	Escherichia coli	ccmF
ccmG	Escherichia coli	ccmG
ccmH	Escherichia coli	ccmH
ccr	Streptomyces collinus	ccr
CdaR	Escherichia coli	CdaR
cel3A	C. japonicus	beta-gluc
Cel7	Bacillus D04	beta-gluc
cellobiohydrolases	T. reesi	CBIOSE-HYD
cellobiose dehydrogenase	H. insolens	CBIOSE-DH
cellobiose transporter-1	Neurospora crassa	CBIOSE-TRANS
cellodextrin transporter	N. crassa	CBIOSE-TRANS
cellulase	Erwinia carotovora	cellulase
chalcone isomerase	Medicago sativa	CHI
chalcone isomerase	A. thaliana	CHI
chalcone isomerase	M. sativa	CHI
chalcone synthase	Petunia X hybrida	CHS
chalcone synthase	Petunia hybrida	CHS
chalcone synthase	A. thaliana	CHS
chalcone synthase	P. hybrida	CHS
chalcone synthase	Hypericum androsaemum	CHS
chbA	Escherichia coli	chbA
chbB	Escherichia coli	chbB
chbC	Escherichia coli	chbC
chbF	Escherichia coli	chbF
chbG	Escherichia coli	chbG
chbR	Escherichia coli	chbR
cimA	Methanococcus jannaschii	cimA
cimA	M. jannaschii	cimA
cinnamate 4-hydroxylase	A. thaliana	C4H
Cinnamic acid 4-hydroxylase	A. thaliana	C4H
CipA5	C. thermocellum	CipA5
cis-aconitic acid decarboxylase	Aspergillus terreus	AADC
citF	Escherichia coli	citF
copalyl diphosphate synthase	Salvia miltiorrhiza	CPPPS
CPP kaurene synthase-like	Salvia miltiorrhiza	CPP kaurene synthase-like
cpxR	Escherichia coli	cpxR
crotonase	C. acetobutylicum	crotonase
crotonase	Clostridium beijerinckii	crotonase
crp	Escherichia coli	crp
crr	Escherichia coli	crr
crt	C. acetobutylicum	crt
crt1	C. kluyveri	crt1
crtB	Synechococcus	crtB
crtB	Pantoea ananatis	crtB
crtB	Erwinia uredovora	crtB
crtB	Xanthophyllomyces dendrorhous	crtB
crtE	Synechococcus	crtE
crtE	Pantoea ananatis	crtE
crtE	Xanthophyllomyces dendrorhous	crtE
crtI	Synechococcus	crtI
crtI	Pantoea ananatis	crtI
crtI	Erwinia uredovora	crtI
crtI	Xanthophyllomyces dendrorhous	crtI
crtY	Xanthophyllomyces dendrorhous	crtY
cscA	Escherichia coli	cscA
cscB	Escherichia coli	cscB
cscK	Escherichia coli	cscK
csrA	Escherichia coli	csrA
csrB	Escherichia coli	csrB
ctfA	C. acetobutylicum	ctfA
ctfB	C. acetobutylicum	ctfB
cubebol synthase	Citrus paradisi	cubebol synthase
cusS	Escherichia coli	cusS
cyaA	Escherichia coli	cyaA
cysK	Escherichia coli	cysK
Cytochrome P450 BM3	B. megaterium	P450
cytochrome P450 reductase	A. thaliana	P450
cytochrome P450 reductase	Arabdopsis thaliana	P450
d12-Desaturase	Mortierella alpina	desaturase
d5-desaturase	Paramecium tetraurelia	desaturase
d6-Desaturase	Ostreococcus tauri	desaturase
d6-Elongase	Mortierella alpina	elongase
d9-Desaturase	Mortierella alpina	desaturase
dadX	Escherichia coli	dadX
dak1	Saccharomyces cerevisiae	dak1
dak2	Saccharomyces cerevisiae	dak2
dammarenediol 12-hydroxylase	Panax ginseng	dammarenediol 12-hydroxylase
dammarenediol-II synthase	Panax ginseng	dammarenediol-II synthase
dapA	Escherichia coli	dapA
dapA	C. glutamicum	dapA
dapB	Escherichia coli	dapB
dapD	Escherichia coli	dapD
dapE	Escherichia coli	dapE
davA	Pseudomonas putida	davA
davB	Pseudomonas putida	davB
davD	Pseudomonas putida	davD
davT	Pseudomonas putida	davT
dehydroshikimate dehydratase	Podospora anserina	dehydroshikimate dehydratase
deoR	Escherichia coli	deoR
deoxyerythronolide B synthase	Saccharaoployspara erythrea	DETBS
dga1	Saccharomyces cerevisiae	dga1
dhaB	Escherichia coli	dhaB
dhaB	Klebsiella pneumoniae	dhaB
dhaD	Klebsiella pneumoniae	dhaD
dhaK	Escherichia coli	dhaK
dhaK	Citrobacter freundii	dhaK
dhaK	Klebsiella pneumoniae	dhaK
dhaL	Escherichia coli	dhaL
dhaM	Escherichia coli	dhaM
dhaT	Klebsiella pneumoniae	dhaT
Diapocarotenal synthase	Staphylococcus aureus	DPCS
DIC1	Saccharomyces cerevisiae	DIC1
Dicer	Saccharomyces castelli	Dicer
dihydropteridine reductase	Homo sapiens	DHPR
diphosphomevalonate decarboxylase	Saccharomyces cerevisiae	ERG19
DLS1	Saccharomyces cerevisiae	DLS1
dpp1	Saccharomyces cerevisiae	dpp1
dppA	Escherichia coli	dppA
dppB	Escherichia coli	dppB
dppC	Escherichia coli	dppC
dppD	Escherichia coli	dppD
dppF	Escherichia coli	dppF
dsbC	Escherichia coli	dsbC
dxr	B. subtilis	dxr
dxs	B. subtilis	dxs
dxs	Escherichia coli	dxs
D-xylose reductase	Candida boidinii	D-xylose reductase
D-xylose reductase	Aspergillus nidulans	D-xylose reductase
D-xylose reductase	Candida shehatae	D-xylose reductase
D-xylose reductase	Pichia stipitis	D-xylose reductase
D-xylose reductase	Yarrowia lipolytica	D-xylose reductase
D-xylose reductase	Scheffersomyces stipitis	D-xylose reductase
e14	Escherichia coli	e14
EC-6.2.1.3	Rhizobium trifolii	FA ligase
eco	Escherichia coli	eco
eda	Escherichia coli	eda
edd	Escherichia coli	edd
EFE	Pseudomonas syringae	EFE
endA	Escherichia coli	endA
endA	None	endA
endoglucanase	T. reesi	endoglucanase
entB	Escherichia coli	entB
entC	Escherichia coli	entC
ENZRXN-21721	Pichia stipitis	SORB-DH
epoxide hydrolase	Agrobacterium radiobacter	EPOX-HL
ERG1	Saccharomyces cerevisiae	ERG1
ERG10	Saccharomyces cerevisiae	ERG10
ERG12	Saccharomyces cerevisiae	ERG12
ERG13	Saccharomyces cerevisiae	ERG13
ERG19	Saccharomyces cerevisiae	ERG19
ERG20	Saccharomyces cerevisiae	ERG20
ERG7	Saccharomyces cerevisiae	ERG7
ERG8	Saccharomyces cerevisiae	ERG8
ERG9	Saccharomyces cerevisiae	ERG9
etfA	C. acetobutylicum	etfA
etfB	C. acetobutylicum	etfB
exuR	Escherichia coli	exuR
FAA1	Saccharomyces cerevisiae	FAA1
FAA2	Saccharomyces cerevisiae	FAA2
FAA4	Saccharomyces cerevisiae	FAA4
fabA	Escherichia coli	fabA
fabB	Escherichia coli	fabB
fabD	Escherichia coli	fabD
fabF	Escherichia coli	fabF
fabG	Escherichia coli	fabG
fabH	Escherichia coli	fabH
FabH2	Bacillus subtilis	FabH2
fabI	Escherichia coli	fabI
fabR	Escherichia coli	fabR
fabZ	Escherichia coli	fabZ
fadA	Escherichia coli	fadA
fadB	Escherichia coli	fadB
fadD	Escherichia coli	fadD
fadE	Escherichia coli	fadE
fadI	Escherichia coli	fadI
fadJ	Escherichia coli	fadJ
fadM	Escherichia coli	fadM
fadR	Escherichia coli	fadR
fapR	Bacillus subtilis	fapR
farnesyl diphosphate synthase	A. thaliana	FPPS
FAS1	Saccharomyces cerevisiae	FAS1
FAS2	Saccharomyces cerevisiae	FAS2
FAT1	Saccharomyces cerevisiae	FAT1
fatty acid O-methyltransferase	Mycobacterium marinum	FAOMT
fatty acyl-ACP reductase	Synechococcus elongatus	FAAR
fatty acyl-ACP thioesterase	Cocos nucifera	FAAT
fatty acyl-CoA reductase	Tyto alba	FAAR
fbp	Escherichia coli	fbp
fdh	Candida boidinii	fdh
fdh1	Saccharomyces cerevisiae	fdh
fdh1	Candida boidinii	fdh
fdhF	Escherichia coli	fdh
fdxA	C. pasteurianum	fdxA
feaB	Escherichia coli	feaB
ferredoxin NADP reductase	Bacillus subtilis	FDXN-NADPR
flavanone-3-hydroxylase	A. thaliana	F3H
flavonone synthase	A. thaliana	FLAVS
flhA	Escherichia coli	flhA
fnr	Escherichia coli	fnr
focA	Escherichia coli	focA
frdA	Escherichia coli	frdA
frdB	Escherichia coli	frdB
frdC	Escherichia coli	frdC
frdD	Escherichia coli	frdD
fruK	Escherichia coli	fruK
fucO	Escherichia coli	fucO
FUM1	Saccharomyces cerevisiae	FUMR
fumA	Escherichia coli	FUMR
fumB	Escherichia coli	FUMR
fumC	Escherichia coli	FUMR
gabD	Escherichia coli	gabD
gabD	Pseudomonas putida	gabD
gabD4	C. necator	gabD
gabT	Pseudomonas putida	gabT
GAL6	Saccharomyces cerevisiae	GAL
GAL80	Saccharomyces cerevisiae	GAL
galP	Escherichia coli	galP
galU	Escherichia coli	galU
gapA	Escherichia coli	gapA
gapN	Streptococcus mutans	gapN
garK	Escherichia coli	garK
gatC	Escherichia coli	gatC
gcvH	Escherichia coli	gcvH
gcvP	Escherichia coli	gcvP
gcvT	Escherichia coli	gcvT
gdh1	Saccharomyces cerevisiae	GDH
GDH2	Saccharomyces cerevisiae	GDH
gdhA	Escherichia coli	gdhA
geraniol synthase	Ocimum basilicum	GS
geranyl diphosphate synthase	Abies grandis	GGPPS
GPPS2	Abies grandis	GPPS
geranylgeranyl diphosphate synthase	Sulfolobus acidocaldarious	GGPPS
geranylgeranyl diphosphate synthase	Sulfolobus acidocaldarius	GGPPS
GGPP synthase	Saccharomyces cerevisiae	BTS1
GGPPS	T. chinensis	GGPPS
GGPPS	Abies grandis	GGPPS
GGPPS	Taxus canadensis	GGPPS
GGPPS	T. canadensis	GGPPS
GGPPS	Archaeoglobus fulgidus	GGPPS
gldA	Escherichia coli	gldA
glk	Escherichia coli	glk
glmS	Escherichia coli	glmS
GLN1	Saccharomyces cerevisiae	GLN1
GLN2	Saccharomyces cerevisiae	GLN2
glnA	Escherichia coli	glnA
glnB	Escherichia coli	glnB
glnE	Escherichia coli	glnE
glnH	Escherichia coli	glnH
glnL	Escherichia coli	glnL
glnP	Escherichia coli	glnP
glnQ	Escherichia coli	glnQ
glpD	Escherichia coli	glpD
glpF	Escherichia coli	glpF
glpK	Escherichia coli	glpK
glpR	Escherichia coli	glpR
glpX	Escherichia coli	glpX
gltA	Escherichia coli	gltA
gltB	Escherichia coli	gltB
gltD	Escherichia coli	gltD
glucosamine-6-phosphate acetyltransferase	Synechocystis	G6PAT
glucosamine-6-phosphate acetyltransferase	Saccharomyces cerevisiae	GNA1
gly43F	C. japonicus	gly43F
glyA	Escherichia coli	glyA
glycerol dehydratase	Escherichia coli	GLYCDHA
glycerol dehydratase	K. pneumoniae	GLYCDHA
glycerol dehydratase	Klebsiella pneumoniae	GLYCDHA
glycerol dehydrogenase	Hansenula polymorpha	GLYCDH
glyV	Escherichia coli	glyV
glyX	Escherichia coli	glyX
glyY	Escherichia coli	glyY
gor	Escherichia coli	gor
GPD1	Saccharomyces cerevisiae	GPD1
gpd2	Saccharomyces cerevisiae	gpd2
gpp1	Saccharomyces cerevisiae	GPPS
gpp2	Saccharomyces cerevisiae	GPPS
GPPS	Abies grandis	GPPS
GRE3	Saccharomyces cerevisiae	GRE3
groL	Escherichia coli	groL
groS	Escherichia coli	groS
grxB	Escherichia coli	grxB
guaB	Escherichia coli	guaB
gudD	Escherichia coli	gudD
GUT2	Saccharomyces cerevisiae	GUT2
gyrA	None	gyrA
gyrA	Escherichia coli	gyrA
haloalcohol dehydrogenase	Agrobacterium radiobacter	HALCODH
haloalkane dehalogenase	Rhodococcus rhodochrous	HALKDHALO
HAP4	Saccharomyces cerevisiae	HAP4
HbCoA dehydrogenase	Clostridium beijerinckii	hbd
hbd	C. acetobutylicum	hbd
hbd	P. gingivalis	hbd
hemA	Salmonella arizona	hemA
hemL	Escherichia coli	hemL
Hemoglobin	Vitreoscilla	Hemoglobin
HFA1	Saccharomyces cerevisiae	HFA1
hisH	Escherichia coli	hisH
HMGS	Saccharomyces cerevisiae	ERG13
hoxE	Synechocystis	hoxE
hoxF	Synechocystis	hoxF
hoxH	Synechocystis	hoxH
hoxU	Synechocystis	hoxU
hoxY	Synechocystis	hoxY
hpaB	Escherichia coli	hpaB
hpaC	Escherichia coli	hpaC
hyaA	Escherichia coli	hyaA
hyaB	Escherichia coli	hyaB
hybB	Escherichia coli	hybB
hybC	Escherichia coli	hybC
hycA	Escherichia coli	hycA
hydA	C. acetobutylicum	hydA
hydE	C. acetobutylicum	hydE
hydG	C. acetobutylicum	hydG
hydrogenase	C. acetobutylicum	hydrogenase
hydroxybutyryl-coA dehydrogenase	Ralstonia eutropha	hydroxybutyryl-coA dehydrogenase
hyfF	Escherichia coli	hyfF
hyfG	Escherichia coli	hyfG
icd	Escherichia coli	icd
ICL1	Saccharomyces cerevisiae	ICL1
iclR	Escherichia coli	iclR
IDH1	Saccharomyces cerevisiae	IDH1
IDH2	Saccharomyces cerevisiae	IDH2
idi	Escherichia coli	idi
IDI1	Saccharomyces cerevisiae	IDI1
IDP1	Saccharomyces cerevisiae	IDP1
mvaE	E. faecalis	mvaE
IgG	Homo sapiens	IgG
ILV3	Saccharomyces cerevisiae	ILV3
ILV5	Saccharomyces cerevisiae	ILV5
ilvA	Escherichia coli	ilvA
ilvA	C. glutamicum	ilvA
ilvB	Escherichia coli	ilvB
ilvC	Escherichia coli	ilvC
ilvD	Escherichia coli	ilvD
ilvE	Escherichia coli	ilvE
ilvH	Escherichia coli	ilvH
ilvI	Escherichia coli	ilvI
ilvN	Escherichia coli	ilvN
Ino1	Saccharomyces cerevisiae	INO1
inositol-1-phosphate synthase	Saccharomyces cerevisiae	INO1
insH	Escherichia coli	insH
ipdC	Azospirillum brasilense	ipdC
ipdV	Bacillus subtilis	ipdV
iraD	Escherichia coli	iraD
iscR	Escherichia coli	iscR
isochorismate pyruvate lyase	Pseudomonas fluorescens	isochorismate pyruvate lyase
ispA	Escherichia coli	ispA
ispD	Escherichia coli	ispD
ispF	Escherichia coli	ispF
ispG	Escherichia coli	ispG
ispH	Escherichia coli	ispH
ispS	Populus nigra	ispS
ispS	Pueraria montana	ispS
ispU	Escherichia coli	ispU
kdpD	Escherichia coli	kdpD
ketoisovalerate oxidoreductase	Beauveria bassiana	ketoisovalerate oxidoreductase
kivD	L. lactis	kivD
kivD	Lactococcus lactis	kivD
lacI	Escherichia coli	lacI
lacY	Escherichia coli	lacY
lacZ	Escherichia coli	lacZ
L-alanine dehydrogenase	Bacillus subtilis	L-alanine dehydrogenase
L-alanine dehydrogenase	Geobacillus stearothermophilus	L-alanine dehydrogenase
L-amino acid a-ligase	Bacillus subtilis	L-amino acid a-ligase
LAT1	Saccharomyces cerevisiae	LAT1
ldcC	Escherichia coli	ldcC
ldh	Streptococcus bovis	ldhA
ldh	Escherichia coli	ldhA
ldh	Lactobacillus casei	ldhA
ldh	Lactobacillus plantarum	ldhA
ldh	Bos taurus	ldhA
ldhA	Escherichia coli	ldhA
ldhA	Cupriavidus necator	ldhA
ldhD	Lactobacillus pentosus	ldhA
ldhL	Pediococcus acidilactici	ldhL
L-DOPA decarboxylase	Pig	L-DOPA decarboxylase
LEU2	Saccharomyces cerevisiae	LEU2
leuA	Escherichia coli	leuA
leuB	Escherichia coli	leuB
leuC	Escherichia coli	leuC
leuD	T. intermedius	leuD
leuD	Escherichia coli	leuD
leuH	T. intermedius	leuH
limonene synthase	Mentha spicata	limonene synthase
linalool synthase	Clarkia breweri	linalool synthase
linoleoyl-CoA desaturase	Mus musculus	linoleoyl-CoA desaturase
lldD	Escherichia coli	lldD
lpd	Escherichia coli	lpd
LPD1	Saccharomyces cerevisiae	LPD1
lpdA	K. pneumoniae	lpdA
lpdA	Escherichia coli	lpdA
lpp1	Saccharomyces cerevisiae	lpp1
lrp	Escherichia coli	lrp
lsrA	Escherichia coli	lsrA
lysA	Escherichia coli	lysA
lysC	Escherichia coli	lysC
lytic polysaccharide monooxygenases	T. aurantiacus	lytic polysaccharide monooxygenases
MAE1	Saccharomyces cerevisiae	MAE1
MAE1	Schizosaccharomyces pombe	MAE1
maeA	Escherichia coli	maeA
maeB	Escherichia coli	maeB
MAF1	Saccharomyces cerevisiae	MAF1
MAL1	Saccharomyces cerevisiae	MAL1
MAL11	Saccharomyces cerevisiae	MAL11
MAL2	Saccharomyces cerevisiae	MAL2
MAL3	Saccharomyces cerevisiae	MAL3
MAL4	Saccharomyces cerevisiae	MAL4
malate dehydrogenase	Rhizopus oryzae	mdh
malonyl-CoA reductase	Chloroflexus aurantiacus	MCOAR
malQ	Escherichia coli	malQ
malT	Escherichia coli	malT
maltose phosphorylase	L. sanfranciscensis	maltose phosphorylase
marA	Escherichia coli	marA
MaSp1	Nephila clavipes	MaSp1
MatA	Saccharomyces cerevisiae	MatA
matB	Rhizobium trifolii	matB
matC	Rhizobium trifolii	matC
mdh	Escherichia coli	MDH
MDH2	Saccharomyces cerevisiae	MDH
MDH3	Saccharomyces cerevisiae	MDH
merP	Tn501	merP
merT	Tn501	merT
metA	Escherichia coli	metA
methionine adenosyltransferase	M. musculus	MAT
methylmalonyl-CoA epimerase	Streptomyces coelicolor	gapN
metJ	Escherichia coli	metJ
mevalonate diphosphate decarboxylase	S. pneumoniae	mvaD
mevalonate kinase	S. pneumoniae	mvaK
mevalonate kinase	Saccharomyces cerevisiae	ERG12
MG517	M. genitalium	MG517
mgsA	Escherichia coli	mgsA
mgsA	Bacillus subtilis	mgsA
mhpD	Escherichia coli	mhpD
mlc	Escherichia coli	mlc
MIG1	Saccharomyces cerevisiae	MIG1
MLS1	Saccharomyces cerevisiae	MLS1
mltA	Escherichia coli	mltA
MNI1	Saccharomyces cerevisiae	MNI1
moaE	Escherichia coli	moaE
MPH2	Saccharomyces cerevisiae	MPH2
MPH3	Saccharomyces cerevisiae	MPH3
mqo	Escherichia coli	mqo
mreC	Escherichia coli	mreC
mtlA	Escherichia coli	mtlA
murE	Escherichia coli	murE
mvaA	Staphylococcus aureus	mvaA
mvaA	S. aureus	mvaA
mvaA	Saureus	mvaA
mvaD	S. pneumoniae	mvaD
mvaE	E. faecalis	mvaE
mvaE	Enterococcus faecalis	mvaE
mvaE	Enterococcus faecilis	mvaE
HMGR	Staphylococcus aureus	mvaA
mvaS	Staphylococcus aureus	mvaS
mvaS	S. aureus	mvaS
mvaS	E. faecalis	mvaS
mvaS	Enterococcus faecalis	mvaS
mvaS	Enterococcus faecilis	mvaS
MVD1	Saccharomyces cerevisiae	ERG19
MVK	Saccharomyces cerevisiae	ERG12
myo-inositol oxygenase	M. musculus	MIOX
myo-inositol oxygenase	Mus musculus	MIOX
NAD+ oxidase	E. faecalis	NADOX
nadK	Escherichia coli	nadK
nagA	Escherichia coli	nagA
nagB	Escherichia coli	nagB
nagC	Escherichia coli	nagC
nagE	Escherichia coli	nagE
nanA	Escherichia coli	nanA
nanE	Escherichia coli	nanE
nanK	Escherichia coli	nanK
napA	Escherichia coli	napA
napB	Escherichia coli	napB
napC	Escherichia coli	napC
napD	Escherichia coli	napD
napF	Escherichia coli	napF
napG	Escherichia coli	napG
napH	Escherichia coli	napH
narP	Escherichia coli	narP
NDE1	Saccharomyces cerevisiae	NDE1
NDE2	Saccharomyces cerevisiae	NDE2
ndh	Escherichia coli	ndh
nixA	Helicobacter pylori	nixA
nlpD	Escherichia coli	nlpD
None	None	None
None	Escherichia coli	None
None	Saccharomyces cerevisiae	None
npr	Escherichia coli	npr
NTE1	Saccharomyces cerevisiae	NTE1
ntt4	Protochlamydia amoebophila	ntt4
nudB	Escherichia coli	nudB
nudF	Bacillus subtilis	nudF
nudF	Escherichia coli	nudF
nuoC	Escherichia coli	nuoC
nupG	Escherichia coli	nupG
omega-3-Desaturase	Saccharomyces kluyveri	omega-3-Desaturase
O-methyltransferase	Homo sapiens	O-methyltransferase
ompC	Escherichia coli	ompC
otsA	Escherichia coli	otsA
otsB	Escherichia coli	otsB
P450	Mycobacterium HXN1500	P450
P450 flavonoid 3'-hydroxylase	Gerbera hybrid	F3H
P450 truncated reductase	Catharanthus roseus	reductase
P450_BM3	Bacillus megaterium	P450
paaF	Escherichia coli	paaF
paaG	Escherichia coli	paaG
paaH	Escherichia coli	paaH
paaJ	Escherichia coli	paaJ
PAD1	Saccharomyces cerevisiae	PAD1
pagP	Escherichia coli	pagP
PAL1	A. thaliana	PAL1
panB	Escherichia coli	panB
panD	Escherichia coli	panD
panE	L. lactis	panE
panZ	Escherichia coli	panZ
patA	Escherichia coli	patA
Patchoulol Synthase	Pogostemon cablin	Patchoulol Synthase
pck	Escherichia coli	pck
PCT540	C. propionicum	P540
PDA1	Saccharomyces cerevisiae	PDA1
PDB1	Saccharomyces cerevisiae	PDB1
pdc	Z. mobilis	pdc
PDC1	Saccharomyces cerevisiae	pdc
PDC2	Saccharomyces cerevisiae	pdc
PDC5	Saccharomyces cerevisiae	pdc
PDC6	Saccharomyces cerevisiae	pdc
pdh	Escherichia coli	pdh
pdhR	Escherichia coli	pdhR
PDX1	Saccharomyces cerevisiae	PDX1
Pea Metallothionein	Pisum sativum	Pea Metallothionein
pepA	Escherichia coli	pepA
pepB	Escherichia coli	pepB
pepD	Escherichia coli	pepD
pepN	Escherichia coli	pepN
pfkA	Escherichia coli	pfkA
pfkB	Escherichia coli	pfkB
pflB	Escherichia coli	pflB
pgi	Escherichia coli	pgi
pgk	Escherichia coli	pgk
pgmB	L. lactis	PGM
phaA	Ralstonia eutrophus	phaA
phaA	Ralstonia eutropha	phaA
phaA	C. necator	phaA
phaA	Cupriavidus necator	phaA
phaA	Ralstonia eutropha H16	phaA
phaB	Ralstonia eutropha	phaB
phaB	C. necator	phaB
phaB	Ralstonia eutropha H16	phaB
phaC	C. necator	phaC
phaC	Ralstonia eutropha	phaC
phaC	Cupriavidus necator	phaC
phaC	Pseudomonas stutzeri 1317	phaC
phaC	Ralstonia eutropha H16	phaC
phaC2	Pseudomonas aeruginosa	phaC
PhaG	Pseudomonas putida	PhaG
phaJ	Pseudomonas putida	phaJ
phbA	Ralstonia eutropha	phbA
phbB	Ralstonia eutropha	phbB
pheA	Escherichia coli	pheA
pheL	Escherichia coli	pheL
phenylalanine ammonia lyase	Rhodotorula glutinis	PAL
phenylalanine ammonia lyase	A. thaliana	PAL
phenylalanine ammonia lyase	Rhodosporidium toruloides	PAL
PHO13	Saccharomyces cerevisiae	PHO13
phosphate butyryltransferase	C. acetobutylicum	ptb
phosphoglucomutase	Nocardia farcinia	PGM
phosphomevalonate kinase	S. pneumoniae	ERG8
phosphopantetheine transferase	Escherichia coli	entD
plant thioesterase	Cinnamonum camphorum	thioesterase
pldA	Escherichia coli	pldA
plsC	Escherichia coli	plsC
PMVK	Saccharomyces cerevisiae	ERG8
pncB	Salmonella typhimurium	pncB
pntA	Escherichia coli	pntA
pntB	Escherichia coli	pntB
potE	Escherichia coli	potE
POX1	Saccharomyces cerevisiae	POX1
poxB	Escherichia coli	poxB
PP_0763	Pseudomonas aeruginosa	PP_0763
ppc	Escherichia coli	ppc
ppdA	Klebsiella oxytoca	ppdA
ppdB	Klebsiella oxytoca	ppdB
ppdC	Koxytoca	ppdC
pps	Escherichia coli	pps
ppsA	Escherichia coli	ppsA
PRB1	Saccharomyces cerevisiae	PRB1
PROTEINSCAFFOLD	Synthetic	ProteinBindingDomainPolymer
ProteinBindingDomainPolymer	HumanDesigned	ProteinBindingDomainPolymer
ProteinBindingDomainPolymer	Designed	ProteinBindingDomainPolymer
protocatechuate 4,5-dioxygenase	Sphingobium	protocatechuate 4,5-dioxygenase
prpB	Escherichia coli	prpB
prpC	Escherichia coli	prpC
prpD	Escherichia coli	prpD
prpE	Salmonella enterica	prpE
prpE	Escherichia coli	prpE
prpR	Escherichia coli	prpR
pta	Escherichia coli	pta
pta	Bacillus subtilis	pta
ptb	C. acetobutylicum	ptb
pterin-4alpha-carbinolamine dehydratase	Homo sapiens	pterin-4alpha-carbinolamine dehydratase
ptsA	Escherichia coli	ptsA
ptsG	Escherichia coli	ptsG
ptsH	Escherichia coli	ptsH
ptsI	Escherichia coli	ptsI
ptsN	Escherichia coli	ptsN
purF	Escherichia coli	purF
putA	Escherichia coli	putA
putative enoyl-CoA hydratase	C. acetobutylicum	putative enoyl-CoA hydratase
puuA	Escherichia coli	puuA
puuP	Escherichia coli	puuP
PXA1	Saccharomyces cerevisiae	PXA1
PYC2	Saccharomyces cerevisiae	pyc
pykA	Escherichia coli	pykA
pykF	Escherichia coli	pykF
pyrD	Escherichia coli	pyrD
pyruvate carboxylase	Bacillus subtilis	pyc
pyruvate carboxylase	Rhizopus oryzae	pyc
pyruvate carboxylases	Saccharomyces cerevisiae	pyc
pyruvate_carboxylase	L. lactis	pyc
qutC	Aspergillus nidulans	qutC
R-3HB-CoA dehydrogenase	Ralstonia eutropha	hbd
rcsB	Escherichia coli	rcsB
rcsC	Escherichia coli	rcsC
rcsD	Escherichia coli	rcsD
recA	Escherichia coli	recA
rep	Escherichia coli	rep
resveratrol synthase	Vitis vinifera	resveratrol synthase
rfbA	Escherichia coli	rfbA
rfbB	Escherichia coli	rfbB
rfbC	Escherichia coli	rfbC
rfbD	Escherichia coli	rfbD
rfbX	Escherichia coli	rfbX
rhtA	Escherichia coli	rhtA
rhtB	Escherichia coli	rhtB
rhtC	Escherichia coli	rhtC
ribA	Escherichia coli	ribA
ribB	Escherichia coli	ribB
ribC	Escherichia coli	ribC
ribD	Escherichia coli	ribD
ribE	Escherichia coli	ribE
ribF	Escherichia coli	ribF
RKI1	Saccharomyces cerevisiae	RKI1
rodA	Escherichia coli	rodA
ROX1	Saccharomyces cerevisiae	ROX1
RPA49	Saccharomyces cerevisiae	RPA49
RPD3	Saccharomyces cerevisiae	RPD3
rpe	Escherichia coli	rpe
RPE1	Saccharomyces cerevisiae	RPE1
rph	Escherichia coli	rph
rpmE	Escherichia coli	rpmE
rpoA	Escherichia coli	rpoA
rpoB	Escherichia coli	rpoB
rpoC	Escherichia coli	rpoC
rpoD	Escherichia coli	rpoD
rpoS	Escherichia coli	rpoS
rppH	Escherichia coli	rppH
rspA	Escherichia coli	rspA
rspB	Escherichia coli	rspB
rssB	Escherichia coli	rssB
S-3HB-CoA dehydrogenase	C. acetobutylicum	hbd
sabinene synthase	Salvia pomifera	sabinene synthase
sad	Escherichia coli	sad
salicylate 1-monoxygase	Pseudomonas putida	salicylate 1-monoxygase
Santalene Synthase	Solanum habrochaites	Santalene Synthase
sclareol synthase	Salvia sclarea	sclareol synthase
scpA	Escherichia coli	scpA
scpB	Escherichia coli	scpB
scpC	Escherichia coli	scpC
sdaA	Escherichia coli	sdaA
sdaB	Escherichia coli	sdaB
SDH1	Saccharomyces cerevisiae	SDH1
SDH2	Saccharomyces cerevisiae	SDH2
SDH3	Saccharomyces cerevisiae	SDH3
sdhA	Escherichia coli	sdhA
sdhB	Escherichia coli	sdhB
sdhC	Escherichia coli	sdhC
sdhD	Escherichia coli	sdhD
SER3	Saccharomyces cerevisiae	SER3
SER33	Saccharomyces cerevisiae	SER33
serA	Escherichia coli	serA
SFC1	Saccharomyces cerevisiae	SFC1
sfp	B. subtilis	sfp
SFP	Escherichia coli	SFP
sgcE	Escherichia coli	sgcE
short chain thioesterases	Rattus norvegicus	thioesterases
SNF2	Saccharomyces cerevisiae	SNF2
SNR84	Saccharomyces cerevisiae	SNR84
SOL3	Saccharomyces cerevisiae	SOL3
speC	Escherichia coli	speC
speE	Escherichia coli	speE
speF	Escherichia coli	speF
speG	Escherichia coli	speG
sthA	Escherichia coli	sthA
Stilbene synthase	Vitis vinifera	stilbene synthase
stilbene synthase	Vitis vinifera, Arabdopsis thaliana	stilbene synthase
SUC2	Saccharomyces cerevisiae	SUC2
sucA	M. bovis	sucA
sucA	Escherichia coli	sucA
sucB	Escherichia coli	sucB
sucC	Escherichia coli	sucC
sucD	Escherichia coli	sucD
sucD	P. gingivalis	sucD
sucD	Clostridium kluyveri	sucD
sulA	Escherichia coli	sulA
surfactin phosphopantetheinyl transferase	Bacillus subtilis	surfactin phosphopantetheinyl transferase
SUT1	Saccharomyces cerevisiae	SUT1
SyntheticBindingDomain	Synthetic	SyntheticBindingDomain
T7 Polymerase	T7 Phage	T7
T7 RNA polymerase	T7 phage	T7
talA	Escherichia coli	talA
talB	Escherichia coli	talB
taxadiene synthase	Aradopsis thaliana	taxadiene synthase
taxadiene synthase	Taxus chinensis	taxadiene synthase
tdcC	Escherichia coli	tdcC
tdcD	Escherichia coli	tdcD
tdcE	Escherichia coli	tdcE
tdh	Escherichia coli	tdh
ter	Treponema denticola	ter
ter	T. denticola	ter
ter	Terponema denticola DSM14222	ter
TES1	Saccharomyces cerevisiae	TES1
tesA	Escherichia coli	tesA
tesB	Escherichia coli	tesB
TGL3	Saccharomyces cerevisiae	TGL3
TgtR	Pseudomonas putida	TgtR
thioesterase	H. influenzae	thioesterase
thiolase	C. acetobutylicum	thiolase
thiolase	Ralstonia eutropha	thiolase
thl	C. acetobutylicum	thl
thrA	Escherichia coli	thrA
thrB	Escherichia coli	thrB
thrC	Escherichia coli	thrC
thrL	Escherichia coli	thrL
TKL1	Saccharomyces cerevisiae	TKL1
TKL2	Saccharomyces cerevisiae	TKL2
tktA	Escherichia coli	tktA
tnaA	Escherichia coli	tnaA
TPI1	Saccharomyces cerevisiae	TPI1
tpiA	Escherichia coli	tpiA
AgBIS	Abies grandis	TPS-Bis
TPS-Bis	Abies grandis	TPS-Bis
TPS-Bis	A. grandis,Saccharomyces cerevisiae	TPS-Bis
TPS-LAS	G. biloba	TPS-LAS
transaldolase	Pichia stipitis	transaldolase
transaldolase	Saccharomyces cerevisiae	transaldolase
trans-cinnamate decarboxylase	Saccharomyces cerevisiae	PAD1
trans-enoyl-CoA reductase	E. gracilis	ter
trans-enoyl-CoA reductase	T. denticola	ter
trpD	Escherichia coli	trpD
trpE	Escherichia coli	trpE
trpL	Escherichia coli	trpL
trpR	Escherichia coli	trpR
trxA	Escherichia coli	trxA
trxB	Escherichia coli	trxB
TUP1	Saccharomyces cerevisiae	TUP1
typA	Escherichia coli	typA
type I fatty acid synthase	Homo sapiens	FAS-T1
tyrA	Escherichia coli	tyrA
tyramine oxidase	Micrococcus luteus	tyramine oxidase
tyrB	Escherichia coli	tyrB
tyrosine ammonia-lyase	Saccharothrix espanaensis	TAL
tyrosine ammonia-lyase	R. glutinis	TAL
tyrosine ammonia-lyase	Rhodotorula glutinis	TAL
tyrosine ammonia-lyase	A. thaliana	TAL
tyrosine ammonia-lyase	R. sphaeroides	TAL
tyrosine hydroxylase	M. musculus	TYR-HYDOXY
tyrR	Escherichia coli	tyrR
ubiA	Escherichia coli	ubiA
ubiC	Escherichia coli	ubiC
udh	Pseudomonas syringae	UDH
UDP-D-glucose:delphinidin 3-O-&beta;-D-glucosyltransferase	Glycine max	UDP-GLYT
UDP-glycosyltransferase	Arabdopsis thaliana	UDP-GLYT
UDP-glycosyltransferase	A. thaliana	UDP-GLYT
UDP-N-acetylglucosamine 2-epimerase	Synechocystis	UDP-NAG-EPI
umpH	Escherichia coli	umpH
UPC2	Saccharomyces cerevisiae	UPC2
uronate dehydrogenase	Agrobacterium tumefaciens	UDH
ushA	Escherichia coli	ushA
uspC	Escherichia coli	uspC
uxaC	Escherichia coli	uxaC
valencene synthase	Citrus sinensis	VALCS
vanillin aminotransferase	Capsicum chinense	VAMT
vioA	Chromobacterium violaceum	vioA
vioB	Chromobacterium violaceum	vioB
vioC	Chromobacterium violaceum	vioC
vioD	Janthinobacterium lividum	vioD
vioE	Chromobacterium violaceum	vioE
VTA1	Saccharomyces cerevisiae	VTA1
XYL1	Pichia stipis	XR
xylA	Escherichia coli	xylA
xylA	Clostridium phytofermentans	xylA
xylA	Piromyces	xylA
xylA	Saccharomyces cerevisiae	xylA
xylB	Escherichia coli	xylB
xylB	Caulobacter crescentus	xylB
xylF	Escherichia coli	xylF
xylG	Escherichia coli	xylG
xylH	Escherichia coli	xylH
xylitol dehydrogenase	Candida albicans	XDH
xylitol dehydrogenase	Candida tropicalis	XDH
xylitol dehydrogenase	Pichia stipis	XDH
xylitol dehydrogenase	Pichia stipitis	XDH
xylitol dehydrogenase	Yarrowia lipolytica	XDH
xylitol dehydrogenase	Scheffersomyces stipitis	XDH
xylose dehydrogenase	Caulobacter crescentus	XDH
xylose reductase	Scheffersomyces stipitis	XR
xylulose kinase	Saccharomyces cerevisiae	XKS1
xylulose kinase	Pichia pastoris	XK
xylulose kinase	Pichia stipis	XK
xylulose kinase	Pichia stipitis	XK
xylulose kinase	Yarrowia lipolytica	XK
xylulose kinase	Scheffersomyces stipitis	XK
xylulose-5-phosphate phosphoketolase	Aspergillus nidulans	X5PPK
Xyn10B	C. stercorarium	Xyn10B
yahK	Escherichia coli	yahK
ybaS	Escherichia coli	ybaS
yceG	Escherichia coli	yceG
ydbK	Escherichia coli	ydbK
ydfG	Escherichia coli	ydfG
ydiB	Escherichia coli	ydiB
ydjG	Escherichia coli	ydjG
yeast metallothionein	Saccharomyces cerevisiae	yeast metallothionein
yeiG	Escherichia coli	yeiG
yfeH	Escherichia coli	yfeH
ygaH	Escherichia coli	ygaH
ygaZ	Escherichia coli	ygaZ
ygfT	Escherichia coli	ygfT
yhbJ	Escherichia coli	yhbJ
yhcA	Escherichia coli	yhcA
YIA6	Saccharomyces cerevisiae	YIA6
yiaY	Escherichia coli	yiaY
yjfP	Escherichia coli	yjfP
yjgB	Escherichia coli	yjgB
yjjM	Escherichia coli	yjjM
YLR042C	Saccharomyces cerevisiae	YLR042C
yneH	Escherichia coli	yneH
yojO	Escherichia coli	yojO
ypl062w	Saccharomyces cerevisiae	ypl062w
ypl064w	Saccharomyces cerevisiae	ypl064w
yqeF	Escherichia coli	yqeF
yqhD	Escherichia coli	yqhD
ytjC	Escherichia coli	ytjC
zwf	Escherichia coli	zwf
ZWF1	Saccharomyces cerevisiae	ZWF1
M11214	C. pasteurianum	M11214
noxE	Lactococcus lactis	NADH oxidase
nuo	Escherichia coli	nuoA
sdh	Escherichia coli	sdhA
TAXADIENE 5ALPHA-HYDROXYLASE	Taxus cuspidata	taxadiene 5&alpha;-hydroxylase
ARO7	Saccharomyces cerevisiae	YPR060C