#fake database class
from collections import defaultdict
import os
import re
import warnings
import math
from Standardization import Standard

class MetEngDatabase:


    def __init__(self,filePaths,numMutantsKey, numMutationsKey, paperTerms,mutantTerms,geneTerms, parsingRules, typeMap, knownMutations, standard_obj = None, metabolite_std = None):

        #list of files + absolute file names
        self.fileDirectory = filePaths

        #unique values of certain entries (title and doi)
        self.uniqueValues = set()

        #list of records
        self.records = []

        if(standard_obj == None):
            standard_obj = Standard('',empty = True)

        if(metabolite_std == None):
            metabolite_std = Standard('',empty = True)

        self.standard_obj = standard_obj

        for record in self.fileDirectory:

            fileID = open(record,'rU')
            fileData = fileID.readlines()
            fileID.close()

            paper = Paper(record,fileData,numMutantsKey,numMutationsKey,paperTerms,mutantTerms,geneTerms,parsingRules, knownMutations, typeMap, standard_obj, metabolite_std)

            if(paper.doi not in self.uniqueValues):
                self.records.append(paper)

            self.uniqueValues.add(paper.doi)

class Paper:

    def __init__(self, fileName, fileData, numMutantsKey, numMutationsKey, paperTerms, mutantTerms, geneTerms, parsingRules, knownMutations, typeMap, standard_obj, metabolite_std):

        self.defaults = {}



        self.defaults['text'] = ''
        self.defaults['float'] = float('nan')
        self.defaults['integer'] = float('nan')

        #backing contains the original key-value file, but with no parsing of the value beyond space trimming.
        self.backing = {}
        self.paperBacking = {}
        self.mutantBacking = {}
        self.geneBacking = {}
        self.annotationBacking = {}

        self.fileName = fileName
        self.keyOrder = []

        #self.paperTerms = paperTerms
        #self.mutantTerms = mutantTerms
        #self.geneTerms = geneTerms
        self.parsingRules = parsingRules

        self.typeMap = typeMap

        Paper.parseRecord(self,fileData)
        Paper.parsePaperEntries(self, paperTerms, parsingRules)

        try:
            numMutants = int(self.backing[numMutantsKey])
        except:
            numMutants = int(float(self.backing[numMutantsKey]))

        Paper.parseMutantEntries(self, numMutantsKey, mutantTerms, parsingRules)
        Paper.parseGeneEntries(self, numMutantsKey, numMutationsKey, geneTerms, parsingRules, knownMutations)

        self.mutants = []

        for i in range(1, numMutants+1):
            mutant = Mutant(i, self.mutantBacking, self.geneBacking, self.annotationBacking, mutantTerms, geneTerms, standard_obj, metabolite_std, parsingRules, numGK = numMutationsKey)
            self.mutants.append(mutant)

        for key in paperTerms:
            setattr(self, key, self.paperBacking[key])

    def get_mutant(self,i):

        return self.mutants[i-1]

    @property
    def mutants(self):
        return self.mutants

    @property
    def doi(self):
        return getattr(self, 'DOI')

    @property
    def title(self):
        return getattr(self, 'Title')

    @property
    def year(self):
        return getattr(self, 'Year')

    @property
    def rgroup(self):
        return getattr(self, 'ResearchGroup')

    @property
    def journal(self):
        return getattr(self, 'Journal')

    @property
    def difficulty(self):
        return getattr(self, 'project_score')

    @property
    def difficulty_reason(self):
        return getattr(self, 'score_reason')

    @property
    def design_method(self):
        return getattr(self, 'project_doe')

    @property
    def categories(self):
        return getattr(self, 'Tags')

    @property
    def total_designs(self):
        return getattr(self, 'total')

    def rewrite(self, output_dir):

        def cts(key, value, parsingRules):

            #I want this to fail immediately if there is no parsing rule for a given key, so no error handling here.

            #print key, value, parsingRules[key], type(value)

            if(type(value) == float and math.isnan(value) or type(value) == int and math.isnan(value)):
                value = ''

            parsing_dict = parsingRules[key]

            if(not parsing_dict['is_list']):
                return "\"" + str(value) + "\""
            else:
                return "\"" + parsing_dict['list_key'].join(value) + "\""

        outputFile = []

        for key in self.paperBacking:

            outputFile.append(key + '=' + cts(key, self.paperBacking[key], self.parsingRules))

        for key in self.mutantBacking:

            outputKey = 'Mutant' + str(key[0]) + "." + key[1]

            outputFile.append(outputKey + '=' + cts(key[1], self.mutantBacking[key], self.parsingRules))

        for key in self.geneBacking:

            outputKey = 'Mutant' + str(key[0]) + '.' + 'Mutation' + str(key[1]) + '.' + key[2]

            outputFile.append(outputKey + '=' + cts(key[2], self.geneBacking[key], self.parsingRules))

        for key in self.annotationBacking:

            outputKey = 'Mutant' + str(key[0]) + '.' + 'Mutation' + str(key[1]) + '.' + key[2] + '.' + key[3]

            outputFile.append(outputKey + '=' + "\"" + self.annotationBacking[key] + "\"")

        fhandle = open(os.path.join(output_dir, self.fileName.split(os.sep)[-1]),'w')

        if(os.path.join(output_dir, self.fileName.split(os.sep)[-1]) == self.fileName):
            raise AssertionError('In place rewriting of files is forbidden.')

        #print os.path.join(output_dir, self.fileName.split(os.sep)[-1])

        for line in outputFile:
            fhandle.write(line + '\n')
        fhandle.close()

    def value_caster(self, term, value, variable_type):

        df = self.defaults

        if(value == ''):
            return df[variable_type]

        if(variable_type == 'text'):
            return str(value)

        if(variable_type == 'integer'):
            return int(value)

        if(variable_type == 'float'):
            return float(value)

        raise AssertionError('Unrecognized variable type requested: %s, %s' %(term, variable_type))

    def parse_entry(self, term, backing_key, storage_key, container, parsingRules):

        try:
            value = self.backing[backing_key].strip()
        except KeyError:
            value = ''
            self.backing[backing_key] = value

        parsing_dict = parsingRules[term]

        if(parsing_dict['is_list'] == False):
            container[storage_key] = self.value_caster(term, value, parsingRules[term]['type'])

        else:

            delimiter = parsing_dict['list_key']

            if(value != ''):
                container[storage_key] = [self.value_caster(term, x, parsingRules[term]['type']) for x in value.split(delimiter)]
                self.backing[storage_key] = [self.value_caster(term, x, parsingRules[term]['type']) for x in value.split(delimiter)]
            else:
                container[storage_key] = []
                self.backing[storage_key] = []

        return container

    def parsePaperEntries(self, paperTerms, parsingRules):

        #we want to extract all of the keys related to paper information and dump them into the paperBacking map.

        for term in paperTerms:
            self.paperBacking = self.parse_entry(term, term, term, self.paperBacking, parsingRules)

    def parseMutantEntries(self, numMutantsKey, mutantTerms, parsingRules):

        numMutants = int(self.paperBacking[numMutantsKey])

        for i in range(1,numMutants+1):
            for term in mutantTerms:
                key = 'Mutant' + str(i) + '.' + term
                self.mutantBacking = self.parse_entry(term, key, (i, term), self.mutantBacking, parsingRules)

    def parseGeneEntries(self, numMutantsKey, numMutationsKey, geneTerms, parsingRules, knownMutations):

        numMutants = int(self.paperBacking[numMutantsKey])

        for i in range(1,numMutants+1):
            numMutations = int(self.mutantBacking[(i,numMutationsKey)])
            notFound = []
            for j in range(1,numMutations+1):

                for term in geneTerms:

                    key = 'Mutant' + str(i) + '.Mutation' + str(j) + '.' + term

                    self.geneBacking = self.parse_entry(term, key, (i,j,term), self.geneBacking, parsingRules)

                    if(term == 'GeneMutation'):
                        mutations = self.geneBacking[(i,j,term)]
                        notFound = self.parseAnnotations(i, j, term, mutations,knownMutations, parsingRules)

                    if(term == 'gComments' and len(knownMutations) > 0):
                        for entry in notFound:
                            self.geneBacking[(i,j,term)] = self.geneBacking[(i,j,term)] + ", " + entry
                            self.backing[(i,j,term)] = self.backing[(i,j,term)] + ", " + entry


    def parseAnnotations(self, i, j, term, mutations, knownMutations, parsingRules):

        notFound = []

        def apply_parsing_rules(mutation, value):

            if(mutation not in parsingRules):
                raise AssertionError('Missing parsing rule for %s mutation annotation' % mutation)

            parsing_dict = parsingRules[mutation]

            if(parsing_dict['is_list'] or parsing_dict['is_tuple']):

                delimiter = parsing_dict['list_key']

                tokens = [x.strip() for x in value.split(delimiter)]

                tokens = [self.value_caster(mutation, x, parsing_dict['type']) for x in tokens]

                if(parsing_dict['is_list']):
                    return tokens
                else:
                    return tuple(tokens)
            else:

                return self.value_caster(mutation, value, parsing_dict['type'])

        for mutation in mutations:

            key = 'Mutant' + str(i) + '.Mutation' + str(j) + '.' + term + '.' + mutation

            try:
                value = self.backing[key]
            except KeyError:
                value = ''

            self.annotationBacking[(i,j,term,mutation.lower())] = apply_parsing_rules(mutation, value)
            self.backing[(i,j,term,mutation.lower())] = apply_parsing_rules(mutation, value)

            if(mutation.lower() not in knownMutations):
                notFound.append(mutation.lower())

        return notFound

    def parseRecord(self,fileData):

        for line in fileData:

            if("#" not in line and len(line.strip())>0):

                tokens = line.strip().split("=")

                key = tokens[0].strip()

                self.keyOrder.append(key)
                value = tokens[1].strip().replace('\"','')
                value = value.strip('\n')

                if(len(value) > 0):
                    self.backing[key] = value


#flattens mutant/gene lists
class Mutant:

    def __init__(self, mutantNumber, mutantBacking, geneBacking, annotationBacking, mutantFields, geneFields, standard_obj, metabolite_std, parsingDict, numGK = 'NumberofMutations', ignore_missing_values = False):

        self.id = mutantNumber
        self.backing = {}
        self.mutations = []

        all_affected_genes = set()
        ancillary_genes    = set()

        #copies mutant i information into it's own data structure
        for term in mutantFields:

            if(ignore_missing_values == True and (self.id, term) not in mutantBacking):
                self.backing[term] = ''
            else:
                if(term == 'TargetMolecule'):
                    self.backing[term] = [metabolite_std.convert(x,'none').upper() for x in mutantBacking[(self.id, term)]]
                else:
                    self.backing[term] = mutantBacking[(self.id, term)]

        numMutations = int(self.backing[numGK])

        for j in range(1, numMutations+1):

            #filter out placeholder garbage here before making mutation object

            if(not (geneBacking[(self.id,j,'GeneName')].upper() == 'NONE' or geneBacking[(self.id,j,'GeneSource')].upper() == 'NONE')):
                mutationObj = Mutation(self.id, j, self.backing['Species'], geneBacking, annotationBacking, geneFields, standard_obj, parsingDict, ignore_missing_values = ignore_missing_values)
                self.mutations.append(mutationObj)

                all_affected_genes = all_affected_genes.union(mutationObj.all_affected_genes(parsingDict))

                ancillary_genes = ancillary_genes.union(mutationObj.ancillary_genes(parsingDict))

        #define attributes
        for key in self.backing:
            setattr(self, key, self.backing[key])

        #reset number of mutations to match the length of mutations array
        setattr(self, numGK, len(self.mutations))

        setattr(self, 'affected_genes', all_affected_genes)

        setattr(self, 'ancillary_genes',ancillary_genes)

    @property
    def mutations(self):
        return self.mutations

    @property
    def species(self):
        return getattr(self, 'Species')

    @property
    def strain(self):
        return getattr(self, 'Subspecies')

    @property
    def culture_vessel(self):
        return getattr(self, 'CultureSystem')

    @property
    def oxygenation(self):
        return getattr(self, 'Oxygen')

    @property
    def medium(self):
        return (getattr(self, 'Medium'), getattr(self, 'Supplements'))

    @property
    def culture_volume(self):
        return float(getattr(self, 'cvolume'))

    @property
    def liquid_volume(self):
        return float(getattr(self, 'fvolume'))

    @property
    def substrates(self):
        return getattr(self, 'CarbonSource')

    @property
    def pH(self):
        return float(getattr(self, 'pH'))

    @property
    def temperature(self):
        return float(getattr(self, 'Temperature'))

    @property
    def rpm(self):
        return float(getattr(self, 'Rotation'))

    @property
    def name(self):
        return getattr(self, 'Name')

    @property
    def methods(self):
        return getattr(self, 'Method')

    @property
    def tolerance(self):
        return getattr(self, 'TolerancePhenotype')

    @property
    def metabolite(self):
        return getattr(self, 'TargetMolecule')

    @property
    def fold_improvement(self):
        return float(getattr(self, 'FoldImprovement'))

    @property
    def titer_units(self):
        return  getattr(self, 'TiterUnits')

    @property
    def yield_units(self):
        return getattr(self, 'YieldUnits')

    @property
    def initial_titer(self):
        return float(getattr(self, 'InitialTiter'))

    @property
    def final_titer(self):
        return float(getattr(self, 'FinalTiter'))

    @property
    #initial yield, float or none
    def initial_yield(self):
        return float(getattr(self, 'InitialYield'))

    @property
    #final yield, float or none
    def final_yield(self):
        return float(getattr(self, 'FinalYield'))

class Mutation:

    def __init__(self, i, j, host, geneBacking, annotations, geneFields, standard_obj, parsingDict, ignore_missing_values = False):

        self.backing = {}
        self.id = j

        self.annotation_backing = {}

        for term in geneFields:

            if(ignore_missing_values == True and (i,j,term) not in geneBacking):
                self.backing[term] = ''
            else:
                self.backing[term] = geneBacking[(i,j,term)]

        for key in self.backing:

            if(key == 'GeneName'):
                setattr(self, key, standard_obj.convert(self.backing[key], self.backing['GeneSource']))
            else:
                setattr(self, key, self.backing[key])

        for mutation in self.changes:
            if(parsingDict[mutation]['category'] == 'gene'):
                if(type(annotations[(i,j,'GeneMutation',mutation)]) is list or type(annotations[(i,j,'GeneMutation',mutation)]) is tuple):
                    self.annotation_backing[mutation] = [standard_obj.convert(x,host) for x in annotations[(i,j,'GeneMutation',mutation)]]
                else:
                    self.annotation_backing[mutation] = standard_obj.convert(annotations[(i,j,'GeneMutation',mutation)], host)
            else:
                self.annotation_backing[mutation] = annotations[(i,j,'GeneMutation',mutation)]

    def ancillary_genes(self, parsingDict):

        all_affected_genes = self.all_affected_genes(parsingDict)

        all_affected_genes.remove(self.name)

        return all_affected_genes

    def all_affected_genes(self, parsingDict):

        output = set()
        output.add(self.name)

        for mutation in self.changes:

            if(parsingDict[mutation]['category'] == 'gene'):

                if(parsingDict[mutation]['is_list'] or parsingDict[mutation]['is_tuple']):
                    for x in self.annotation(mutation):
                        output.add(x)
                else:
                    output.add(self.annotation(mutation))

        return output

    def annotation(self, mutation):

        if(mutation in self.annotation_backing):
            return self.annotation_backing[mutation]
        else:
            raise AssertionError('No such mutation present in this mutation object: %s' % mutation)


    @property
    #source strain for the gene, string
    def source(self):

        return getattr(self, 'GeneSource')

    @property
    #returns a boolean describing whether or not this gene has been mutated in previous designs or is common knowledge
    def is_original(self):

        result = getattr(self, 'GeneOriginal')

        return result == 'yes'

    @property
    #strings describing the effect of all gene mutations together
    def effects(self):

        return getattr(self, 'GeneEffect')

    @property
    #list of mutations affecting the gene, see Term Usage.txt for allowed entries (not enforced at the coding level)
    def changes(self):

        return getattr(self, 'GeneMutation')

    @property
    #alternative names for the gene in question
    def nicknames(self):

        return getattr(self, 'GeneNickname')

    @property
    #gene name
    def name(self):

        return getattr(self, 'GeneName')



