import os
from MetEngDatabase import MetEngDatabase
from collections import defaultdict
from Standardization import Standard

INPUT_DIR = os.getcwd() + os.sep + 'inputs'+ os.sep
OUTPUT_DIR = os.getcwd() + os.sep + 'output_2015'+ os.sep
DATABASE_DIR = os.getcwd() + os.sep + "database_store" + os.sep
ERROR_DIR = os.getcwd() + os.sep + 'errors'+ os.sep

#define universal constants relating to mutation classification
OE_mutations     = set(['amplified','oe','plasmid','rbs_tuned'])
REP_mutations    = set(['rep','antisense'])
DEL_mutations    = set(['del','frameshift','is_insertion','indel'])
RANDOM_mutations = set(['aa_snps','nuc_snps'])
ADD_mutations    = set(['sensor','regulated'])

def networkx_to_igraph(networkx_graph, directed = True):

    import igraph

    nodes = networkx_graph.nodes()
    edges = networkx_graph.edges()

    nodes_to_int = {}
    int_to_nodes = {}

    for i in range(0,len(nodes)):
        nodes_to_int[nodes[i]] = i
        int_to_nodes[i] = nodes[i]

    #convert edges

    converted_edges = []

    converted_weights = []

    for edge in edges:
        node1 = nodes_to_int[edge[0]]
        node2 = nodes_to_int[edge[1]]


        if('weight' in networkx_graph[edge[0]][edge[1]]):
            converted_weights.append(networkx_graph[edge[0]][edge[1]]['weight'])
        converted_edges.append((node1,node2))

    #build igraph,
    #compute betweenness,
    #converte btw scores back to networkx node names

    if(directed == False):
        directed = None

    if(converted_weights != []):

        G = igraph.Graph(n = len(nodes), edges = converted_edges, directed = directed, edge_attrs={'weight':converted_weights})

    else:

        G = igraph.Graph(n = len(nodes), edges = converted_edges, directed = directed)

    return G, int_to_nodes, converted_weights

#clusters graphs into communities using leading eigenvector by MEJ Newmann method
#citation: Finding community structure in networks using the eigenvectors of matrices
#I tried other methods, but they just make one super cluster with >1000 others with very few nodes in them.
#betweenness: too computationally expensive
#multilevel: few clusters with many nodes
#spinglass: doesn't work (bug or user error?)
#infomap: few clusters with many nodes
#MCL: haven't tried, need to build a wrapper for calling from python, build the software for windows

def compute_igraph_clustering(networkx_graph, method = None):

    G, int_to_nodes, converted_weights = networkx_to_igraph(networkx_graph, directed = True)

    if(converted_weights == []):
        converted_weights = None

    clusters = None

    if(method == 'leading_eigenvector'):
        clusters = G.community_leading_eigenvector(weights=converted_weights)#.as_clustering()
    elif(method == 'infomap'):
        clusters = G.community_infomap(edge_weights=converted_weights)
    elif(method == 'edge_betweenness'):
        clusters = G.community_edge_betweenness(weights=converted_weights).as_clustering()
    elif(method == 'multilevel'):
        clusters = G.as_undirected().community_multilevel(weights=converted_weights)
    elif(method == 'walktrap'):
        clusters = G.community_walktrap(weights=converted_weights).as_clustering()
    elif(method == 'fastgreedy'):
        clusters = G.as_undirected().community_fastgreedy(weights=converted_weights).as_clustering()
    elif(method == 'label_propagation'):
        clusters = G.as_undirected().community_label_propagation(weights=converted_weights)
    else:
        #break if invalid clustering selection selected
        raise KeyError('Unrecognized clustering method provided: %s' % method)

    #clusters = dendrogram.as_clustering()

    membership = clusters.membership

    nodes_to_clusters = {}

    cluster_membership = defaultdict(list)

    for i in range(0, len(membership)):

        node_id = int_to_nodes[i]
        nodes_to_clusters[node_id] = membership[i]

        cluster_membership[membership[i]].append(node_id)
    
    return nodes_to_clusters, cluster_membership

#reconstructs the passed info to an igraph
def compute_igraph_evcentrality(networkx_graph, directed = True):

    G, int_to_nodes, converted_weights = networkx_to_igraph(networkx_graph, directed = directed)
    
    #already normalized

    if(converted_weights == []):
        converted_weights = None
    
    ec = G.eigenvector_centrality(weights = converted_weights)

    ec_dict = {}

    for i in range(0, len(ec)):
        ec_dict[int_to_nodes[i]] = ec[i]
        
    return ec_dict


#reconstructs the passed info to an igraph
def compute_igraph_betweenness(networkx_graph, directed = True):

    G, int_to_nodes, converted_weights = networkx_to_igraph(networkx_graph, directed = directed)

    if(converted_weights == []):
        converted_weights = None
    
    bc = G.betweenness(weights = converted_weights)

    #normalizing value
    max_value = max(bc)

    bc_dict = {}
    #0-1.0 scale for all networks
    for i in range(0, len(bc)):
        bc_dict[int_to_nodes[i]] = float(bc[i])/float(max_value)
        
    return bc_dict

def compute_frequency(default_dict):

    d_sum = 0

    fdict = {}

    for key in default_dict:
        d_sum = d_sum + default_dict[key]

    for key in default_dict:
        fdict[key] = float(default_dict[key])/float(d_sum)

    return fdict

def normalize_dict(tfdict, rfdict):

    output_dict = defaultdict(float)

    for key in tfdict:

        if(key in rfdict and rfdict[key] != 0):
            output_dict[key] = tfdict[key]/rfdict[key]
        else:
            print 'Invalid or missing key from reference dictionary'
            raise ValueError

    return output_dict
        
def writefile(header, outputList, delim,  outputName):

    data = [header]
    
    for item in outputList:
        data.append(item)
        
    fhandle = open(outputName,'w')

    for line in data:
        line = [str(x) for x in line]
        fhandle.write(delim.join(line) + "\n")
    fhandle.close()

def load_use_abbrevs():

    umapper = {}
    umapper['BIOFUELS'] = 'BF'
    umapper['BIOMASS'] = 'BIOM'
    umapper['BIOREMEDIATION'] = 'REMD'
    umapper['COSMETICS'] = 'COSM'
    umapper['FOOD ADDITIVES'] = 'FOOD'
    umapper['ORGANIC ACIDS'] = 'OACID'
    umapper['pharmaceuticals'.upper()] = 'PHARM'
    umapper['POLYMERS'] = 'POLY'
    umapper['FEEDSTOCK'] = 'FSTOCK'
    return umapper

def load_category_abbrevs():

    cmapper = {}
    cmapper['ALCOHOLS'] = 'ALC'
    cmapper['POLYKETIDE'] = 'POLYK'
    cmapper['ALDEHYDES'] = 'ALDE'
    cmapper['AMINO ACIDS'] = 'AA'
    cmapper['BIOMASS'] = 'BIOM'
    cmapper['BIOPOLYMER'] = 'POLY'
    cmapper['CHALCONE'] = 'CHAL'
    cmapper['DIAMINE'] = 'DIA'
    cmapper['DIOLS'] = 'DIOLS'
    cmapper['DNA'] = 'DNA'
    cmapper['ethylene glycol'.upper()] = 'ETGL'
    cmapper['FATTY ACIDS'] = 'FA'
    cmapper['flavanoids'.upper()] = 'FLAV'
    cmapper['ESTERS'] = 'ESTER'
    cmapper['KETONES'] = 'KET'
    cmapper['HEAVY METALS'] = 'HEAV'
    cmapper['HYDROCARBON'] = 'HYC'
    cmapper['HYDROGEN'] = 'H2'
    cmapper['metabolic intermediates'.upper()] = 'META'
    cmapper['organic acids'.upper()] = 'OACID'
    cmapper['PHENOLICS'] = 'PHEN'
    cmapper['protein'.upper()] = 'PROT'
    cmapper['SOLVENTS'] = 'SOLV'
    cmapper['sugar alcohol'.upper()] = 'SUGA'
    cmapper['TERPENOIDS'] = 'TERP'
    cmapper['VITAMINS'] = 'VITA'
    cmapper['AROMATICS'] = 'AROM'
    return cmapper

def load_journal_abbrevs():
    
    journal_mapper = {}
    journal_mapper['Metabolic Engineering'.upper()] = 'ME'
    journal_mapper['Applied and Environmental Microbiology'.upper()] = 'AEM'
    journal_mapper['Microbial Cell Factories'.upper()] = 'MCF'
    journal_mapper['Biotechnology and Bioengineering'.upper()] = 'BB'
    journal_mapper['Applied Microbiology and Biotechnology'.upper()] = 'AMB'
    journal_mapper['PNAS'] = 'PNAS'
    journal_mapper['Nature Biotechnology'.upper()] = 'NBT'
    journal_mapper['Journal of Industrial Microbiology and Biotechnology'.upper()] = 'JIMB'
    journal_mapper['ACS Synthetic Biology'.upper()] = 'ACSB'
    journal_mapper['APPLIED BIOTECHNOLOGY AND MICROBIOLOGY'] = 'ABM'
    journal_mapper['FEMS YEAST RESEARCH'] = 'FEMY'
    journal_mapper['JOURNAL OF BIOTECHNOLOGY'] = 'JBIOT'
    journal_mapper['NATURE'] = 'NAT'
    journal_mapper['NATURE CHEMICAL BIOLOGY'] = 'NATCB'
    journal_mapper['PLOS GENETICS'] = 'PLGE'
    journal_mapper['BIOTECHNOLOGY FOR BIOFUELS'] = 'BFBF'
    journal_mapper['NATURE COMMUNICATIONS'] = 'NCOMM'
    journal_mapper['WILEY INTERSCIENCE'] = 'WINT'
    journal_mapper['BIOSCIENCE, BIOTECHNOLOGY, AND BIOCHEMISTRY'] = 'B3'
    journal_mapper['BIOTECHNOLOGY PROGRESS'] = 'BPROG'
    journal_mapper['JOURNAL OF APPLIED MICROBIOLOGY'] = 'JAM'
    journal_mapper['NUCLEIC ACIDS RESEARCH'] = 'NAR'
    journal_mapper['ORGANIC AND BIOMOLECULAR CHEMISTRY'] = 'OBC'

    journal_mapper['Other'] = 'OTH'

    return journal_mapper

def load_method_abbrevs():

    method_mapper = {}
    method_mapper['human'.upper()] = 'HUM'
    method_mapper['library'.upper()] = 'LIB'
    method_mapper['fba'.upper()] = 'FBA'
    method_mapper['evolution'.upper()] = 'EVO'
    method_mapper['random mutagenesis'.upper()] = 'RAND'
    method_mapper['protein engineering'.upper()] = 'PREN'
    method_mapper['transcriptomics'.upper()] = 'TRANS'
    method_mapper['liquidliquid'.upper()] = 'LL'
    method_mapper['computational'.upper()] = 'COMP'
    method_mapper['Other'] = 'OTH'
    method_mapper['combinatorial'.upper()] = 'CMBT'
    method_mapper['METABOLOMICS'] = 'METAB'
    method_mapper['SYNTHETIC PATHWAY'] = 'SYNP'
    method_mapper['COMPARTMENT'] = 'TARG'
    method_mapper['MODULES'] = 'MOD'
    method_mapper['SELECTION'] = 'SELX'

    return method_mapper

def load_mutation_abbrevs():

    mut_mapper = {}
    mut_mapper['oe'.upper()] = 'OE'
    mut_mapper['plasmid'.upper()] = 'PLA'
    mut_mapper['del'.upper()] = 'DEL'
    mut_mapper['integrated'.upper()] = 'INT'
    mut_mapper['terminated'.upper()] = 'TER'
    mut_mapper['aa_snps'.upper()] = 'AA*'
    mut_mapper['codonoptimized'.upper()] = 'C-OPT'
    mut_mapper['amplified'.upper()] = 'AMP'
    mut_mapper['truncated'.upper()] = 'TRC'
    mut_mapper['nuc_snps'.upper()] = 'SNP'
    mut_mapper['mutated'.upper()] = 'UNK'
    mut_mapper['compartmentalization'.upper()] = 'COMP'
    mut_mapper['con'.upper()] = 'CON'
    mut_mapper['scaffold_binder'.upper()] = 'SCFB'
    mut_mapper['protein_fusion'.upper()] = 'FUSE'
    mut_mapper['indel'.upper()] = 'INDEL'
    mut_mapper['antisense'.upper()] = 'ASEN'
    mut_mapper['rbs_tuned'.upper()] = 'RBS-T'
    mut_mapper['regulated'.upper()] = 'REG'
    mut_mapper['Other'] = 'OTH'

    return mut_mapper

def getGeneAccessionDict(species, reverse_mapping = False):

    files = {'ESCHERICHIA COLI':'EcAccession.txt', 'SACCHAROMYCES CEREVISIAE':'ScAccession.txt'}

    output_dict = {}
    reverse_dict = {}

    if(species.upper() in files):

        accession_file = files[species.upper()]

        fhandle = open(os.path.join(INPUT_DIR, accession_file),'rU')
        lines = fhandle.readlines()
        fhandle.close()

        for line in lines[1:]:

            tokens = line.split('\t')

            gene_name = tokens[0].strip()
            accession = tokens[1].strip()
            output_dict[gene_name.upper()] = accession
            output_dict[gene_name] = accession
            reverse_dict[accession] = gene_name.upper()

    else:
        raise KeyError

    if(reverse_mapping):
        return output_dict, reverse_dict
    else:
        return output_dict

def load_gene_standardization():

    fhandle = open(INPUT_DIR + 'Gene Name Mapping.txt','rU')
    standard_names_file = fhandle.readlines()
    fhandle.close()

    gene_dict = {}

    for line in standard_names_file[1:]:

        token = line.split('\t')

        laser_gene_name = token[0].strip()
        standard_name = token[2].strip()

        gene_dict[laser_gene_name.upper()] = standard_name.upper()

    return gene_dict

def load_product_ontology(metabolite_cache):

    fhandle = open(INPUT_DIR + 'Product Ontology.txt','rU')
    product_cats = fhandle.readlines()
    fhandle.close()

    pc_dict = {}

    for line in product_cats[1:]:
        token = line.strip().split('\t')
        product = token[0].upper()
        intended_uses = token[1].strip().split(',')
        product_class = token[2].strip().split(',')

        if(product in metabolite_cache and metabolite_cache[product][0] != None):
            pc_dict[metabolite_cache[product][0]] = (intended_uses, product_class)
        else:
            pc_dict[product] = (intended_uses, product_class)

    return pc_dict

def load_metabolite_cache(return_standard = False):

    fhandle = open(INPUT_DIR+'Compound Information.txt','rU')
    lines = fhandle.readlines()
    fhandle.close()

    mapper = {}

    tuples = []

    for line in lines[1:]:

        token = line.strip().split('\t')

        laser_name = token[0].upper()
        biocyc_name = token[1].upper()

        if(biocyc_name != 'NONE'):

            mw = float(token[2])
            formula = token[3].translate(None,'()')
            elements = formula.split(',')
            elem_dict = {}

            for item in elements:

                component = item.split(' ')

                element = component[0]
                subscript = int(component[1])

                elem_dict[element] = subscript
                
            mapper[laser_name] = (biocyc_name, mw, elem_dict)
            mapper[biocyc_name] = (biocyc_name, mw, elem_dict)

            tuples.append((laser_name, 'none', biocyc_name))

        elif(biocyc_name == 'NONE' and token[2] != '?'):

            mw = float(token[2])
            formula = token[3].translate(None,'()')
            elements = formula.split(',')
            elem_dict = {}

            for item in elements:

                component = item.split(' ')

                element = component[0]
                subscript = int(component[1])

                elem_dict[element] = subscript
                
            mapper[laser_name] = (None, mw, elem_dict)

            tuples.append((laser_name, 'none', laser_name))

        else:

            tuples.append((laser_name, 'none', laser_name))

            mapper[laser_name] = (None, None, None)
            
    if(not return_standard):
        return mapper
    else:
        standard = Standard('',empty=True)
        standard.load_tuples(tuples)
        return mapper, standard


def get_added_reactions(file_name):

    fhandle = open(file_name,'rU')
    lines = fhandle.readlines()
    fhandle.close()

    reaction_dict = defaultdict(dict)

    for line in lines[1:]:

        tokens = line.strip().split('\t')

        gene_name = tokens[0]
        direction = tokens[1]
        left_hs   = tokens[2]
        right_hs  = tokens[3]
        coefficients = tokens[4]
        occurrences = tokens[5]

        reaction_dict[gene_name]['direction'] = direction
        reaction_dict[gene_name]['reactants'] = left_hs
        reaction_dict[gene_name]['products']  = right_hs
        reaction_dict[gene_name]['coefficients'] = coefficients.split(',')
        reaction_dict[gene_name]['count'] = occurrences

    return reaction_dict

def build_new_reactions(output_file_name, host = 'Escherichia coli'):

    import psycopg2
    import psycopg2.extras
    import warnings
    from ReactionBuilder import ReactionBuilder

    #outputs a list of reactions that have all native metabolites but are not already present in the e. coli metabolic model
    #output file is used as an input for the ESD 'add' mutation (add new reaction)

    rxnbuilder = ReactionBuilder()

    try:
        connect = psycopg2.connect("dbname='biocyc' user='james' host='localhost' password='winkler'")
    except:
        print "I am unable to connect to the database"
        raise

    cur = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

    papers = getDatabase()

    reaction_list = []
    id_set = set()

    #what to do about citramalate pathway?

    warnings.warn('Remeber that you converted cimA to the net reaction from acetyl-coa to 2-ketobutyate')

    reaction_counter = defaultdict(int)

    for paper in papers:

        model_log_tuples = rxnbuilder.generateModel(cur, paper)

        for tup in model_log_tuples:

            (model, log, _) = tup

            #print tup

            #reactions that have metabolites that are all present in the reaction network, but are not encoded by the genome
            added_reactions =  log.get_data('native_reactions')

            for rxn in added_reactions:

                reaction_counter[rxn['unique_id']]+=1

                if(rxn['unique_id'] in id_set or host.upper() != rxn['host'].upper()):
                    continue
                id_set.add(rxn['unique_id'])
                reaction_list.append(rxn)

    output_list = []

    for rxn in reaction_list:

        output_string = (rxn['gene'], rxn['reaction_direction'], rxn['left_hs'], rxn['right_hs'], rxn['coefficient'], reaction_counter[rxn['unique_id']])

        output_list.append(output_string)

        #print 'Gene %s:' % rxn['gene'], rxn['left_hs'], rxn['reaction_direction'], rxn['right_hs'], rxn['coefficient'], 'occurs %i times in LASER' % reaction_counter[rxn['unique_id']]

    connect.close()

    writefile(('Gene name', 'Reaction Direction','Reactants', 'Products', 'Coefficients (in order)', 'Occurences in LASER'),output_list,'\t',output_file_name)

def get_hashcode(paper):

    import hashlib

    return hashlib.md5(str(paper)).hexdigest()

def strain_properties_effort_correlation(return_y = False):

    import numpy
    fhandle = open(os.path.join(INPUT_DIR,'etas.txt'),'rU')

    time_array = []
    e1_array = []
    e2_array = []
    e3_array = []
    e4_array = []
    e5_array = []

    for line in fhandle:

        tokens = line.strip().split('\t')

        e1,e2,e3,e4,e5,time,fte = float(tokens[1]),float(tokens[2]),float(tokens[3]),float(tokens[4]),float(tokens[5]),float(tokens[6]),float(tokens[7])

        time_array.append(time*fte)
        e1_array.append(e1)
        e2_array.append(e2)
        e3_array.append(e3)
        e4_array.append(e4)
        e5_array.append(e5)

    limit = len(time_array)

    time_data = time_array

    property_data = [e1_array,e2_array,e3_array,e4_array,e5_array]

    import scipy.optimize
    import scipy.stats

    opt_function = lambda x,a,b,c,d,e,f: a*x[0] + b*x[1] + c*x[2] + d*x[3] + e*x[4] + f

    popt, pcov = scipy.optimize.curve_fit(opt_function,[x[0:limit] for x in property_data],time_data[0:limit])

    '''
    clf = linear_model.Lasso(alpha=0.1)

    clf.fit(numpy.transpose([x[0:limit] for x in property_data]),time_data[0:limit])

    clf_rsq = clf.score(numpy.transpose([x[0:limit] for x in property_data]), time_data[0:limit])

    print clf.coef_
    print clf.intercept_
    print clf_rsq
    '''

    alpha = popt

    sstot = 0
    ssres = 0

    ymean = numpy.mean(time_data)

    y_est = []

    for e1,e2,e3,e4,e5,true_y in zip(property_data[0],property_data[1],property_data[2],property_data[3],property_data[4], time_data):

        y_estimate = alpha[0] * e1 + alpha[1] * e2 + alpha[2] * e3 + alpha[3] * e4 + alpha[4]*e5 + alpha[5]

        #print y_estimate
        y_est.append(y_estimate)

        sstot += (true_y-ymean)**2
        ssres += (y_estimate-true_y)**2

    #Rsquared = 1-ssres/sstot

    slope, intercept, Rsquared, pvalue, stderr = scipy.stats.linregress(y_est, time_data)

    if(numpy.sqrt(Rsquared) < 0.2):
        print 'R: %g' % numpy.sqrt(1-ssres/sstot)
        raise AssertionError('Time eta correlation too poor.')

    final_function = lambda e1,e2,e3,e4,e5: alpha[0] * e1 + alpha[1] * e2 + alpha[2] * e3 + alpha[3] * e4 + alpha[4]*e5 + alpha[5]

    fmt = '{0:.3g}'

    equation_string = 'y = ' + fmt.format(alpha[0]) + '*e1+' + fmt.format(alpha[1]) + '*e2+\n' + fmt.format(alpha[2]) + '*eta3+' + fmt.format(alpha[3]) + '*eta4+' + fmt.format(alpha[4]) + '*eta5+' + fmt.format(alpha[5])

    if(return_y):
        return final_function, time_data, y_est, equation_string, Rsquared
    else:
        return final_function

def build_mutational_spectra(papers, standard_obj):

    spectrum = defaultdict(dict)

    for paper in papers:
        for mutant in paper.mutants:
            for gene in mutant.mutations:

                name = standard_obj.convert(gene.name.upper(), gene.source.upper()).upper()
                mutations = gene.changes
                for mut in mutations:
                    if(mut in spectrum[name]):
                        spectrum[name][mut] += 1
                    else:
                        spectrum[name][mut] = 1

    return spectrum

def get_object_fields():

    inputTerms = INPUT_DIR+'InputFields_typed.txt'

    inputs = open(inputTerms,'rU')

    dataTerms = []
    muttTerms = []
    geneTerms = []

    for line in inputs:

            line = line.translate(None,'\n\"')
            tokens = line.split("\t")

            category  = tokens[0]
            entryName = tokens[1]

            if(category == 'paper'):
                    dataTerms.append(entryName)

            if(category == 'mutant'):
                    muttTerms.append(entryName)

            if(category == 'gene'):
                    geneTerms.append(entryName)

            if(category == 'hidden'):
                    dataTerms.append(entryName)

    return dataTerms, muttTerms, geneTerms

def getDatabase(standard, metabolite_std, getParser = False, years = ('2014','2015')):

    inputTerms = os.path.join(INPUT_DIR,'InputFields_typed.txt')

    annotation_terms = open(os.path.join(INPUT_DIR, 'Annotation Grammar_typed.txt'),'rU')
    annotation_lines = annotation_terms.readlines()
    annotation_terms.close()

    annotation_format_dict = {}
    parserRules     = {}

    headers = annotation_lines[0].strip().split('\t')
    c = {}

    for i, token in zip(range(0, len(headers)), headers):
        c[token] = i

    for line in annotation_lines[1:]:

        tokens = line.translate(None,'\n\"').split('\t')

        tokens = [x.strip() for x in tokens]

        mutation_name = tokens[c['Mutation']]
        mutation_type = tokens[c['Type']]
        category      = tokens[c['Category']]
        is_list       = tokens[c['List']] == '1'
        is_tuple      = tokens[c['Tuple']] == '1'
        delimiter     = tokens[c['Delimiter']]

        if(delimiter.upper() == 'NONE'):
            delimiter = ''

        parsing_dict = {'type':mutation_type,'is_list':is_list,'is_tuple':is_tuple,'list_key':delimiter, 'category':category}

        parserRules[mutation_name] = parsing_dict



    fhandle = open(inputTerms,'rU')
    inputs = fhandle.readlines()
    fhandle.close()

    dataTerms = []
    muttTerms = []
    geneTerms = []

    termCategoryMap = {}
    termTypeMap     = {}
    termRequiredMap = {}
    termEntryMap    = {}
    termMultipleMap = {}

    headers = inputs[0].strip().split('\t')
    c = {}

    for i, token in zip(range(0, len(headers)), headers):
        c[token] = i

    for line in inputs[1:]:

            line = line.translate(None,'\n\"')
            tokens = line.split("\t")

            tokens = [x.strip() for x in tokens]

            category  = tokens[c['Category']]
            entryName = tokens[c['Internal Name']]
            display   = tokens[c['Displayed Name']]
            inputType = tokens[c['Type']]
            inputRequired = tokens[c['Required?']]
            multipleValues= tokens[c['Can select multiple?']]
            inputEntry    = tokens[c['Key for dropdown entries']]
            is_list   = tokens[c['List?']]
            list_delimiter= tokens[c['Delimiter']]

            if(category == 'paper'):
                    dataTerms.append(entryName)

            if(category == 'mutant'):
                    muttTerms.append(entryName)

            if(category == 'gene'):
                    geneTerms.append(entryName)

            if(category == 'hidden'):
                    dataTerms.append(entryName)

            parsing_dict = {}
            parsing_dict['type'] = inputType
            parsing_dict['is_list'] = is_list == '1'
            parsing_dict['list_key'] = list_delimiter
            parsing_dict['is_dropdown'] = inputEntry.upper() != 'NONE'
            parsing_dict['dropdown_key'] = inputEntry
            parsing_dict['multiple'] = multipleValues == '1'

            termCategoryMap[entryName] = display
            termTypeMap[entryName]     = inputType
            termRequiredMap[entryName] = inputRequired
            termEntryMap[entryName]    = inputEntry
            termMultipleMap[entryName] = multipleValues

            parserRules[entryName]     = parsing_dict

    numMK = 'NumberofMutants'
    numGK = 'NumberofMutations'

    paths = []

    for year in years:

        path = os.path.join(DATABASE_DIR,str(year))

        record_file_list = os.listdir(path)

        paths.extend([os.path.join(path,x) for x in record_file_list])

    database = MetEngDatabase(paths, numMK, numGK, dataTerms, muttTerms, geneTerms, parserRules, termTypeMap, [], standard_obj=standard, metabolite_std=metabolite_std)

    if(getParser):
        return database.records, parserRules
    else:
        return database.records
