from collections import defaultdict
from collections import Counter
from MetEngDatabase import MetEngDatabase
from MetEngDatabase import Paper
from cobra import Model, Reaction, Metabolite
from ModelLogger import ModelLogger
from RegulatoryBuilder import RegulatoryBuilder
from ReactionBuilder import ReactionBuilder
import DatabaseUtils
import networkx
import cobra
import os
import psycopg2
import psycopg2.extras

def write_error_dict(error_dict, filename):

    outputList = []
    for key in error_dict:
        if(type(key) == tuple):
            data_tuple = key + tuple(str(error_dict[key]))
            outputList.append(data_tuple)

    DatabaseUtils.writefile(tuple(), outputList, '\t', filename)

papers = DatabaseUtils.getDatabase()

ERROR_DIR = os.getcwd() + os.sep + 'errors' + os.sep 

try:
    connect = psycopg2.connect("dbname='biocyc' user='james' host='localhost' password='winkler'")
except:
    print "I am unable to connect to the database"

cur = connect.cursor(cursor_factory=psycopg2.extras.RealDictCursor)

rxn_build = ReactionBuilder()
reg_build = RegulatoryBuilder()

cobra_missing_genes = defaultdict(int)
#gene reaction pairs
cobra_missing_grp = defaultdict(int)
cobra_missing_mets = defaultdict(int)
cobra_unpaired_mets = defaultdict(int) 
regnet_missing_genes = defaultdict(int)

counter = 0

for paper in papers:

    cobra_logs, model = rxn_build.generateModel(cur, paper)
    regulatory_logs,model = reg_build.generateNetwork(cur, paper)

    for cobra_log in cobra_logs:

        #could not pair with metacyc id at all
        missingCobraGenes = cobra_log.get_data('missingGenes')

        #could not pair discovered gene with any reactions (regulators?)
        missingGeneRxnPairs = cobra_log.get_data('missing_rxns')

        #generic metabolites/unpaired
        missingMetabolites = cobra_log.get_data('missing_metabolites')
        genericMetabolites = cobra_log.get_data('generic_metabolites')

        #not cobra metabolites
        not_cobra_metabolites = cobra_log.get_data('not_cobra_metabolites')

        for gene in missingCobraGenes:
            cobra_missing_genes[gene] = cobra_missing_genes[gene] + 1

        for gene in missingGeneRxnPairs:
            cobra_missing_grp[gene] = cobra_missing_grp[gene] + 1

        for metabolite in missingMetabolites:
            cobra_missing_mets[metabolite] = cobra_missing_mets[metabolite] + 1

        for metabolite in not_cobra_metabolites:
            cobra_unpaired_mets[metabolite] = cobra_unpaired_mets[metabolite] + 1

    for regulatory_log in regulatory_logs:

        #genes not found in the regulatory network
        missingRegnetGenes = regulatory_log.get_data('missingGenes')

        for gene in missingRegnetGenes:
            regnet_missing_genes[gene] = regnet_missing_genes[gene] + 1

    if(counter % 10 == 0):
        print 'On paper %i' % counter

    counter = counter + 1

print 'Writing error data out'

#output mismatchs, errors, etc
write_error_dict(cobra_missing_genes,ERROR_DIR+'LASER Genes not found in MetaCyc.txt')
write_error_dict(missingGeneRxnPairs,ERROR_DIR+'Metacyc Genes without Reactions.txt')
write_error_dict(cobra_missing_mets,ERROR_DIR+'Metabolites without compound information.txt')
write_error_dict(cobra_unpaired_mets,ERROR_DIR+'Metabolites not found in COBRA model.txt')
write_error_dict(regnet_missing_genes,ERROR_DIR+'Gene missing in regulatory network files.txt')

        

        

        
        

    

    
