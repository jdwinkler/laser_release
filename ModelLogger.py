class ModelLogger:

    def __init__(self, unique_id):

        self.heading_order = []
        self.heading_variables = {}
        self.output_strings = {}
        self.unique_id = unique_id

        self.backing = {}

    def print_report(self):

        if(self.heading_order == []):
            print 'Must specify data categories, data before displaying report.'
            return

        else:

            for heading in self.heading_order:

                print '\n*****$---------%s---------$******\n' % heading

                output_data = self.heading_variables[heading]
                output_string = self.output_strings[heading]

                for datapoint in output_data:
                    print output_string + ": " + str(datapoint)

                if(output_data == []):
                    print 'No data provided.'

                #print '\n$---------END SECTION---------$\n'

    def add_section(self, heading, data, output_string):

        if(heading not in self.heading_variables):
            self.heading_order.append(heading)
            self.heading_variables[heading] = data
            self.output_strings[heading] = output_string
        else:
            self.heading_variables[heading].extend(data)

    def add_data(self, key, value):

        self.backing[key] = value

    def get_data(self, key):

        return self.backing[key]

    def get_dict(self):

        return self.backing
