__author__ = 'jdwinkler'

import networkx
import os
import numpy
import scipy.stats
import DatabaseUtils
from collections import defaultdict
import sys

def process_GO_buffer(buffer, keys):

    closure = defaultdict(list)

    keys = set(keys)

    keys.add('id')
    keys.add('is_a')

    id_key = ''

    for line in buffer:

        tokens = [x.strip() for x in line.split(' ',1)]

        key = tokens[0].replace(':','')
        value = tokens[1]

        #print line, tokens, key, value

        if(key not in keys):
            #not of interest, skip line
            continue

        if(key == 'id'):
            id_key = value
        else:
            closure[key].append(value)

    edges = []

    #this handles the case where the term is a root of a graph (has no is_a)
    if('is_a' in closure):

        for parent in closure['is_a']:

            parent = parent.split(' ! ')[0].strip()

            edges.append((parent, id_key))

    for key in closure:

        if(len(closure) == 1):
            closure[key] = closure[key][0]

    return id_key, closure, edges

def parse_GO_file(file_name, keys_of_interest):

    #converts GO file into a queryable python dictionary
    #you can also pass the output to build_SQL_database to do that (if you want, I may not)

    GO_term = []
    add = False

    GO_dict = {}

    if(type(keys_of_interest) != list):
        keys_of_interest = [keys_of_interest]


    GO_network = networkx.DiGraph()

    #todo: output relational graph of GO terms as well

    for line in open(file_name,'rU'):

        line = line.strip()

        if(len(line) == 0 or line[0] == '#'):
            #comment, continue (I had to manually add this)

            if(len(line) == 0 and add == True):
                #represents a break between GO terms
                id, record, edges = process_GO_buffer(GO_term, keys_of_interest)

                GO_network.add_edges_from(edges)

                GO_term = []
                GO_dict[id] = record
                add = False

            continue

        elif(add):

            GO_term.append(line)

        if('[term]' in line.lower()):
            add = True

    return GO_dict, GO_network

def gene_set_statistics(GO_dict, gene_go_list, keys_to_analyze):

    counting_statistics = defaultdict(int)

    output_dict = {}

    #single key of interest
    if(type(keys_to_analyze) != list):
        keys_to_analyze = [keys_to_analyze]

    for (gene, go_list) in gene_go_list:

        data = {}

        for go in go_list:
            for key in keys_to_analyze:
                data[go] = GO_dict[go][key]

                counting_statistics[go] = counting_statistics[go] + 1

        output_dict[gene] = data


    return output_dict, counting_statistics

#computes the statistical significance of GO frequency in the counting stats dict
#implements a simple monte-carlo boostraping algorithm: (from wikipedia)

#The Monte Carlo algorithm for case resampling is quite simple. First, we resample the data with replacement, and the size of the resample must be equal to the size of the original data set.
#Then the statistic of interest is computed from the resample from the first step. We repeat this routine many times to get a more precise estimate of the Bootstrap distribution of the statistic.
def compute_enrichment(species_GO_dict, dataset_size, counting_stats, trials = 1000, apply_bonferroni = True, seed = 50):

    def dict_frequency(data_dict):

        total_tags = 0
        for key in data_dict:
            total_tags += data_dict[key]

        frequency_dict = {}

        for key in data_dict:
            frequency_dict[key] = float(data_dict[key])/float(total_tags)

        return frequency_dict

    numpy.random.seed(seed)


    #species/target go dicts are gene-go codes, counting stats is a go-int dictionary
    result_stats = defaultdict(list)
    counting_stats = dict_frequency(counting_stats)
    for i in range(0, trials):
        #select number of genes equal to number of keys in the target_go_dict, with replacement, without weights
        random_genes = numpy.random.choice(species_GO_dict.keys(), size = dataset_size, replace=True)

        temp_counting_stats = defaultdict(int)

        for gene in random_genes:
            for go in species_GO_dict[gene]:
                temp_counting_stats[go] += 1

        #trial_results.append(temp_counting_stats)

        #print temp_counting_stats

        frequency_dict = dict_frequency(temp_counting_stats)

        for key in frequency_dict:
            result_stats[key].append(frequency_dict[key])

    #have all data, want to compute probability of observing counting_stats (from target GO dict) in distribution

    collated_dict = {}
    p_values = {}
    comparison_count = 0
    no_variation = set()

    for key in result_stats:

        trial_mean = numpy.mean(result_stats[key])
        trial_std  = numpy.std(result_stats[key])

        collated_dict[key] = (trial_mean, trial_std)

        if(trial_std == 0):
            no_variation.add(key)
            #print 'No variation detected for GO ID %s in %i samples' % (key, trials)
            continue

        if(key in counting_stats):
            p_values[key] = scipy.stats.norm.cdf((trial_mean - counting_stats[key])/trial_std)
            comparison_count+=1
        else:
            p_values[key] = scipy.stats.norm.cdf((trial_mean - 0)/trial_std)
            comparison_count+=1

    if(apply_bonferroni and comparison_count != 0):

        alpha_modifier = 1.0/float(comparison_count)

        for key in p_values:
            p_values[key] = p_values[key]*alpha_modifier

    return p_values

def load_species_dataset(go_file):

    output_dict = defaultdict(set)

    for line in open(go_file,'rU'):

        if('!' == line[0]):
            continue

        tokens = line.split('\t')

        gene = tokens[2].strip()
        go_id = tokens[4].strip()

        output_dict[gene.upper()].add(go_id)

    return output_dict

def test_functionality():

    input_dir = DatabaseUtils.INPUT_DIR

    ecoli_sample_data = load_species_dataset(os.path.join(input_dir,'gene_association.ecocyc'))

    keys = ['id', 'name', 'namespace','def']

    GO_dict = parse_GO_file(os.path.join(input_dir,'go-basic.obo'), keys)

    subset = GO_dict.keys()

    test_go_sample = ['GO:0003756', 'GO:0008270', 'GO:0006457', 'GO:0042026', 'GO:0051082', 'GO:0016020', 'GO:0005737', 'GO:0008270', 'GO:0016020', 'GO:0008270','GO:0006260','GO:0009408']

    synthetic_dict = {'dnaJ':test_go_sample}

    output_dict, counts = gene_set_statistics(GO_dict, ecoli_sample_data, 'name')

    for key in counts:
        print key, counts[key]

if(__name__ == '__main__'):
    test_functionality()






